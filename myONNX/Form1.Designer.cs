﻿
namespace HKTEST.ONNX
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_model_deploy = new System.Windows.Forms.Button();
            this.btn_choose_testimage = new System.Windows.Forms.Button();
            this.btn_choose_claspath = new System.Windows.Forms.Button();
            this.btn_choose_model_path = new System.Windows.Forms.Button();
            this.tb_test_image = new System.Windows.Forms.TextBox();
            this.tb_clas_path = new System.Windows.Forms.TextBox();
            this.tb_model_path = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1274, 117);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 32);
            this.button3.TabIndex = 27;
            this.button3.Text = "模型推理Debug";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(1008, 57);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(171, 40);
            this.button2.TabIndex = 26;
            this.button2.Text = "图像读取";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(764, 56);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(171, 40);
            this.button1.TabIndex = 25;
            this.button1.Text = "模型装载";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 24;
            this.listBox1.Location = new System.Drawing.Point(10, 147);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(655, 172);
            this.listBox1.TabIndex = 24;
            // 
            // btn_model_deploy
            // 
            this.btn_model_deploy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.btn_model_deploy.Location = new System.Drawing.Point(1274, 59);
            this.btn_model_deploy.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_model_deploy.Name = "btn_model_deploy";
            this.btn_model_deploy.Size = new System.Drawing.Size(171, 40);
            this.btn_model_deploy.TabIndex = 22;
            this.btn_model_deploy.Text = "模型推理";
            this.btn_model_deploy.UseVisualStyleBackColor = true;
            this.btn_model_deploy.Click += new System.EventHandler(this.btn_model_deploy_Click);
            // 
            // btn_choose_testimage
            // 
            this.btn_choose_testimage.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_choose_testimage.Location = new System.Drawing.Point(584, 97);
            this.btn_choose_testimage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_choose_testimage.Name = "btn_choose_testimage";
            this.btn_choose_testimage.Size = new System.Drawing.Size(82, 36);
            this.btn_choose_testimage.TabIndex = 21;
            this.btn_choose_testimage.Text = "选择";
            this.btn_choose_testimage.UseVisualStyleBackColor = true;
            this.btn_choose_testimage.Click += new System.EventHandler(this.btn_choose_testimage_Click);
            // 
            // btn_choose_claspath
            // 
            this.btn_choose_claspath.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_choose_claspath.Location = new System.Drawing.Point(584, 53);
            this.btn_choose_claspath.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_choose_claspath.Name = "btn_choose_claspath";
            this.btn_choose_claspath.Size = new System.Drawing.Size(82, 36);
            this.btn_choose_claspath.TabIndex = 20;
            this.btn_choose_claspath.Text = "选择";
            this.btn_choose_claspath.UseVisualStyleBackColor = true;
            this.btn_choose_claspath.Click += new System.EventHandler(this.btn_choose_claspath_Click);
            // 
            // btn_choose_model_path
            // 
            this.btn_choose_model_path.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_choose_model_path.Location = new System.Drawing.Point(584, 9);
            this.btn_choose_model_path.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_choose_model_path.Name = "btn_choose_model_path";
            this.btn_choose_model_path.Size = new System.Drawing.Size(82, 35);
            this.btn_choose_model_path.TabIndex = 23;
            this.btn_choose_model_path.Text = "选择";
            this.btn_choose_model_path.UseVisualStyleBackColor = true;
            this.btn_choose_model_path.Click += new System.EventHandler(this.btn_choose_model_path_Click);
            // 
            // tb_test_image
            // 
            this.tb_test_image.Font = new System.Drawing.Font("宋体", 12F);
            this.tb_test_image.Location = new System.Drawing.Point(131, 102);
            this.tb_test_image.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_test_image.Name = "tb_test_image";
            this.tb_test_image.Size = new System.Drawing.Size(446, 35);
            this.tb_test_image.TabIndex = 19;
            // 
            // tb_clas_path
            // 
            this.tb_clas_path.Font = new System.Drawing.Font("宋体", 12F);
            this.tb_clas_path.Location = new System.Drawing.Point(131, 56);
            this.tb_clas_path.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_clas_path.Name = "tb_clas_path";
            this.tb_clas_path.Size = new System.Drawing.Size(446, 35);
            this.tb_clas_path.TabIndex = 18;
            // 
            // tb_model_path
            // 
            this.tb_model_path.Font = new System.Drawing.Font("宋体", 12F);
            this.tb_model_path.Location = new System.Drawing.Point(131, 10);
            this.tb_model_path.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_model_path.Name = "tb_model_path";
            this.tb_model_path.Size = new System.Drawing.Size(446, 35);
            this.tb_model_path.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(24, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 29);
            this.label4.TabIndex = 16;
            this.label4.Text = "测试图片:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(24, 61);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 29);
            this.label3.TabIndex = 15;
            this.label3.Text = "类别文件:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(24, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 29);
            this.label2.TabIndex = 14;
            this.label2.Text = "推理模型:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1027, 176);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 37);
            this.label1.TabIndex = 13;
            this.label1.Text = "图片预测结果";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pictureBox2.Location = new System.Drawing.Point(673, 220);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(838, 576);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pictureBox1.Location = new System.Drawing.Point(10, 346);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(655, 450);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1521, 810);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btn_model_deploy);
            this.Controls.Add(this.btn_choose_testimage);
            this.Controls.Add(this.btn_choose_claspath);
            this.Controls.Add(this.btn_choose_model_path);
            this.Controls.Add(this.tb_test_image);
            this.Controls.Add(this.tb_clas_path);
            this.Controls.Add(this.tb_model_path);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btn_model_deploy;
        private System.Windows.Forms.Button btn_choose_testimage;
        private System.Windows.Forms.Button btn_choose_claspath;
        private System.Windows.Forms.Button btn_choose_model_path;
        private System.Windows.Forms.TextBox tb_test_image;
        private System.Windows.Forms.TextBox tb_clas_path;
        private System.Windows.Forms.TextBox tb_model_path;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}