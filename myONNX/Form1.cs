﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;
using OpenCvSharp.Dnn;
using System.Runtime.InteropServices;
using System.Threading;

namespace HKTEST.ONNX
{
    public partial class Form1 : Form
    {
        //申明模型变量
        public MyONNX myModel = new MyONNX();

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_choose_model_path_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            //若要改变对话框标题
            dlg.Title = "选择推理模型文件";
            //指定当前目录
            //dlg.InitialDirectory = System.Environment.CurrentDirectory;
            //dlg.InitialDirectory = System.IO.Path.GetFullPath(@"..//..//..//..");
            //设置文件过滤效果
            dlg.Filter = "模型文件(*.pt,*.onnx,*.engine)|*.pt;*.onnx;*.engine";
            dlg.InitialDirectory = @"E:\Git_space\Csharp_deploy_Yolov8\model";
            //判断文件对话框是否打开
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tb_model_path.Text = dlg.FileName;
            }
        }

        private void btn_choose_claspath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            //若要改变对话框标题
            dlg.Title = "选择分类文件";
            //指定当前目录
            //dlg.InitialDirectory = System.Environment.CurrentDirectory;
            //dlg.InitialDirectory = System.IO.Path.GetFullPath(@"..//..//..//..");
            //设置文件过滤效果
            dlg.Filter = "分类文件(*.txt)|*.txt";
            dlg.InitialDirectory = @"E:\Git_space\Csharp_deploy_Yolov8\demo";
            //判断文件对话框是否打开
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tb_clas_path.Text = dlg.FileName;
            }
        }

        private void btn_choose_testimage_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            //若要改变对话框标题
            dlg.Title = "选择测试图片文件";
            //指定当前目录
            //dlg.InitialDirectory = System.Environment.CurrentDirectory;
            //dlg.InitialDirectory = System.IO.Path.GetFullPath(@"..//..//..//..");
            //设置文件过滤效果
            dlg.Filter = "图片文件(*.png,*.jpg,*.jepg)|*.png;*.jpg;*.jepg";
            dlg.InitialDirectory = @"E:\Git_space\Csharp_deploy_Yolov8\demo";
            //判断文件对话框是否打开
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tb_test_image.Text = dlg.FileName;
            }
        }

        private void btn_model_deploy_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;           //数据预处理
            Tensor<float> tensor = myModel.image2tensor(myModel.image);
            DateTime end = DateTime.Now;
            TimeSpan ts1 = end.Subtract(begin);
            listBox1.Items.Add("图像预处理时间(ms)：" + ts1.TotalMilliseconds.ToString());

            begin = DateTime.Now;
            //模型推理
            float[] result_array = myModel.ONNX_Run(tensor);
            end = DateTime.Now;
            TimeSpan ts2 = end.Subtract(begin);
            listBox1.Items.Add("模型推理时间(ms)：" + ts2.TotalMilliseconds.ToString());

            begin = DateTime.Now;
            //resize的比例
            float[] factors = new float[2];
            factors[0] = factors[1] = (float)(Math.Max(myModel.image.Rows, myModel.image.Cols) / 640.0);
            //结果处理NMS
            DetectionResult result_pro = new DetectionResult(myModel.class_names, factors);
            Result result = result_pro.process_result(result_array);   //
            Mat result_image = result_pro.draw_result(result, myModel.image.Clone());
            end = DateTime.Now;
            TimeSpan ts3 = end.Subtract(begin);
            listBox1.Items.Add("后处理时间(ms)：" + ts3.TotalMilliseconds.ToString());

            pictureBox2.Image = new Bitmap(result_image.ToMemoryStream()) as System.Drawing.Image;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string model_path = tb_model_path.Text;
            //model_path = @"E:\Git_space\基于Csharp部署Yolov8\model\yolov8s.engine";
            //model_path = @"E:\Git_space\Csharp_deploy_Yolov8\model\yolov8s.onnx";
            //model_path = @"E:\Git_space\Csharp_deploy_Yolov8\model\yolov8s-seg.onnx";
            //model_path = @"E:\Git_space\Csharp_deploy_Yolov8\model\yolov8s-pose.onnx";
            string classer_path = tb_clas_path.Text;


            DateTime begin = DateTime.Now;
            //模型装载
            myModel.ONNX_Load(model_path, classer_path);
            DateTime end = DateTime.Now;
            TimeSpan model_load = end.Subtract(begin);

            listBox1.Items.Add("模型加载时间(ms)：" + model_load.TotalMilliseconds.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string image_path = tb_test_image.Text;
            //image_path = @"E:\Git_space\Csharp_deploy_Yolov8\demo\demo_9.jpg";

            DateTime begin = DateTime.Now;
            //图像装载
            myModel.image = new Mat(image_path);
            myModel.resize_image = myModel.image2resize_image(myModel.image);
            DateTime end = DateTime.Now;
            TimeSpan data_load = end.Subtract(begin);

            listBox1.Items.Add("数据加载时间(ms)：" + data_load.TotalMilliseconds.ToString());

            pictureBox1.Image = new Bitmap(myModel.image.ToMemoryStream()) as System.Drawing.Image;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string image_path = tb_test_image.Text;

            //图像装载
            DateTime begin = DateTime.Now;
            Mat image = new Mat(image_path);
            DetectionResult results = myModel.Run(image); ;
            DateTime end = DateTime.Now;
            TimeSpan ts = end.Subtract(begin);
            listBox1.Items.Add("识别时间(ms)：" + ts.TotalMilliseconds.ToString());

            //    Image image = Image.Load<Rgb24>(tb_test_image.Text);

            //    DateTime begin = DateTime.Now;
            //    image.Mutate(x => x.Resize(640, 640));
            //    Tensor<float> input_tensor = MyONNX.ExtractPixels(image);
            //    DateTime end = DateTime.Now;
            //    TimeSpan ts1 = end.Subtract(begin);
            //    listBox1.Items.Add("数据预处理时间(ms)：" + ts1.TotalMilliseconds.ToString());

            //    begin = DateTime.Now;
            //    //运行 Inference 并获取结果
            //    float[] result_array = myModel.ONNX_Run(input_tensor);
            //    end = DateTime.Now;
            //    TimeSpan ts2 = end.Subtract(begin);
            //    listBox1.Items.Add("模型推理时间(ms)：" + ts2.TotalMilliseconds.ToString());

            //    begin = DateTime.Now;
            //    //resize的比例
            //    float[] factors = new float[2];
            //    factors[0] = factors[1] = (float)(Math.Max(image.Height, image.Width) / 640.0);
            //    //结果处理NMS
            //    DetectionResult result_pro = new DetectionResult(tb_clas_path.Text, factors);
            //    Result result = result_pro.process_result(result_array);   //

            //    Mat result_image = result_pro.draw_result(result, myModel.image.Clone());
            //    end = DateTime.Now;
            //    TimeSpan result_process = end.Subtract(begin);
            //    listBox1.Items.Add("结果处理时间(ms)：" + result_process.TotalMilliseconds.ToString());

            //    pictureBox2.Image = new Bitmap(result_image.ToMemoryStream()) as System.Drawing.Image;

            //    ///////////////
            //    //float[,,,] arr = new float[1, 640, 640, 3];
            //    //NDArray ndarr = np.array(arr);
            //    //NDArray ndarr1 = ndarr.reshape(1, 3, 640, 640);

            //    //Bitmap image=new Bitmap(tb_test_image.Text);
            //    //NDArray arr2 = np_.ToNDArray(image);
            //    //NDArray arr3 = arr2.reshape(1, 3, image.Height, image.Width);

            //    //int x = 0;
        }
    }
}
