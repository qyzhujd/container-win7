﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Text.RegularExpressions;

namespace RegNo
{
    public partial class frmRegNo : Form
    {
        public static string MachineID = "";

        public frmRegNo()
        {
         InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = GenerateRegNo(textBox1.Text);
        }

        //验证注册ID
        public static bool RegNoVerification(string RegNo)
        {
            if (MachineID == "")
                MachineID = GetMac();

            string GeneratedNo = GenerateRegNo(MachineID);

            if (RegNo != GeneratedNo)
                
                return false;
            else
                return true;
        }

        
        public static string getMachineID()
        {
            string[] mocString;
            string MachineID = "";

            //MachineID = GetMac();
            mocString = GetMoc();
            for (int i = 0; i < 2; i++)
            {
                mocString[i] = mocString[i].Replace(":", "");
                if (mocString[i].Length > 6)
                    MachineID += mocString[i].Substring(mocString[i].Length - 6, 6);
                else
                    MachineID += mocString[i];
            }

            return MachineID;
        }

        public static string GenerateRegNo(string MachineID)
        {
            string RegNo, t1;
            int tt;

            RegNo = "";
            for (int i = 0; i < MachineID.Length; i++)
            {
                t1 = MachineID.Substring(i, 1);
                if (IsNumeric(t1))
                {
                    tt = (i + Convert.ToInt32(t1)) % 10;
                }
                else
                {
                    byte[] array = Encoding.ASCII.GetBytes(t1);
                    tt = (i + array[0]) % 10;
                }
                RegNo = tt.ToString() + RegNo;
            }

            return RegNo;
        }

        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?/d*[.]?/d*$");
        }


        public static string GetCpu()
        {
            string str = "";

            ManagementClass mcCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection mocCpu = mcCpu.GetInstances();
            foreach (ManagementObject m in mocCpu)
            {
                str = m["ProcessorId"].ToString();
                break;
            }

            return str;
        }

        public static string GetMac()
        {
            string str = "";

            ManagementClass mcMAC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection mocMAC = mcMAC.GetInstances();
            foreach (ManagementObject m in mocMAC)
            {
                if ((bool)m["IPEnabled"])
                {
                    str = m["MacAddress"].ToString();
                    break;
                }
            }

            return str.Replace(":", "A"); ;
        }

        public static string[] GetMoc()
        {
            string[] str = new string[3];

            ManagementClass mcCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection mocCpu = mcCpu.GetInstances();
            foreach (ManagementObject m in mocCpu)
            {
                str[0] = m["ProcessorId"].ToString();
            }

            ManagementClass mcHD = new ManagementClass("win32_logicaldisk");
            ManagementObjectCollection mocHD = mcHD.GetInstances();
            foreach (ManagementObject m in mocHD)
            {
                if (m["DeviceID"].ToString() == "C:")
                {
                    str[1] = m["VolumeSerialNumber"].ToString();
                    break;
                }
            }

            ManagementClass mcMAC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection mocMAC = mcMAC.GetInstances();
            foreach (ManagementObject m in mocMAC)
            {
                if ((bool)m["IPEnabled"])
                {
                    str[2] = m["MacAddress"].ToString();
                    break;
                }
            }

            return str;
        }

        private void frmRegNo_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = getMachineID();
        }
    }
}
