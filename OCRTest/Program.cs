﻿using OpenCvSharp;
using Sdcb.PaddleOCR;
using Sdcb.PaddleOCR.Models;
using System;
using System.Linq;

namespace OCRTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //初始化paddleocr检测器
            PaddleOcrDetector detector = new PaddleOcrDetector("./output/det_inference/");
            //初始化paddleocr识别器
            PaddleOcrRecognizer recognizer = new PaddleOcrRecognizer(RecognizationModel.FromDirectory("./output/rec_inference/Student/", "./myLabel.txt", ModelVersion.V3));
            try
            {
                //设定参数
                bool AllowRotateDetection = false; /* 允许识别有角度的文字 */
                Mat rawFrame = new Mat("21.jpg");
                //Mat ripeFrame = rawFrame;
                //裁剪图像
                //Mat ripeFrame = rawFrame[new Rect(0, 0, 640, 480)];
                //Mat ripeFrame = ContrastAjust(rawFrame[new Rect(0, 0, 640, 480)], 125);
                Mat ripeFrame = ContrastAjust(rawFrame, 125);
                //放大图像至原来的三倍
                Cv2.Resize(ripeFrame, ripeFrame, new OpenCvSharp.Size(ripeFrame.Width * 3, ripeFrame.Height * 3), 0, 0, InterpolationFlags.Linear);
                RotatedRect[] rects = detector.Run(ripeFrame);

                Mat[] mats =
                    rects.Select(rect =>
                    {
                        Mat roi = ripeFrame[GetCropedRect(rect.BoundingRect(), ripeFrame.Size())];
                        bool wider = roi.Width > roi.Height;
                        if (!wider)
                        {
                            Cv2.Transpose(roi, roi);
                            Cv2.Flip(roi, roi, FlipMode.X);
                        }
                        return roi;
                    })
                    .ToArray();
                try
                {
                    for (int kk = 0; kk < mats.Length; kk++)
                    {
                        Cv2.ImWrite($"./results/{kk}.jpg", mats[kk]);
                        PaddleOcrRecognizerResult result = recognizer.Run(mats[kk]);
                        Console.WriteLine(result.Text);
                    }
                }
                finally
                {
                    foreach (Mat mat in mats)
                    {
                        mat.Dispose();
                    }
                }
            }
            finally
            {
                detector.Dispose();
                recognizer.Dispose();
            }
            

            //Cv2.ImWrite("cvTest.jpg",ripeFrame);
            Console.ReadKey();
        }

        public static Mat GetRotateCropImage(Mat src, RotatedRect rect)
        {
            bool wider = rect.Size.Width > rect.Size.Height;
            float angle = rect.Angle;
            Size srcSize = src.Size();
            Rect boundingRect = rect.BoundingRect();

            int expTop = Math.Max(0, 0 - boundingRect.Top);
            int expBottom = Math.Max(0, boundingRect.Bottom - srcSize.Height);
            int expLeft = Math.Max(0, 0 - boundingRect.Left);
            int expRight = Math.Max(0, boundingRect.Right - srcSize.Width);

            Rect rectToExp = boundingRect + new Point(expTop, expLeft);
            Rect roiRect = Rect.FromLTRB(
                boundingRect.Left + expLeft,
                boundingRect.Top + expTop,
                boundingRect.Right - expRight,
                boundingRect.Bottom - expBottom);
            using Mat boundingMat = src[roiRect];
            using Mat expanded = boundingMat.CopyMakeBorder(expTop, expBottom, expLeft, expRight, BorderTypes.Replicate);
            Point2f[] rp = rect.Points()
                .Select(v => new Point2f(v.X - rectToExp.X, v.Y - rectToExp.Y))
                .ToArray();

            Point2f[] srcPoints = (wider, angle) switch
            {
                (true, >= 0 and < 45) => new[] { rp[1], rp[2], rp[3], rp[0] },
                _ => new[] { rp[0], rp[3], rp[2], rp[1] }
            };

            var ptsDst0 = new Point2f(0, 0);
            var ptsDst1 = new Point2f(rect.Size.Width, 0);
            var ptsDst2 = new Point2f(rect.Size.Width, rect.Size.Height);
            var ptsDst3 = new Point2f(0, rect.Size.Height);

            using Mat matrix = Cv2.GetPerspectiveTransform(srcPoints, new[] { ptsDst0, ptsDst1, ptsDst2, ptsDst3 });

            Mat dest = expanded.WarpPerspective(matrix, new Size(rect.Size.Width, rect.Size.Height), InterpolationFlags.Nearest, BorderTypes.Replicate);

            if (!wider)
            {
                Cv2.Transpose(dest, dest);
            }
            else if (angle > 45)
            {
                Cv2.Flip(dest, dest, FlipMode.X);
            }
            return dest;
        }

        private static Rect GetCropedRect(Rect rect, Size size)
        {
            return Rect.FromLTRB(
                MathUtil.Clamp(rect.Left, 0, size.Width),
                MathUtil.Clamp(rect.Top, 0, size.Height),
                MathUtil.Clamp(rect.Right, 0, size.Width),
                MathUtil.Clamp(rect.Bottom, 0, size.Height));
        }

        private static Mat ContrastAjust(Mat src,int contrastValue)
        {
            Mat dst = new Mat(src.Size(), src.Type());
            for (int y = 0; y < src.Rows; y++)
            {
                for (int x = 0; x < src.Cols; x++)
                {
                    Vec3b color = new Vec3b
                    {
                        Item0 = Saturate_cast(src.Get<Vec3b>(y, x).Item0 * (contrastValue * 0.01) ), // B
                        Item1 = Saturate_cast(src.Get<Vec3b>(y, x).Item1 * (contrastValue * 0.01) ), // G
                        Item2 = Saturate_cast(src.Get<Vec3b>(y, x).Item2 * (contrastValue * 0.01) )  // R
                    };
                    dst.Set(y, x, color);
                }
            }
            return dst;
        }

        //要确保运算后的像素值在正确的范围内
        private static byte Saturate_cast(double n)
        {
            if (n <= 0)
            {
                return 0;
            }
            else if (n > 255)
            {
                return 255;
            }
            else
            {
                return (byte)n;
            }
        }
    }
}
