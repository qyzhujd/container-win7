﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;

namespace HKTest
{
    public partial class Form1 : Form
    {
        //用于建立数据库连接
        private SQLiteConnection mConn;
        //数据适配器
        private SQLiteDataAdapter mAdapter;
        //用于存储数据库中的查询出来的数据
        private DataSet dataSource;
        //定义每页展示三十条数据
        private int pageSize;

        //根据数据库返回页面数量
        private int pageCount = 0;

        public int PageCount
        {
            get
            {
                if (dataSource != null)
                {
                    pageCount = (int)Math.Ceiling(dataSource.Tables[0].Rows.Count * 1.0 / pageSize);
                }
                return pageCount;
            }
        }

        private int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            //根据页码不同，切换底部导航栏按钮状态
            set
            {
                if (value == 0)
                {
                    value = 0;
                    //回到第一页按钮
                    this.bindingNavigatorMoveFirstItem.Enabled = false;
                    //向前翻页按钮
                    this.bindingNavigatorMovePreviousItem.Enabled = false;
                    //向后翻页按钮
                    this.bindingNavigatorMoveNextItem.Enabled = false;
                    //回到最后一页按钮
                    this.bindingNavigatorMoveLastItem.Enabled = false;
                }
                else if (value == 1)
                {
                    //回到第一页按钮
                    this.bindingNavigatorMoveFirstItem.Enabled = false;
                    //向前翻页按钮
                    this.bindingNavigatorMovePreviousItem.Enabled = false;
                    //向后翻页按钮
                    this.bindingNavigatorMoveNextItem.Enabled = true;
                    //回到最后一页按钮
                    this.bindingNavigatorMoveLastItem.Enabled = true;
                }
                else if (value >= PageCount)
                {
                    value = PageCount;
                    //回到第一页按钮
                    this.bindingNavigatorMoveFirstItem.Enabled = true;
                    //向前翻页按钮
                    this.bindingNavigatorMovePreviousItem.Enabled = true;
                    //向后翻页按钮
                    this.bindingNavigatorMoveNextItem.Enabled = false;
                    //回到最后一页按钮
                    this.bindingNavigatorMoveLastItem.Enabled = false;
                }
                else
                {
                    //回到第一页按钮
                    this.bindingNavigatorMoveFirstItem.Enabled = true;
                    //向前翻页按钮
                    this.bindingNavigatorMovePreviousItem.Enabled = true;
                    //向后翻页按钮
                    this.bindingNavigatorMoveNextItem.Enabled = true;
                    //回到最后一页按钮
                    this.bindingNavigatorMoveLastItem.Enabled = true;
                }
                currentPage = value;
            }
        }

        private int currentRow;
        /// <summary>
        /// 每页首行索引
        /// </summary>
        public int CurrentRow
        {
            get
            {
                if (CurrentPage == 1)
                {
                    currentRow = 0;
                }
                else
                {
                    currentRow = (CurrentPage - 1) * pageSize;
                }
                return currentRow;
            }
        }

        int max = 0;
        /// <summary>
        /// 每页行数最大值索引索引
        /// </summary>
        public int Max
        {
            get
            {
                if (CurrentPage == pageCount)
                {
                    max = dataSource.Tables[0].Rows.Count;
                }
                else
                {
                    max = pageSize * CurrentPage;
                }
                return max;
            }
        }

        public Form1()
        {
            InitializeComponent();
            dateTimePicker1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            dateTimePicker2.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,23,59,59);
        }

        /// <summary>
        /// 绑定combobox中的页码
        /// </summary>
        private void BindPage()
        {
            this.bindingNavigatorCountItem.Text = PageCount.ToString();
            this.toolStripComboBox1.Items.Clear();
            for (int i = 1; i <= pageCount; i++)
            {
                this.toolStripComboBox1.Items.Add("第" + i + "页");
            }
            //查询到有数据
            if (this.toolStripComboBox1.Items.Count > 0)
            {
                this.toolStripComboBox1.SelectedIndex = 0;
            }
            else //没有查询到数据
            {
                dataGridView1.DataSource = null;
            }
        }

        /// <summary>
        /// 分页绑定数据
        /// </summary>
        private void BindSource()
        {
            DataTable dt = new DataTable();
            //下面这个循环比较有用，没有这个循环直接绑定的话datagridview是不显示数据的
            //原因是在dt这个对象里面那个columns集合是空，所以这个datagridview没有办法自动绑定
            for (int i = 0; i < dataSource.Tables[0].Columns.Count; i++)
            {
                dt.Columns.Add(dataSource.Tables[0].Columns[i].ToString());
            }
            for (int i = CurrentRow; i < Max; i++)
            {
                dt.ImportRow(dataSource.Tables[0].Rows[i]);
            }
            this.dataGridView1.DataSource = dt;
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = true;
            dataSource = new DataSet();
            pageSize = 30;
            CurrentPage = 0;
            //连接数据库
            mConn = new SQLiteConnection();
            mConn.ConnectionString = @"data source = " + Application.StartupPath + "/ContainerDB.db";
            mConn.Open();
        }

        /// <summary>
        /// 第一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
        {
            this.toolStripComboBox1.SelectedIndex = 0;
        }

        /// <summary>
        /// 切换页码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentPage = this.toolStripComboBox1.SelectedIndex + 1;
            this.bindingNavigatorPositionItem.Text = CurrentPage.ToString();
            BindSource();
        }

        /// <summary>
        /// 下一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.toolStripComboBox1.SelectedIndex = CurrentPage;
            }
            catch (Exception ee)
            { }
        }


        /// <summary>
        /// 检索按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            string startTime = this.dateTimePicker1.Value.ToString("yyy-MM-dd HH:mm");
            string endTime = this.dateTimePicker2.Value.ToString("yyy-MM-dd HH:mm");
            string cameraType = this.comboBox1.SelectedItem.ToString();
            string containerNo = txtNO.Text;
                 
            string sql;

            if (cameraType == "综合")
            {
                sql = $"SELECT * FROM [TABLE] where" +
                    $" datetime(GrabTime) >= '{startTime}'" +
                    $" and datetime(GrabTime) <= '{endTime}'" +
                    $" and ContainerNo like '%{containerNo}%'" +
                    $" and CameraType LIKE '{cameraType}%'";
            }
            else 
            {
                sql = $"SELECT * FROM [TABLE] where" +
                $" datetime(GrabTime) >= '{startTime}'" +
                $" and datetime(GrabTime) <= '{endTime}'" +
                $" and ContainerNo like '%{containerNo}%'" +
                $" and CameraType ='{cameraType}'";
            }
            mAdapter = new SQLiteDataAdapter(sql, mConn);
            dataSource.Clear();
            mAdapter.Fill(dataSource);
            mAdapter.Dispose();
            BindPage();
            Double okItems = Convert.ToDouble(dataSource.Tables[0].Compute("Count([CheckResult])", "CheckResult=1"));
            Double sumItems = Convert.ToDouble(dataSource.Tables[0].Compute("Count([CheckResult])", ""));
            //更新识别率
            if (sumItems == 0)
            {
                this.label5.Text = "??%";
                this.toolStripLabel2.Text = $"{sumItems}条";
            }
            else
            {
                string okPercent = (okItems / sumItems * 100).ToString("f2");
                this.label5.Text = $"{okPercent}%";
                this.toolStripLabel2.Text = $"{sumItems}条";
            }
        }

        /// <summary>
        /// 最后一页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
        {
            this.toolStripComboBox1.SelectedIndex = PageCount - 1;
        }

        //更换表格列名
        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < this.dataGridView1.Columns.Count; i++)
            {
                if (this.dataGridView1.Columns[i].HeaderText == "ContainerNo")
                {
                    this.dataGridView1.Columns[i].HeaderText = "箱号";
                }
                else if (this.dataGridView1.Columns[i].HeaderText == "ContainerType")
                {
                    this.dataGridView1.Columns[i].HeaderText = "箱型";
                }
                else if (this.dataGridView1.Columns[i].HeaderText == "CameraType")
                {
                    this.dataGridView1.Columns[i].HeaderText = "相机机位";
                }
                else if (this.dataGridView1.Columns[i].HeaderText == "ImagePath")
                {
                    this.dataGridView1.Columns[i].HeaderText = "图像路径";
                }
                else if (this.dataGridView1.Columns[i].HeaderText == "GrabTime")
                {
                    this.dataGridView1.Columns[i].HeaderText = "抓拍时间";
                }
                else if (this.dataGridView1.Columns[i].HeaderText == "CheckResult")
                {
                    this.dataGridView1.Columns[i].HeaderText = "校验结果";
                }
            }

        }

        private void bindingNavigatorMovePreviousItem_Click_1(object sender, EventArgs e)
        {
            this.toolStripComboBox1.SelectedIndex = CurrentPage - 2;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string imagePath = System.IO.Path.Combine(InitialValue.Settings.ImagePath, dataGridView1.CurrentCell.Value.ToString());
            try
            {
                if (imagePath.Contains(":"))
                {
                    Mat image = Cv2.ImRead(imagePath);
                    Cv2.NamedWindow(imagePath, WindowFlags.AutoSize);
                    Cv2.ImShow(imagePath, image);
                }
            }
            catch
            {
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
