﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;
using System.Drawing;
using OpenCvSharp;
using OpenCvSharp.Dnn;
using OpenCvSharp.Extensions;
using Point = OpenCvSharp.Point;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace HKTest.ONNX
{
    //把一个任意尺寸的Mat图像转成正方形的640*640的推理图像
    public class MyONNX
    {
        public string m_model_path;
        public Mat image;
        public Mat resize_image;    
        public InferenceSession onnx_session;
        public string[] class_names;        // 识别结果类型

        public resultPP[] Run(Mat image)
        {
            if (onnx_session == null)
            {
                resultPP[] tt = new resultPP[0];
                return tt;
            }
            //数据预处理
            Tensor<float> tensor = image2tensor(image);

            //模型推理
            float[] result_array = ONNX_Run(tensor);

            //resize的比例
            float[] factors = new float[2];
            factors[0] = factors[1] = (float)(Math.Max(image.Rows, image.Cols) / 640.0);

            //结果处理NMS
            DetectionResult result_pro = new DetectionResult(class_names, factors);
            Result result = result_pro.process_result(result_array);  

            return result_pro.results.ToArray();
        }


        /// <summary>
        /// 读取本地识别结果类型文件到内存
        /// </summary>
        /// <param name="path">文件路径</param>
        public void read_class_names(string path)
        {

            List<string> str = new List<string>();
            StreamReader sr = new StreamReader(path);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                str.Add(line);
            }

            class_names = str.ToArray();
        }

        //模型装载
        public void ONNX_Load(string model_path, string classer_path)
        {
            m_model_path = model_path;

            // 创建输出会话，用于输出模型读取信息
            SessionOptions options = new SessionOptions();
            options.LogSeverityLevel = OrtLoggingLevel.ORT_LOGGING_LEVEL_INFO;

            // 设置为CPU上运行
            //options.AppendExecutionProvider_CPU(0);
            options.AppendExecutionProvider_CUDA(0);

            // 推理模型类，读取本地模型文件
            onnx_session = new InferenceSession(m_model_path, options);//model_path 为onnx模型文件的路径
            
            //读取类别文件
            read_class_names(classer_path);
        }

        public void ONNX_Load_CPU(string model_path, string classer_path)
        {
            m_model_path = model_path;

            // 创建输出会话，用于输出模型读取信息
            SessionOptions options = new SessionOptions();
            options.LogSeverityLevel = OrtLoggingLevel.ORT_LOGGING_LEVEL_INFO;

            // 设置为CPU上运行
            options.AppendExecutionProvider_CPU(0);
            //options.AppendExecutionProvider_CUDA(0);

            // 推理模型类，读取本地模型文件
            onnx_session = new InferenceSession(m_model_path, options);//model_path 为onnx模型文件的路径

            //读取类别文件
            read_class_names(classer_path);
        }

        //模型卸载
        public void ONNX_dispose()
        {
            if (onnx_session !=null)
                onnx_session.Dispose();
        }

        public Mat image2resize_image(Mat image)
        {
            // 配置图片数据
            int max_image_length = image.Cols > image.Rows ? image.Cols : image.Rows;
            Mat max_image = Mat.Zeros(new OpenCvSharp.Size(max_image_length, max_image_length), MatType.CV_8UC3);
            Rect roi = new Rect(0, 0, image.Cols, image.Rows);
            image.CopyTo(new Mat(max_image, roi));

            // 将图片转为RGB通道
            Mat image_rgb = new Mat();
            Cv2.CvtColor(max_image, image_rgb, ColorConversionCodes.BGR2RGB);    //色彩空间转换

            //图像尺寸缩放至640*640
            Mat resize_image = new Mat();
            Cv2.Resize(image_rgb, resize_image, new OpenCvSharp.Size(640, 640));

            max_image.Dispose();
            image_rgb.Dispose();
            
            return resize_image;
        }


        public Tensor<float> image2tensor(Mat image)
        {
            // 配置图片数据
            int max_image_length = image.Cols > image.Rows ? image.Cols : image.Rows;
            Mat max_image = Mat.Zeros(new OpenCvSharp.Size(max_image_length, max_image_length), MatType.CV_8UC3);
            Rect roi = new Rect(0, 0, image.Cols, image.Rows);
            image.CopyTo(new Mat(max_image, roi));

            //图像尺寸缩放至640*640
            Mat resize_image = new Mat();   //类静态变量
            Cv2.Resize(max_image, resize_image, new OpenCvSharp.Size(640, 640));

            //// 将图片转为RGB三通道
            //Mat image_rgb = new Mat();
            //Cv2.CvtColor(max_image, image_rgb, ColorConversionCodes.BGR2RGB);    //色彩空间转换
            //转成32FC3
            Mat resize_image1 = new Mat(640, 640, MatType.CV_32FC3);
            resize_image.ConvertTo(resize_image1, MatType.CV_32FC3, 1 / 255F); 
            
            Mat imageR = new Mat(640, 640, MatType.CV_32FC1);
            Mat imageG = new Mat(640, 640, MatType.CV_32FC1);
            Mat imageB = new Mat(640, 640, MatType.CV_32FC1);

            Cv2.ExtractChannel(resize_image1, imageR, 2);
            Cv2.ExtractChannel(resize_image1, imageG, 1); 
            Cv2.ExtractChannel(resize_image1, imageB, 0);

            int pixelnum = (int)resize_image.Total();
            float[] array = new float[pixelnum * 3];

            Marshal.Copy(imageR.Data, array, 0, pixelnum);
            Marshal.Copy(imageG.Data, array, pixelnum, pixelnum);
            Marshal.Copy(imageB.Data, array, pixelnum*2, pixelnum);

            var tensor = new DenseTensor<float>(array.AsMemory<float>(), new[] { 1, 3, 640, 640 });

            max_image.Dispose();
            resize_image1.Dispose();
            imageR.Dispose();
            imageG.Dispose();
            imageB.Dispose();

            return tensor;
        }


        public float[] ONNX_Run(Tensor<float> input_tensor)
        {
            // 创建输入容器
            List<NamedOnnxValue> input_ontainer = new List<NamedOnnxValue>();
            //将 input_tensor 放入一个输入参数的容器，并指定名称
            input_ontainer.Add(NamedOnnxValue.CreateFromTensor("images", input_tensor));

            //运行 Inference 并获取结果
            IDisposableReadOnlyCollection<DisposableNamedOnnxValue> result_infer = onnx_session.Run(input_ontainer);
 
            // 将输出结果转为DisposableNamedOnnxValue数组
            DisposableNamedOnnxValue[] results_onnxvalue = result_infer.ToArray();

            // 读取第一个节点输出并转为Tensor数据
            Tensor<float> result_tensors = results_onnxvalue[0].AsTensor<float>();

            //推理模型返回结果
            float[] result_array = result_tensors.ToArray();

            return result_array;
        }
    }


    public struct resultPP
    {
        public Rect Rect;
        public int LabelId;
        public string LabelName;
        public float Confidence;
    }


    public class Result
    {
        // 获取结果长度
        public int length 
        { get 
            { 
                return scores.Count;
            } 
        }

        // 识别结果类
        public List<string> classes = new List<string>();
        // 置信值
        public List<float> scores = new List<float>();
        // 预测框
        public List<Rect> rects = new List<Rect>();

        /// <summary>
        /// 物体检测
        /// </summary>
        /// <param name="score">预测分数</param>
        /// <param name="rect">识别框</param>
        /// <param name="cla">识别类</param>
        public void add(float score, Rect rect, string cla) {
            scores.Add(score);
            rects.Add(rect);
            classes.Add(cla);
        }
    }

    public class DetectionResult
    {
        public float[] scales;
        // 置信度阈值
        public float score_threshold;
        // 非极大值抑制阈值
        public float nms_threshold;
        //类别名
        public string[] class_names;

        //与PPyolo兼容
        public List<resultPP> results = new List<resultPP>();
        resultPP result0;

        /// <summary>
        /// 结果处理类构造
        /// </summary>
        /// <param name="path">识别类别文件地址</param>
        /// <param name="scales">缩放比例</param>
        /// <param name="score_threshold">分数阈值</param>
        /// <param name="nms_threshold">非极大值抑制阈值</param>
        public DetectionResult(string[] class_names, float[] scales, float score_threshold = 0.25f, float nms_threshold = 0.5f)
        {
            this.class_names = class_names;
            this.scales = scales;
            this.score_threshold = score_threshold;
            this.nms_threshold = nms_threshold;
        }

        //空对象创建
        public DetectionResult()
        {
        }
        /// <summary>
        /// 结果处理
        /// </summary>
        /// <param name="result">模型预测输出</param>
        /// <returns>模型识别结果</returns>
        public Result process_result10(float[] result)
        {
            List<Rect> position_boxes = new List<Rect>();
            List<int> class_ids = new List<int>();
            List<float> confidences = new List<float>();
            Rect box = new Rect();
            int ids = 0;
            float confidence = 0;

            Result re_result = new Result();
            for (int i = 0; i < result.Length; i += 6)
            {
                if (result[i + 4] < 0.5)
                    break;
                else
                {
                    box.X = (int)(result[i + 0] * this.scales[0]);
                    box.Y = (int)(result[i + 1] * this.scales[1]);
                    box.Width = (int)((result[i + 2] - result[i + 0]) * this.scales[0]);
                    box.Height = (int)((result[i + 3] - result[i + 1]) * this.scales[1]);
                    confidence = result[i + 4];
                    ids = (int)result[i + 5];

                    position_boxes.Add(box);
                    class_ids.Add(ids);
                    confidences.Add(confidence);

                    re_result.add(confidence, box, this.class_names[ids]);

                    //结果存到PPYolo格式的results列表中, //resultPP result0;
                    result0.Rect = box;
                    result0.LabelName = this.class_names[ids];
                    result0.Confidence = confidence;

                    results.Add(result0);
                }
            }
            return re_result;
        }

        public Result process_result(float[] result)
        {
            int cols = result.Length / 8400;   //类别数+4（对应bounding box的4个位置值）
            Mat result_data = new Mat(cols, 8400, MatType.CV_32F, result);
            result_data = result_data.Clone().T();

            // 存放结果list
            List<Rect> position_boxes = new List<Rect>();
            List<int> class_ids = new List<int>();
            List<float> confidences = new List<float>();

            // 预处理输出结果
            float[] rowdata = new float[8400 * cols];
            Marshal.Copy(result_data.Data, rowdata, 0, rowdata.Length);

            for (int i = 0; i < result_data.Rows; i++)
            {
                //Mat classes_scores = result_data.Row(i).ColRange(4, cols);//GetArray(i, 5, classes_scores);
                //Point max_classId_point, min_classId_point;
                //double max_score, min_score;
                //// 获取一组数据中最大值及其位置
                //Cv2.MinMaxLoc(classes_scores, out min_score, out max_score,
                //    out min_classId_point, out max_classId_point);
                int ii = i * cols;
                int max_classId_point = 0;
                float max_score = rowdata[ii + 4];
                for (int j = 5; j < cols; j++)
                {
                    if (rowdata[ii + j] > max_score)
                    {
                        max_score = rowdata[ii + j];
                        max_classId_point = j - 4;
                    }
                }

                // 置信度 0～1之间
                // 获取识别框信息
                if (max_score > 0.25)
                {
                    float cx = result_data.At<float>(i, 0);
                    float cy = result_data.At<float>(i, 1);
                    float ow = result_data.At<float>(i, 2);
                    float oh = result_data.At<float>(i, 3);
                    int x = (int)((cx - 0.5 * ow) * this.scales[0]);
                    int y = (int)((cy - 0.5 * oh) * this.scales[1]);
                    int width = (int)(ow * this.scales[0]);
                    int height = (int)(oh * this.scales[1]);
                    Rect box = new Rect();
                    box.X = x;
                    box.Y = y;
                    box.Width = width;
                    box.Height = height;

                    position_boxes.Add(box);
                    //class_ids.Add(max_classId_point.X);
                    class_ids.Add(max_classId_point);
                    confidences.Add((float)max_score);
                }
            }

            // NMS非极大值抑制
            int[] indexes = new int[position_boxes.Count];
            CvDnn.NMSBoxes(position_boxes, confidences, this.score_threshold, this.nms_threshold, out indexes);

            Result re_result = new Result();
            for (int i = 0; i < indexes.Length; i++)
            {
                int index = indexes[i];
                int idx = class_ids[index];
                re_result.add(confidences[index], position_boxes[index], this.class_names[class_ids[index]]);

                //结果存到PPYolo格式的results列表中
                result0.Rect = position_boxes[index];
                result0.LabelName = class_names[class_ids[index]];
                result0.Confidence = confidences[index];

                results.Add(result0);
            }
            return re_result;
        }
        
        /// <summary>
                 /// 结果绘制
                 /// </summary>
                 /// <param name="result">识别结果</param>
                 /// <param name="image">绘制图片</param>
                 /// <returns></returns>
        public Mat draw_result(Result result, Mat image)
        {

            // 将识别结果绘制到图片上
            for (int i = 0; i < result.length; i++)
            {
                //Console.WriteLine(result.rects[i]);
                Cv2.Rectangle(image, result.rects[i], new Scalar(0, 0, 255), 2, LineTypes.Link8);
                Cv2.Rectangle(image, new Point(result.rects[i].TopLeft.X, result.rects[i].TopLeft.Y - 20),
                    new Point(result.rects[i].BottomRight.X, result.rects[i].TopLeft.Y), new Scalar(0, 255, 255), -1);
                Cv2.PutText(image, result.classes[i] + "-" + result.scores[i].ToString("0.00"),
                    new Point(result.rects[i].X, result.rects[i].Y - 10),
                    HersheyFonts.HersheySimplex, 0.6, new Scalar(0, 0, 0), 1);
            }
            return image;
        }

    }

}
