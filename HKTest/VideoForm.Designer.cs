﻿
namespace HKTest
{
    partial class VideoForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoForm));
            this.PnlVideo = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblCapIamge = new System.Windows.Forms.Label();
            this.FCameraState = new System.Windows.Forms.Label();
            this.FTab = new System.Windows.Forms.TabControl();
            this.FImg1 = new System.Windows.Forms.TabPage();
            this.FImg2 = new System.Windows.Forms.TabPage();
            this.FTImg1 = new System.Windows.Forms.TabPage();
            this.FTImg2 = new System.Windows.Forms.TabPage();
            this.BCameraState = new System.Windows.Forms.Label();
            this.BTab = new System.Windows.Forms.TabControl();
            this.BTImg1 = new System.Windows.Forms.TabPage();
            this.BTImg2 = new System.Windows.Forms.TabPage();
            this.BImg1 = new System.Windows.Forms.TabPage();
            this.BImg2 = new System.Windows.Forms.TabPage();
            this.lbDateTime = new System.Windows.Forms.Label();
            this.textNo2 = new System.Windows.Forms.TextBox();
            this.textType1 = new System.Windows.Forms.TextBox();
            this.textType2 = new System.Windows.Forms.TextBox();
            this.cmbContainerType = new System.Windows.Forms.ComboBox();
            this.lbState = new System.Windows.Forms.Label();
            this.buttonStatistics = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGateName = new System.Windows.Forms.Label();
            this.lblNetworking = new System.Windows.Forms.Label();
            this.tmRepeatSend = new System.Windows.Forms.Timer(this.components);
            this.tmMediaInit = new System.Windows.Forms.Timer(this.components);
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.textNo1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblCarCount = new System.Windows.Forms.Label();
            this.FImgPB1 = new System.Windows.Forms.PictureBox();
            this.FImgPB2 = new System.Windows.Forms.PictureBox();
            this.FTImgPB1 = new System.Windows.Forms.PictureBox();
            this.FTImgPB2 = new System.Windows.Forms.PictureBox();
            this.BTImgPB1 = new System.Windows.Forms.PictureBox();
            this.BTImgPB2 = new System.Windows.Forms.PictureBox();
            this.BImgPB1 = new System.Windows.Forms.PictureBox();
            this.BImgPB2 = new System.Windows.Forms.PictureBox();
            this.FrontVideoPB = new System.Windows.Forms.PictureBox();
            this.BackVideoPB = new System.Windows.Forms.PictureBox();
            this.PnlVideo.SuspendLayout();
            this.FTab.SuspendLayout();
            this.FImg1.SuspendLayout();
            this.FImg2.SuspendLayout();
            this.FTImg1.SuspendLayout();
            this.FTImg2.SuspendLayout();
            this.BTab.SuspendLayout();
            this.BTImg1.SuspendLayout();
            this.BTImg2.SuspendLayout();
            this.BImg1.SuspendLayout();
            this.BImg2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrontVideoPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackVideoPB)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlVideo
            // 
            this.PnlVideo.BackColor = System.Drawing.SystemColors.HighlightText;
            this.PnlVideo.Controls.Add(this.label10);
            this.PnlVideo.Controls.Add(this.label9);
            this.PnlVideo.Controls.Add(this.lblCapIamge);
            this.PnlVideo.Controls.Add(this.FCameraState);
            this.PnlVideo.Controls.Add(this.FTab);
            this.PnlVideo.Controls.Add(this.BCameraState);
            this.PnlVideo.Controls.Add(this.BTab);
            this.PnlVideo.Controls.Add(this.FrontVideoPB);
            this.PnlVideo.Controls.Add(this.BackVideoPB);
            resources.ApplyResources(this.PnlVideo, "PnlVideo");
            this.PnlVideo.Name = "PnlVideo";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            this.label10.DoubleClick += new System.EventHandler(this.label10_DoubleClick);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            this.label9.DoubleClick += new System.EventHandler(this.label9_DoubleClick);
            // 
            // lblCapIamge
            // 
            resources.ApplyResources(this.lblCapIamge, "lblCapIamge");
            this.lblCapIamge.Name = "lblCapIamge";
            // 
            // FCameraState
            // 
            this.FCameraState.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.FCameraState, "FCameraState");
            this.FCameraState.ForeColor = System.Drawing.Color.Black;
            this.FCameraState.Name = "FCameraState";
            this.FCameraState.Click += new System.EventHandler(this.FCameraState_Click);
            this.FCameraState.DoubleClick += new System.EventHandler(this.FCameraState_DoubleClick);
            // 
            // FTab
            // 
            this.FTab.Controls.Add(this.FImg1);
            this.FTab.Controls.Add(this.FImg2);
            this.FTab.Controls.Add(this.FTImg1);
            this.FTab.Controls.Add(this.FTImg2);
            resources.ApplyResources(this.FTab, "FTab");
            this.FTab.Name = "FTab";
            this.FTab.SelectedIndex = 0;
            this.FTab.Tag = "0";
            this.FTab.Resize += new System.EventHandler(this.tabControl_Resize);
            // 
            // FImg1
            // 
            this.FImg1.Controls.Add(this.FImgPB1);
            resources.ApplyResources(this.FImg1, "FImg1");
            this.FImg1.Name = "FImg1";
            this.FImg1.UseVisualStyleBackColor = true;
            // 
            // FImg2
            // 
            this.FImg2.Controls.Add(this.FImgPB2);
            resources.ApplyResources(this.FImg2, "FImg2");
            this.FImg2.Name = "FImg2";
            this.FImg2.UseVisualStyleBackColor = true;
            // 
            // FTImg1
            // 
            this.FTImg1.Controls.Add(this.FTImgPB1);
            resources.ApplyResources(this.FTImg1, "FTImg1");
            this.FTImg1.Name = "FTImg1";
            this.FTImg1.UseVisualStyleBackColor = true;
            // 
            // FTImg2
            // 
            this.FTImg2.Controls.Add(this.FTImgPB2);
            resources.ApplyResources(this.FTImg2, "FTImg2");
            this.FTImg2.Name = "FTImg2";
            this.FTImg2.UseVisualStyleBackColor = true;
            // 
            // BCameraState
            // 
            this.BCameraState.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.BCameraState, "BCameraState");
            this.BCameraState.ForeColor = System.Drawing.Color.Black;
            this.BCameraState.Name = "BCameraState";
            this.BCameraState.DoubleClick += new System.EventHandler(this.BCameraState_DoubleClick);
            // 
            // BTab
            // 
            this.BTab.Controls.Add(this.BTImg1);
            this.BTab.Controls.Add(this.BTImg2);
            this.BTab.Controls.Add(this.BImg1);
            this.BTab.Controls.Add(this.BImg2);
            resources.ApplyResources(this.BTab, "BTab");
            this.BTab.Name = "BTab";
            this.BTab.SelectedIndex = 0;
            this.BTab.Tag = "1";
            this.BTab.Resize += new System.EventHandler(this.tabControl_Resize);
            // 
            // BTImg1
            // 
            this.BTImg1.Controls.Add(this.BTImgPB1);
            resources.ApplyResources(this.BTImg1, "BTImg1");
            this.BTImg1.Name = "BTImg1";
            this.BTImg1.UseVisualStyleBackColor = true;
            // 
            // BTImg2
            // 
            this.BTImg2.Controls.Add(this.BTImgPB2);
            resources.ApplyResources(this.BTImg2, "BTImg2");
            this.BTImg2.Name = "BTImg2";
            this.BTImg2.UseVisualStyleBackColor = true;
            // 
            // BImg1
            // 
            this.BImg1.Controls.Add(this.BImgPB1);
            resources.ApplyResources(this.BImg1, "BImg1");
            this.BImg1.Name = "BImg1";
            this.BImg1.UseVisualStyleBackColor = true;
            // 
            // BImg2
            // 
            this.BImg2.Controls.Add(this.BImgPB2);
            resources.ApplyResources(this.BImg2, "BImg2");
            this.BImg2.Name = "BImg2";
            this.BImg2.UseVisualStyleBackColor = true;
            // 
            // lbDateTime
            // 
            this.lbDateTime.BackColor = System.Drawing.Color.Gainsboro;
            resources.ApplyResources(this.lbDateTime, "lbDateTime");
            this.lbDateTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbDateTime.Name = "lbDateTime";
            this.lbDateTime.Click += new System.EventHandler(this.lbDateTime_Click);
            // 
            // textNo2
            // 
            resources.ApplyResources(this.textNo2, "textNo2");
            this.textNo2.Name = "textNo2";
            this.textNo2.TextChanged += new System.EventHandler(this.textNo2_TextChanged);
            // 
            // textType1
            // 
            resources.ApplyResources(this.textType1, "textType1");
            this.textType1.Name = "textType1";
            this.textType1.TextChanged += new System.EventHandler(this.textType1_TextChanged);
            // 
            // textType2
            // 
            resources.ApplyResources(this.textType2, "textType2");
            this.textType2.Name = "textType2";
            this.textType2.TextChanged += new System.EventHandler(this.textType2_TextChanged);
            // 
            // cmbContainerType
            // 
            resources.ApplyResources(this.cmbContainerType, "cmbContainerType");
            this.cmbContainerType.ForeColor = System.Drawing.Color.Blue;
            this.cmbContainerType.FormattingEnabled = true;
            this.cmbContainerType.Items.AddRange(new object[] {
            resources.GetString("cmbContainerType.Items"),
            resources.GetString("cmbContainerType.Items1"),
            resources.GetString("cmbContainerType.Items2"),
            resources.GetString("cmbContainerType.Items3"),
            resources.GetString("cmbContainerType.Items4"),
            resources.GetString("cmbContainerType.Items5")});
            this.cmbContainerType.Name = "cmbContainerType";
            this.cmbContainerType.SelectedIndexChanged += new System.EventHandler(this.cmbContainerType_SelectedIndexChanged);
            // 
            // lbState
            // 
            this.lbState.BackColor = System.Drawing.Color.Gainsboro;
            this.lbState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.lbState, "lbState");
            this.lbState.ForeColor = System.Drawing.Color.Blue;
            this.lbState.Name = "lbState";
            // 
            // buttonStatistics
            // 
            resources.ApplyResources(this.buttonStatistics, "buttonStatistics");
            this.buttonStatistics.Name = "buttonStatistics";
            this.buttonStatistics.UseVisualStyleBackColor = true;
            this.buttonStatistics.Click += new System.EventHandler(this.buttonStatistics_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Name = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Name = "label4";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Name = "label5";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Name = "label6";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Name = "label8";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Name = "label7";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Name = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblGateName
            // 
            this.lblGateName.BackColor = System.Drawing.SystemColors.ControlLight;
            resources.ApplyResources(this.lblGateName, "lblGateName");
            this.lblGateName.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblGateName.Name = "lblGateName";
            this.lblGateName.DoubleClick += new System.EventHandler(this.lblGateName_DoubleClick);
            // 
            // lblNetworking
            // 
            this.lblNetworking.BackColor = System.Drawing.Color.Gainsboro;
            this.lblNetworking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.lblNetworking, "lblNetworking");
            this.lblNetworking.ForeColor = System.Drawing.Color.Blue;
            this.lblNetworking.Name = "lblNetworking";
            // 
            // tmRepeatSend
            // 
            this.tmRepeatSend.Tick += new System.EventHandler(this.tmRepeatSend_Tick);
            // 
            // tmMediaInit
            // 
            this.tmMediaInit.Enabled = true;
            this.tmMediaInit.Interval = 500;
            this.tmMediaInit.Tick += new System.EventHandler(this.tmMediaInit_Tick);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoCheck = false;
            resources.ApplyResources(this.radioButton1, "radioButton1");
            this.radioButton1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.TabStop = true;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoCheck = false;
            resources.ApplyResources(this.radioButton2, "radioButton2");
            this.radioButton2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.TabStop = true;
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // textNo1
            // 
            resources.ApplyResources(this.textNo1, "textNo1");
            this.textNo1.Name = "textNo1";
            this.textNo1.TextChanged += new System.EventHandler(this.textNo1_TextChanged);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Name = "label11";
            // 
            // lblCarCount
            // 
            this.lblCarCount.BackColor = System.Drawing.Color.Gainsboro;
            this.lblCarCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.lblCarCount, "lblCarCount");
            this.lblCarCount.ForeColor = System.Drawing.Color.DarkOrchid;
            this.lblCarCount.Name = "lblCarCount";
            // 
            // FImgPB1
            // 
            this.FImgPB1.Image = global::HKTest.Properties.Resources._133341F_13;
            resources.ApplyResources(this.FImgPB1, "FImgPB1");
            this.FImgPB1.Name = "FImgPB1";
            this.FImgPB1.TabStop = false;
            this.FImgPB1.Click += new System.EventHandler(this.FTImgPB2_Click);
            // 
            // FImgPB2
            // 
            this.FImgPB2.Image = global::HKTest.Properties.Resources._133341F_14;
            resources.ApplyResources(this.FImgPB2, "FImgPB2");
            this.FImgPB2.Name = "FImgPB2";
            this.FImgPB2.TabStop = false;
            // 
            // FTImgPB1
            // 
            this.FTImgPB1.Image = global::HKTest.Properties.Resources.FM;
            resources.ApplyResources(this.FTImgPB1, "FTImgPB1");
            this.FTImgPB1.Name = "FTImgPB1";
            this.FTImgPB1.TabStop = false;
            // 
            // FTImgPB2
            // 
            this.FTImgPB2.Image = global::HKTest.Properties.Resources.FM;
            resources.ApplyResources(this.FTImgPB2, "FTImgPB2");
            this.FTImgPB2.Name = "FTImgPB2";
            this.FTImgPB2.TabStop = false;
            // 
            // BTImgPB1
            // 
            this.BTImgPB1.Image = global::HKTest.Properties.Resources.BM;
            resources.ApplyResources(this.BTImgPB1, "BTImgPB1");
            this.BTImgPB1.Name = "BTImgPB1";
            this.BTImgPB1.TabStop = false;
            // 
            // BTImgPB2
            // 
            this.BTImgPB2.Image = global::HKTest.Properties.Resources.BM;
            resources.ApplyResources(this.BTImgPB2, "BTImgPB2");
            this.BTImgPB2.Name = "BTImgPB2";
            this.BTImgPB2.TabStop = false;
            // 
            // BImgPB1
            // 
            this.BImgPB1.Image = global::HKTest.Properties.Resources._133343B_15;
            resources.ApplyResources(this.BImgPB1, "BImgPB1");
            this.BImgPB1.Name = "BImgPB1";
            this.BImgPB1.TabStop = false;
            // 
            // BImgPB2
            // 
            this.BImgPB2.BackColor = System.Drawing.Color.White;
            this.BImgPB2.Image = global::HKTest.Properties.Resources._133343B_16;
            resources.ApplyResources(this.BImgPB2, "BImgPB2");
            this.BImgPB2.Name = "BImgPB2";
            this.BImgPB2.TabStop = false;
            this.BImgPB2.Click += new System.EventHandler(this.BImgPB2_Click);
            // 
            // FrontVideoPB
            // 
            this.FrontVideoPB.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            resources.ApplyResources(this.FrontVideoPB, "FrontVideoPB");
            this.FrontVideoPB.Name = "FrontVideoPB";
            this.FrontVideoPB.TabStop = false;
            this.FrontVideoPB.Tag = "0";
            this.FrontVideoPB.Paint += new System.Windows.Forms.PaintEventHandler(this.FrontVideoPB_Paint);
            // 
            // BackVideoPB
            // 
            this.BackVideoPB.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            resources.ApplyResources(this.BackVideoPB, "BackVideoPB");
            this.BackVideoPB.Name = "BackVideoPB";
            this.BackVideoPB.TabStop = false;
            this.BackVideoPB.Tag = "1";
            this.BackVideoPB.Paint += new System.Windows.Forms.PaintEventHandler(this.BackVideoPB_Paint);
            // 
            // VideoForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.lblCarCount);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.lblNetworking);
            this.Controls.Add(this.lblGateName);
            this.Controls.Add(this.lbDateTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbState);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textNo2);
            this.Controls.Add(this.textNo1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonStatistics);
            this.Controls.Add(this.cmbContainerType);
            this.Controls.Add(this.textType2);
            this.Controls.Add(this.textType1);
            this.Controls.Add(this.PnlVideo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "VideoForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.PnlVideo.ResumeLayout(false);
            this.PnlVideo.PerformLayout();
            this.FTab.ResumeLayout(false);
            this.FImg1.ResumeLayout(false);
            this.FImg2.ResumeLayout(false);
            this.FTImg1.ResumeLayout(false);
            this.FTImg2.ResumeLayout(false);
            this.BTab.ResumeLayout(false);
            this.BTImg1.ResumeLayout(false);
            this.BTImg2.ResumeLayout(false);
            this.BImg1.ResumeLayout(false);
            this.BImg2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrontVideoPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackVideoPB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PnlVideo;
        private System.Windows.Forms.PictureBox FrontVideoPB;
        private System.Windows.Forms.PictureBox BackVideoPB;
        private System.Windows.Forms.TabControl BTab;
        private System.Windows.Forms.TabPage BTImg1;
        private System.Windows.Forms.TabPage BImg1;
        private System.Windows.Forms.TabPage BImg2;
        private System.Windows.Forms.PictureBox FTImgPB2;
        private System.Windows.Forms.PictureBox BImgPB1;
        private System.Windows.Forms.PictureBox BImgPB2;
        private System.Windows.Forms.Label lbDateTime;
        private System.Windows.Forms.TextBox textNo2;
        private System.Windows.Forms.TextBox textType1;
        private System.Windows.Forms.TextBox textType2;
        private System.Windows.Forms.ComboBox cmbContainerType;
        private System.Windows.Forms.Label lbState;
        private System.Windows.Forms.Button buttonStatistics;
        private System.Windows.Forms.TabPage BTImg2;
        private System.Windows.Forms.PictureBox BTImgPB2;
        private System.Windows.Forms.TabPage FTImg1;
        private System.Windows.Forms.PictureBox FTImgPB1;
        private System.Windows.Forms.TabPage FImg2;
        private System.Windows.Forms.PictureBox FImgPB2;
        private System.Windows.Forms.TabPage FImg1;
        private System.Windows.Forms.PictureBox BTImgPB1;
        private System.Windows.Forms.TabControl FTab;
        private System.Windows.Forms.TabPage FTImg2;
        private System.Windows.Forms.PictureBox FImgPB1;
        private System.Windows.Forms.Label BCameraState;
        private System.Windows.Forms.Label FCameraState;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGateName;
        private System.Windows.Forms.Label lblNetworking;
        private System.Windows.Forms.Timer tmRepeatSend;
        private System.Windows.Forms.Timer tmMediaInit;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox textNo1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblCapIamge;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblCarCount;
    }
}

