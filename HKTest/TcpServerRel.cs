﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;
using HKTest.Utils;

namespace HKTest
{
    class TcpServerRel
    {
        private  Encoding encode = Encoding.Default;

        //作为客户端连接服务端
        public TcpClient client = null;
        public NetworkStream stream = null;

        //作为服务端连接客户端    
        public Socket listener = null;
        public Socket sckServer = null;
        public static bool connectState = true;

        public Thread tcc;

        public bool ServerFlag;
        
        //创建tcp客户端，连接服务端
        public bool TcpConnectServer()
        {
            try
            {
                client = new TcpClient(InitialValue.Network.ServerIP, InitialValue.Network.ServerPort);
                WriteLogFile("已连接:" + client.Client.RemoteEndPoint.ToString());
                Console.WriteLine("已连接:" + client.Client.RemoteEndPoint.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("连接失败: {0}", e.ToString());
                ServerFlag = false;
                WriteLogFile("连接失败");
                //MessageBox.Show("服务器接口失败！");
                return false;
            }
            return true;
        }

        public bool TcpConectServerRetry()
        {
            if (client == null)
            {
                return TcpConnectServer();
            }
            else if (!client.Connected)
            {
                client.Close();
                Application.DoEvents();
                return TcpConnectServer();
            }
            else
            {
                return true;
            }
        }

        //创建tcp服务器，等待客户端连接
        public void TcpConnectClient()
        {
            if (listener == null)
            {
                IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Any, InitialValue.Network.ReleasePort);
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listener.Bind(iPEndPoint); //绑定端口号
                listener.Listen(1);//设置最大监听数

                //listener = new TcpListener(IPAddress.Any, InitialValue.Network.ReleasePort); //IPAddress.Parse(InitialValue.Network.ServerIP)
                ////listener = new TcpListener(IPAddress.Parse("192.168.179.1"), 8001);
                //listener.Start();
                Console.WriteLine("开始监听");
            }
            while (true)
            {
                try
                {
                    sckServer = listener.Accept();
                    VideoForm.ActiveTimeRel = DateTime.Now.Ticks / 10000;     //纪录连接或收到数据时间
                    VideoForm.tcpMessage = ("已连接:" + sckServer.RemoteEndPoint.ToString());
                    WriteLogFile("已连接:" + sckServer.RemoteEndPoint.ToString());
                    Console.WriteLine("已连接:" + sckServer.RemoteEndPoint.ToString());

                    ServerFlag = true;
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Connect to  TcpClient Fail for : {0}", e.ToString());
                    ServerFlag = false;
                    WriteLogFile("Tcp客户端连接失败");
                    //MessageBox.Show("服务器接口失败！");
                    //return;
                }
                //break;
            }
            return;
        }

        //作为tcp服务端，查看是否断开连接，否则则重连
        public bool TcpConnectClientRetry()
        {
            if (tcc.ThreadState != ThreadState.Stopped)  //正在等待连接
            {
                //Console.WriteLine("tcc.status: " + tcc.ThreadState);
                return false;
            }
            else if (sckServer.Connected)    //已连接
            {
                return true;  //返回已连接标志
            }
            else     //未连接，也未开始侦听，则开启侦听线程
            {
                Console.WriteLine("Tcp重启连接");
                //tcc = new Thread(TcpConnectClient);
                //tcc.IsBackground = true;
                //tcc.Start();
                TcpConnectClient();

                return false;
            }
        }

        public bool SendhandShake(int Ascii0)
        {
            if (sckServer == null)
            {
                return true;
            }

            byte[] SendArray = new byte[3] { 0, 0, 0 };   //发送命令字节数组
            SendArray[0] = (byte)Ascii0;
            bool blockingState = sckServer.Blocking;
            
            try
            {
                sckServer.Blocking = false;
                int cnt = sckServer.Send(SendArray);
                return true; //若Send错误会跳去执行catch体，而不会执行其try体里其之后的代码
            }
            catch (SocketException e)
            {
                // 10035 == WSAEWOULDBLOCK
                if (e.NativeErrorCode.Equals(10035))
                {
                    //Console.WriteLine("Still Connected, but the Send would block");
                    return true;
                }

                else
                {
                    //Console.WriteLine("Disconnected: error code {0}!", e.NativeErrorCode);
                    return false;
                }
            }
            finally
            {
                sckServer.Blocking = blockingState;
            }
        }


        // 字符串（16进制报文）转化为byte二进制发送
        public static byte[] HexStringToByteArray(string str)
        {
            str = str.Replace(" ", "");
            byte[] buffer = new byte[str.Length / 2];
            for (int i = 0; i < str.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(str.Substring(i, 2), 16);
            return buffer;
        }

        // 把int32类型的数据转存到4个字节的byte数组中
        private static bool ConvertIntToByteArray(Int32 m, ref byte[] arry)
        {
            if (arry == null) return false;
            if (arry.Length < 4) return false;

            arry[0] = (byte)(m & 0xFF);
            arry[1] = (byte)((m & 0xFF00) >> 8);
            arry[2] = (byte)((m & 0xFF0000) >> 16);
            arry[3] = (byte)((m >> 24) & 0xFF);

            return true;
        }
        //合并两个字节数组
        private static byte[] addBytes(byte[] data1, byte[] data2, int validLen)
        {
            if (validLen <= 0)
            {
                throw new Exception("Data2 cannot be null!");
            }

            if (data1 == null || data1.Length == 0)
            {
                byte[] buffer = new byte[validLen];
                Buffer.BlockCopy(data2, 0, buffer, 0, validLen);
                return buffer;
            }

            byte[] data3 = new byte[data1.Length + validLen];
            Buffer.BlockCopy(data1, 0, data3, 0, data1.Length);
            Buffer.BlockCopy(data2, 0, data3, data1.Length, validLen);
            return data3;
        }
        //// 二进制数组转字符串（16进制报文）
        //private static string byteArrayToHexString(byte[] bytes, int len)
        //{
        //    string returnStr = "";
        //    if (bytes != null)
        //    {
        //        for (int i = 0; i < len - 1; i++)
        //        {
        //            returnStr += bytes[i].ToString("X2");
        //            returnStr += " ";
        //        }
        //        returnStr += bytes[len - 1].ToString("X2");
        //    }
        //    return returnStr;
        //}
        //// 发送并接受
        //private string sendMessage(string msg)
        //{
        //    if (client == null)
        //    {
        //        Console.WriteLine("Send Error : TcpConnection has not be established");
        //        return "Error";
        //    }
        //    try
        //    {
        //        byte[] cvtMsg = HexStringToByteArray(msg);
        //        stream = client.GetStream();
        //        stream.Write(cvtMsg, 0, cvtMsg.Length);
        //        cvtMsg = new byte[16];
        //        string recvMsg = string.Empty;
        //        int bytes = stream.Read(cvtMsg, 0, cvtMsg.Length);
        //        recvMsg = byteArrayToHexString(cvtMsg, bytes);
        //        return recvMsg;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Send Tcp Fail for : " + e.ToString());
        //        client = null;
        //    }
        //    return "Error";
        //}
        // 发送数据
        //private void MyMessageBox()
        //{
        //    MessageBox.Show("Tcp发送数据失败！", "错误信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
        //}
        //private void ShowMessageBox()
        //{
        //    Thread TCP = new Thread(() => MyMessageBox());
        //    TCP.Start();
        //}


        //老协议，本端作为服务端，发送给客户端
        public bool SendData_Old(byte[] cvtMsg, string msg)
        {
            if (sckServer == null)
                return false;

            try
            {
                //Console.WriteLine("Tcp判断是否支持写入发送数据");
                //if (stream.CanWrite)//获取一个值，该值指示 NetworkStream 是否支持写入。 
                //{
                //Console.WriteLine("Tcp开始发送箱号");
                sckServer.Send(cvtMsg);//将数据写入 NetworkStream 
                //Console.WriteLine("Tcp发送成功");
                //if(InitialValue.Network.IsRecordSendBoxLog == 1 && !msg.Equals("05 00 00"))
                if (!msg.Equals("05 00 00"))
                    WriteLogFile("发送数据成功！"+msg);
                //Console.WriteLine("Tcp清空缓存区数据");
                //}

                //Console.WriteLine("TCP发送结束");
            }
            catch (Exception e)
            {
                sckServer?.Close();
                sckServer = null;
                WriteLogFile("发送数据失败");
                //Console.WriteLine("Tcp发送数据失败");
                return false;
            }

            //if (!msg.Equals("05 00 00"))
            //{
            //    //WriteLogFile("Tcp发送数据失败");
            //}

            return true;
        }

        ////软筑协议,作为客户端向服务端发送数据
        public bool SendData_New(string Data, int flag)
        {
            TcpConectServerRetry();

            if (client == null)
            {
                Console.WriteLine("Send Error : Tcp Connection has not be established");
                VideoForm.tcpMessage = "等待连接！";
                return false;
            }

            try
            {
                string AgreementHeader = "E2 5C 4B 89";                     //包头，固定值：0xE2 0x5C 0x4B 0x89
                //string TotalLength = "00 00 00 00 26";                    //总长
                string MessageCode = "41";                                  //报文代码，0x41
                string StationNumber = InitialValue.Network.StationNumber;  //场站号，由海关指定  00 00 00 00 00 00 00 00 00 00
                string ChannelNumber = InitialValue.Network.ChannelNumber;  //通道号，由海关指定  00 00 00 00 00 00 00 00 00 00
                string InOut_Signal = "49";                                 //进出标志：进场卡口I:0x49, 出场卡口E:0x45
                string Identifier = "FF FF FF FF";                          //标识符：0xFF 0xFF 0xFF 0xFF
                //string XMLLength = "00 00 00 00";                         //XML流长度：XML格式数据的长度
                string AgreementEnd = "FF FF";                              //包尾：固定值，0xFF 0xFF

                byte[] Header = HexStringToByteArray(AgreementHeader);
                byte[] Second = HexStringToByteArray(MessageCode+ StationNumber+ ChannelNumber+ InOut_Signal+ Identifier);
                byte[] End = HexStringToByteArray(AgreementEnd);

                byte[] BData = System.Text.Encoding.Default.GetBytes(Data);    //XML格式数据
                int XMLLength = BData.Length;
                byte[] xml_length = new byte[4];
                byte[] TotalLength = new byte[4];
                ConvertIntToByteArray(XMLLength, ref xml_length);
                ConvertIntToByteArray(XMLLength+38, ref TotalLength);

                byte[] cvtMsg = addBytes(Header, TotalLength, TotalLength.Length);
                cvtMsg = addBytes(cvtMsg, Second, Second.Length);
                cvtMsg = addBytes(cvtMsg, xml_length, xml_length.Length);
                cvtMsg = addBytes(cvtMsg, BData, BData.Length);
                cvtMsg = addBytes(cvtMsg, End, End.Length);

                stream = client.GetStream();
                if (stream.CanWrite)//获取一个值，该值指示 NetworkStream 是否支持写入。 
                {
                    stream.Write(cvtMsg, 0, cvtMsg.Length);//将数据写入 NetworkStream
                    stream.Flush();
                    WriteLogFile("发送数据成功:" + Data);
                }
                stream.Close();
                client.Close();
                //cvtMsg = new byte[16];
                //string recvMsg = string.Empty;
                //int bytes = stream.Read(cvtMsg, 0, cvtMsg.Length);
                //recvMsg = byteArrayToHexString(cvtMsg, bytes);
                //return recvMsg;
            }
            catch (Exception e)
            {
                //MessageBox.Show("发送数据失败！");
                //if(TcpShowFlag == true)
                //if(flag==0)
                //{

                //    ShowMessageBox();
                //    //TcpShowFlag = false;
                //}
                WriteLogFile("发送数据失败");
                //MessageBox.Show("发送数据失败！", "错误信息", MessageBoxButtons.OK, MessageBoxIcon.Error);
                client = null;
                return false;
            }
            return true;
            //return "Error";
        }

        //写日志文件
        private void WriteLogFile(string message)
        {   
            if (InitialValue.Network.IsRecordSendBoxLog == 1)
            {

                ContainerHelper.saveLogFile(message);
                    //string LogFileName = ".\\" + DateTime.Now.ToString("yyyy-MM-") + "LogFile.log";
                //if (!File.Exists(LogFileName))//判断日志文件是否为当月
                //    File.Create(LogFileName).Close();//创建文件
                //System.IO.StreamWriter writer = File.AppendText(LogFileName);//文件中添加文件流
                //writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + message);
                //writer.Flush();
                //writer.Close();
            }

            VideoForm.tcpMessage = DateTime.Now.ToString("HH:mm") + " " + message;
        } 
    }
}

