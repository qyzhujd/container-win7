﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using OpenCvSharp;

namespace HKTest
{
    public struct AppSettings
    {
        //通用配置
        public string GateName;    //卡口名称
        
        public string ImagePath;   //图像存盘路径

        public string ImagePath1;   //存空车和社会车辆图像，如果为非空，则存储和发送

        public string RegNo;

        public int SaveFlag;              //是否存储采集图像，0:不存储， 1：存储， 2：h986存储方式

        public int RepeatRecogFlag;      //是否允许同箱号重复识别，0：不允许，1：允许

        public int FrameRateFlag;        //处理帧率标志，=0 实时，=1 降半（用于Win7）

        public int FrontFrameInterval;         //前侧采集两帧间隔帧数

        public int FrontTimeout;    //前侧采集到图像后后侧没有触发，则一定时间后放弃，单位是秒
    }

    public struct NetworkSettings
    {
        public int ReleasePort;    //前台放行程序端口等，本机作为服务器等待连接
        public int P1SendDuration;        //用于重发P1报文的时间间隔,以毫秒为单位，0不重发

        public string ServerIP;         //服务器IP地址和端口号（软筑协议），本机作为客户端，连接该服务端
        public int ServerPort;

        public string PrefixInformation;         //发送箱号信息的前缀

        public string StationNumber;    //场站号，由海关指定
        public string ChannelNumber;    //通道号，由海关指定

        public int SendProtocalType;         //发送箱号协议类型
        public int IsRecordSendBoxLog;       //是否记录发送箱号的日志

        public int ErrorNoSend;    //识别错误时是否自动发送箱号，1：发送，0：不发送
    }

    public struct VideoSettings
    {
        //视频流地址
        public string VideoURL;
        //校正参数
        public string CalibParas1;
        public Mat warpMatrix1;
        public int leftMargin1;
        public int widthMargin1;
        public string CalibParas2;
        public Mat warpMatrix2;
        public int leftMargin2;
        public int widthMargin2;
        public double x1, y1, x2, y2;  //箱号左上、右下位置的百分比
    }


    public struct RecogPara
    {
        public string FileName;
        public string RecogString;
        public int TypeNo;
        public string Type;
    }

    public struct ModelDir
    {
        public string DetectionPlace;
        public string DetectionOCR;
        public string RecognitionOCR;

        public string BaseDir;
    }

    //从配置文件中初始化一些参数
    public static class InitialValue
    {
        //程序通用设置
        public static AppSettings Settings;
        //网络通信配置
        public static NetworkSettings Network;
        //摄像头配置
        public static VideoSettings[] Video;
        //视频流读取模式
        public static string ReadMode;
        public static ModelDir modelDir;

        public static void ReadInit()
        {
            string FILE_NAME = ".\\RecogCS.ini";

            if (!File.Exists(FILE_NAME))
            {
                MessageBox.Show("无初始化文件，程序退出！");
                System.Environment.Exit(0);
            }

            IniFile Ini = new IniFile(FILE_NAME);


            Settings.RegNo = Ini.GetString("App", "RegNo", "");
            Settings.GateName = Ini.GetString("App", "Name", "");
            Settings.ImagePath = Ini.GetString("App", "ImagePath", "");
            Settings.ImagePath1 = Ini.GetString("App", "ImagePath1", "");
            Settings.RepeatRecogFlag = Ini.GetInt("App", "RepeatRecogFlag", 0);
            Settings.SaveFlag = Ini.GetInt("App", "SaveFlag", 0);
            Settings.FrameRateFlag = Ini.GetInt("App", "FrameRateFlag", 0);
            
            string tmp = CheckDiskState(true);

            if ((InitialValue.Settings.ImagePath == "") || (InitialValue.Settings.ImagePath == null))
            {
                InitialValue.Settings.ImagePath = Path.Combine(Environment.CurrentDirectory, "ImgSave");
            }
            Settings.FrontFrameInterval = Ini.GetInt("App", "FrontFrameInterval", 1);
            Settings.FrontTimeout = Ini.GetInt("App", "FrontTimeout", 10);
            

            //读取摄像头参数
            Video = new VideoSettings[2];
            for (int i = 0; i < 2; i++)
            {
                Video[i].VideoURL = Ini.GetString("Video", "VideoURL" + i.ToString(), "");
            }
            Video[0].CalibParas1 = Ini.GetString("Video", "CalibParas00", "");  //后侧
            Video[0].warpMatrix1 = getWarpMatrix(Video[0].CalibParas1);
            Video[0].leftMargin1 = -1;
            Video[0].CalibParas2 = Ini.GetString("Video", "CalibParas01", "");
            Video[0].warpMatrix2 = getWarpMatrix(Video[0].CalibParas2);
            Video[0].leftMargin2 = -1;

            Video[0].x1 = Convert.ToDouble(Ini.GetString("Video", "BackX1", "0.15"));
            Video[0].y1 = Convert.ToDouble(Ini.GetString("Video", "BackY1", "0.1"));
            Video[0].x2 = Convert.ToDouble(Ini.GetString("Video", "BackX2", "0.85"));
            Video[0].y2 = Convert.ToDouble(Ini.GetString("Video", "BackY2", "0.95"));

            Video[1].CalibParas1 = Ini.GetString("Video", "CalibParas10", "");  //前侧
            Video[1].warpMatrix1 = getWarpMatrix(Video[1].CalibParas1);
            Video[1].leftMargin1 = -1;
            Video[1].CalibParas2 = Ini.GetString("Video", "CalibParas11", "");
            Video[1].warpMatrix2 = getWarpMatrix(Video[1].CalibParas2);
            Video[1].leftMargin2 = -1;

            Video[1].x1 = Convert.ToDouble(Ini.GetString("Video", "FrontX1", "0.15"));
            Video[1].y1 = Convert.ToDouble(Ini.GetString("Video", "FrontY1", "0.1"));
            Video[1].x2 = Convert.ToDouble(Ini.GetString("Video", "FrontX2", "0.85"));
            Video[1].y2 = Convert.ToDouble(Ini.GetString("Video", "FrontY2", "0.95"));


            //识别模型目录
            modelDir.DetectionPlace = @".\Model\ppyolo_r18vd_coco";
            modelDir.DetectionOCR = @".\Model\det_inference/";
            modelDir.RecognitionOCR = @".\Model\ppocr_v3_rec_en";
            modelDir.BaseDir = @".\Model";

            //读取联网信息
            Network.ReleasePort = Ini.GetInt("Network", "ReleasePort", 8001);
            //Network.RelPort = Ini.GetInt("Network", "RelPort", 8002);
            Network.P1SendDuration = Ini.GetInt("Network", "P1SendDuration", 0);

            Network.ServerIP = Ini.GetString("Network", "ServerIP", "");
            Network.ServerPort = Ini.GetInt("Network", "ServerPort", 0);

            Network.PrefixInformation = Ini.GetString("Network", "PrefixInformation", "");
            Network.StationNumber = Ini.GetString("Network", "StationNumber", "");
            Network.ChannelNumber = Ini.GetString("Network", "ChannelNumber", "");

            Network.SendProtocalType = Ini.GetInt("Network", "SendProtocalType", 0);
            Network.IsRecordSendBoxLog = Ini.GetInt("Network", "IsRecordSendBoxLog", 0);
            Network.IsRecordSendBoxLog = 1;  //必须保存，否则无法统计当日集卡数！

           Network.ErrorNoSend = Ini.GetInt("Network", "ErrorNoSend", 1);            
        }


        //看存图像的磁盘是否存在或满了
        //msgShowFlag=true,直接显示退出！否则返回提示信息
        public  static string CheckDiskState(Boolean msgShowFlag)
        {
            if (Settings.SaveFlag > 0)
            {
                if (!Directory.Exists(Settings.ImagePath))  //判断是否存在
                {
                    MessageBox.Show("图像存盘目录不存在，请检查存盘路径是否正常！");
                    System.Environment.Exit(0);
                }

                DriveInfo[] driveInfos = DriveInfo.GetDrives();
                foreach (DriveInfo item in driveInfos)
                {
                    if (item.IsReady)
                    {
                        if (item.Name == Settings.ImagePath.Substring(0, 1).ToUpper() + @":\")
                        {
                            long diskspace = item.AvailableFreeSpace / 1024 / 1024; //指示驱动器上的可用空闲空间总量（以M为单位）。
                            if (diskspace < 1024)
                            {
                                if (msgShowFlag)
                                {
                                    MessageBox.Show("图像存盘目录剩余空间小于1G，请清理磁盘空间！");
                                    System.Environment.Exit(0);
                                }
                                else
                                    return "图像存盘目录剩余空间小于1G，请清理磁盘空间！";
                            }
                        }
                    }
                }
            }

            return "";
        }

        public static Mat getWarpMatrix(string calibString)
        {
            Point2f[] srcPt = new Point2f[4];
            Point2f[] dstPt = new Point2f[4];
            Mat warpmatrix = null; 

            try
            {
                if (calibString != "")
                {
                    //string calbString = "109,94,539,87,448,370,165,366,91,95,518,91,436,374,167,370";//正
                    //string calbString = "108,94,540,87,450,368,167,365,162,93,595,88,465,367,182,362";//右偏

                    int[] numbers = calibString.Split(',')
                                        .Select(s => int.Parse(s))
                                        .ToArray();

                    srcPt[0] = new Point2f(numbers[0], numbers[1]);
                    srcPt[1] = new Point2f(numbers[2], numbers[3]);
                    srcPt[2] = new Point2f(numbers[4], numbers[5]);
                    srcPt[3] = new Point2f(numbers[6], numbers[7]);
                    dstPt[0] = new Point2f(numbers[8], numbers[9]);
                    dstPt[1] = new Point2f(numbers[10], numbers[11]);
                    dstPt[2] = new Point2f(numbers[12], numbers[13]);
                    dstPt[3] = new Point2f(numbers[14], numbers[15]);

                    warpmatrix = Cv2.GetPerspectiveTransform(srcPt, dstPt);
                }

                return warpmatrix;
            }
            catch (Exception ee)
            {
                MessageBox.Show("图像校正参数错误:"+calibString);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
                return warpmatrix;
                //throw new Exception(ee.ToString());
            }
        }
    }
}
