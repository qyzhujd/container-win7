////////////////////////////////////////////////////////////////////
// Copyright (C) 2012 SafeNet, Inc. All rights reserved.
//
// SuperDog(R) is a trademark of SafeNet, Inc.
//
//
////////////////////////////////////////////////////////////////////
using System;
using System.Windows.Forms;
using System.Collections.Specialized;
using System.Text;
using System.Runtime.InteropServices;
using SuperDog;


namespace DogDemo
{
    /// <summary>
    /// </summary>
    public class DogDemo
    {
        private const string vendorCodeString_Demo =
            "AzIceaqfA1hX5wS+M8cGnYh5ceevUnOZIzJBbXFD6dgf3tBkb9cvUF/Tkd/iKu2fsg9wAysYKw7RMA" +
            "sVvIp4KcXle/v1RaXrLVnNBJ2H2DmrbUMOZbQUFXe698qmJsqNpLXRA367xpZ54i8kC5DTXwDhfxWT" +
            "OZrBrh5sRKHcoVLumztIQjgWh37AzmSd1bLOfUGI0xjAL9zJWO3fRaeB0NS2KlmoKaVT5Y04zZEc06" +
            "waU2r6AU2Dc4uipJqJmObqKM+tfNKAS0rZr5IudRiC7pUwnmtaHRe5fgSI8M7yvypvm+13Wm4Gwd4V" +
            "nYiZvSxf8ImN3ZOG9wEzfyMIlH2+rKPUVHI+igsqla0Wd9m7ZUR9vFotj1uYV0OzG7hX0+huN2E/Id" +
            "gLDjbiapj1e2fKHrMmGFaIvI6xzzJIQJF9GiRZ7+0jNFLKSyzX/K3JAyFrIPObfwM+y+zAgE1sWcZ1" +
            "YnuBhICyRHBhaJDKIZL8MywrEfB2yF+R3k9wFG1oN48gSLyfrfEKuB/qgNp+BeTruWUk0AwRE9XVMU" +
            "uRbjpxa4YA67SKunFEgFGgUfHBeHJTivvUl0u4Dki1UKAT973P+nXy2O0u239If/kRpNUVhMg8kpk7" +
            "s8i6Arp7l/705/bLCx4kN5hHHSXIqkiG9tHdeNV8VYo5+72hgaCx3/uVoVLmtvxbOIvo120uTJbuLV" +
            "TvT8KtsOlb3DxwUrwLzaEMoAQAFk6Q9bNipHxfkRQER4kR7IYTMzSoW5mxh3H9O8Ge5BqVeYMEW36q" +
            "9wnOYfxOLNw6yQMf8f9sJN4KhZty02xm707S7VEfJJ1KNq7b5pP/3RjE0IKtB2gE6vAPRvRLzEohu0" +
            "m7q1aUp8wAvSiqjZy7FLaTtLEApXYvLvz6PEJdj4TegCZugj7c8bIOEqLXmloZ6EgVnjQ7/ttys7VF" +
            "ITB3mazzFiyQuKf4J6+b/a/Y";

        /*private const string vendorCodeString =
            "Fo69ORmxab5PLEJBxw46G6Joe3CnYUyhaXms38Gt1mYa7qkcFUayvfyjpDc0VWuZyA0SfzlbuEnZMj"+
            "shX983yeIx2zXiwmLgUM87NX8btcVGh9FMqjavRaUZYR3aag+/xilaAqV3iZ1Zf8P9y1r7kCqnEcfp"+
            "kkn4yrAgM0OH8MEDyp5w57CYd8gmrk5whcn4VX7t4LmKm/Z2LoY0PbRqQi7PoR6NLJssqj6PqXjrfs"+
            "e4wPBJbgN/wodfWOicZMmuq71oiAEdM/RQdzWSggYrU4smYNnUJPCCiyteeZ8gEHY7qz5ksXYIANwd"+
            "YVkOwO9Ztp/PGsxkrNJJwURXhHq7GhxOYMiMcay/cXlNcpNmIqroMRzIL36hmOiKzjRfaXGC4hTdQO"+
            "vsajMypH6bDwsrqO3KJ935QmgTRp4nEfsHeEr/k8nmSoAttNekkpzWmHqaevVdBMicssOjghEanBRd"+
            "AIuZzsg40cbbHuF8kj3TCof7+lzMXECT0RFKL+psCFy7M9paOcNIAdrOMmASNW9ba0i60Qcck7+hBU"+
            "8beFFurWMjQy704g5nndpCF4czxRsXNY093lDQMQGRn8hsDKuWcUbl1UYvX0hH6r42QeMWKzpeSIOs"+
            "VA0tscovp0vjYeXId1DGqbJW/NuEtKKDU98vPKzlfTyxalp1YpHAfrM7c/wT/57ocORec6RVR8jsMB"+
            "D2xWn0PpFE1v9w+gVCEvWc0mngauGO8pmYrRlHWB65fPrrConMchQmioy0Q6p9Bz+WFfoRViiBSVVS"+
            "jU6ZZ8uLpCBPrA6Y2tQ5e/a7ouvB4KPkp4UAQcHHTjZ1fFQE8NdxM7QC5AHIlFi/SSTbb92xO1FTsC"+
            "Ak/B7pNS32AWuO66dMKU7Ii+/Wwhd3tmr8bcsQbeK9SPrVpsLar5/+11Jb6Hp4Ir16e+y1yATRRp5V"+
            "061TZAJWIut55xjLqwrWEg9Loa9O/EWiC3lUEJYhVTJ44A==";
        */
        private const string vendorCodeString = 
            "43v+iQVXFn1hDXWXpK2Jvi8PvfEdq1iDj1ObG1O2CrtcTgtBj/9U9lXBVrZc9QsU+rNV85SVpf4CSS"+
            "kNYZMDyh7ky22PG6oeczL2E1s9QDe/+iJRQImk0KRjZ4Lq7M4WTE15lc2mceSh5Ybi0gk42/HelPW5"+
            "VEdbr0iEEW7GBs/7GpDN+ad63J0LqILaT6Kss/kJddZRrP6CAmN+zmXQzR5mby3nydfvP8+CFJbWqB"+
            "/q3H3H2vqXe42Oqb1l4eN6QFY28gsofOp9j0oE4c1IVh2ua2dKbAuL4FOKF/V8g2E5fqtd19tLEc9m"+
            "//njix/nKvJE14OG/ZI/WCX9OzLpkaIXJ29rIHA32Jb4zmaYINB6xHc1y4u4KWS22COxIaJktD+kJP"+
            "5WY2GgsU1QGOgZnVpkUNwrP3n01Nf6qpm1chg9co5IxoIeGLpr/ppnW+Ts4xOggzKMyud/vThZ6fMd"+
            "/+DuxXjwWT0Sg1M10I0hDU0T4D83+AdbVPkglCbjApwYnyqenrmIcx7eKybT9XwUMPl34pMctgDo8k"+
            "dkJXf6qHHICAZpl10wbuQ/YffDpWTzfdCZDBXFkoKQcuu7GuBHcN9ioNglow6RUsSOSHCibnbSc5JK"+
            "AY1vPablz3IdJC7OhUkH4kqIxLqOSnpZXTHxyHLDiNwSReCCghu+P9OJwZeEmnukLpo3JboyiPPaod"+
            "2QaQEvOQJEjd4sfonjtL9PdUCIOjzcZ7BV4R5x9gkG6PTHN0twkl+3Uj59OrqT7xVefdAdM1as75JS"+
            "XPcbHmZa9m4s9eBftrXZ6ok70rh/+ktAHjYwzd9UK0giLGjorzzV/SkBhvFptku46/7THjnRvTpJaW"+
            "R4tTqZPgHUTpelF/rTrNVQ9urFcnBcCM+rpNPF/gGH+tRH+Qn7tSH0QJyA1LfSap+0gHOE/fnektlG"+
            "h4m7SEGl85DZIRD96Y5dXVh+kmyoPp1fVd9phn5uDTjteA==";
        
        
        public const int FileId = 0xfff4;    // file id of default read/write file

        public const string defaultScope = "<dogscope />";

        private string scope;

        /// <summary>
        /// Constructor
        /// Intializes the object.
        /// </summary>
        public DogDemo()
        {
        }


        /// <summary>
        /// Demonstrates the usage of the AES encryption and
        /// decryption methods.
        /// </summary>
        protected void EncryptDecryptDemo(Dog dog)
        {
            if ((null == dog) || !dog.IsLoggedIn())
                return;

            // the string to be encryted/decrypted.
            string text = "SuperDog is great";

            // convert the string into a byte array.
            byte[] data = UTF8Encoding.Default.GetBytes(text);

            // encrypt the data.
            DogStatus status = dog.Encrypt(data);

            if (DogStatus.StatusOk == status)
            {
                text = UTF8Encoding.Default.GetString(data);

                // decrypt the data.
                // on success we convert the data back into a 
                // human readable string.
                status = dog.Decrypt(data);

                if (DogStatus.StatusOk == status)
                {
                    text = UTF8Encoding.Default.GetString(data);
                }
            }

            // Second choice - encrypting a string using the 
            // native .net API
            text = "Encrypt/Decrypt String";

            status = dog.Encrypt(ref text);

            if (DogStatus.StatusOk == status)
            {
                status = dog.Decrypt(ref text);

                if (DogStatus.StatusOk == status)
                {
                    //Verbose("Decrypted string: \"" + text + "\"");
                }
            }
        }

        /// <summary>
        /// Demonstrates how to perform a login and an automatic
        /// logout using C#'s scope clause.
        /// </summary>
        protected void LoginDefaultAutoDemo()
        {
            DogFeature feature = DogFeature.Default;

            // this will perform a logout and object disposal
            // when the using scope is left.
            
            scope = defaultScope;
            
            using (Dog dog = new Dog(feature))
            {
                DogStatus status = dog.Login(vendorCodeString, scope);
            }
        }


        /// <summary>
        /// Demonstrates how to login into a dog using the
        /// default feature. The default feature is ALWAYS 
        /// available in every SuperDog.
        /// </summary>
        protected Dog LoginDefaultDemo()
        {
            DogFeature feature = DogFeature.Default;

            Dog dog = new Dog(feature);
            
            scope = defaultScope;

            DogStatus status = dog.Login(vendorCodeString, scope);

            // Please note that there is no need to call
            // a logout function explicitly - although it is
            // recommended. The garbage collector will perform
            // the logout when disposing the object.
            // If you need more control over the logout procedure
            // perform one of the more advanced tasks.
            return dog.IsLoggedIn() ? dog : null;
        }


        /// <summary>
        /// Demonstrates how to login using a feature id.
        /// </summary>
        protected Dog LoginDemo(DogFeature feature)
        {
            // create a dog object using a feature
            // and perform a login using the vendor code.
            Dog dog = new Dog(feature);

            DogStatus status = dog.Login(vendorCodeString, scope);

            return dog.IsLoggedIn() ? dog : null;
        }


        /// <summary>
        /// Demonstrates how to perform a login using the default
        /// feature and how to perform an automatic logout
        /// using the SuperDog's Dispose method.
        /// </summary>
        protected void LoginDisposeDemo()
        {
            DogFeature feature = DogFeature.Default;

            Dog dog = new Dog(feature);

            DogStatus status = dog.Login(vendorCodeString, scope);

            dog.Dispose();
        }


        /// <summary>
        /// Demonstrates how to perform a login and a logout
        /// using the default feature.
        /// </summary>
        protected void LoginLogoutDefaultDemo()
        {
            DogFeature feature = DogFeature.Default;

            Dog dog = new Dog(feature);

            DogStatus status = dog.Login(vendorCodeString, scope);

            if (DogStatus.StatusOk == status)
            {
                status = dog.Logout();
            }

            // recommended, but not mandatory
            // this call ensures that all resources to SuperDog
            // are freed immediately.
            dog.Dispose();
        }


        /// <summary>
        /// Performs a logout operation
        /// </summary>
        protected void LogoutDemo(ref Dog dog)
        {
            if ((null == dog) || !dog.IsLoggedIn())
                return;

            DogStatus status = dog.Logout();

            // get rid of the dog immediately.
            dog.Dispose();
            dog = null;
        }


        /// <summary>
        /// Demonstrates how to perform read and write
        /// operations on a file in SuperDog
        /// </summary>
        protected void ReadWriteDemo(Dog dog, Int32 fileId)
        {
            if ((null == dog) || !dog.IsLoggedIn())
                return;

            // Get a file object to a file in SuperDog.
            // please note: the file object is tightly connected
            // to its dog object. logging out from a dog also
            // invalidates the file object.
            // doing the following will result in an invalid
            // file object:
            // dog.login(...)
            // DogFile file = dog.GetFile();
            // dog.logout();
            // Debug.Assert(file.IsValid()); will assert
            DogFile file = dog.GetFile(fileId);
            if (!file.IsLoggedIn())
            {
                // Not logged into a dog - nothing left to do.
                return;
            }

            // get the file size
            int size = 0;
            DogStatus status = file.FileSize(ref size);

            if (DogStatus.StatusOk != status)
            {
                return;
            }

            // read the contents of the file into a buffer
            byte[] bytes = new byte[size];

            status = file.Read(bytes, 0, bytes.Length);

            if (DogStatus.StatusOk != status)
            {
                return;
            }

            // now let's write some data into the file
            byte[] newBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7 };

            status = file.Write(newBytes, 0, newBytes.Length);
            if (DogStatus.StatusOk != status)
            {
                return;
            }

            // and read them again
            status = file.Read(newBytes, 0, newBytes.Length);
            if (DogStatus.StatusOk == status)
                //DumpBytes(newBytes);

            // restore the original contents
            file.Write(bytes, 0, bytes.Length);
        }


        /// <summary>
        /// Demonstrates how to read and write to/from a
        /// file at a certain file position
        /// </summary>
        protected void ReadWritePosDemo(Dog dog, Int32 fileId)
        {
            if ((null == dog) || !dog.IsLoggedIn())
                return;

            // firstly get a file object to a file.
            DogFile file = dog.GetFile(fileId);
            if (!file.IsLoggedIn())
            {
                // Not logged into dog - nothing left to do.
                return;
            }

            // we want to write an int at the end of the file.
            // therefore we are going to 
            // - get the file's size
            // - set the object's read and write position to
            //   the appropriate offset.
            int size = 0;
            DogStatus status = file.FileSize(ref size);

            if (DogStatus.StatusOk != status)
            {
                return;
            }

            // set the file pos to the end minus the size of int
            file.FilePos = size - DogFile.TypeSize(typeof(int));

            // now read what's there
            int aValue = 0;
            status = file.Read(ref aValue);

            if (DogStatus.StatusOk != status)
            {
                return;
            }

            // write some data.
            status = file.Write(int.MaxValue);

            if (DogStatus.StatusOk != status)
            {
                return;
            }

            // read back the written value.
            int newValue = 0;
            status = file.Read(ref newValue);

            if (DogStatus.StatusOk == status)
            {
                //Verbose("Data read: 0x" + newValue.ToString("X2"));
            }

            // restore the original data.
            file.Write(aValue);
        }


        /// <summary>
        /// Demonstrates how to get current time and date of 
        /// a SuperDog when available.
        /// </summary>
        protected void TestTimeDemo(Dog dog)
        {
            if ((null == dog) || !dog.IsLoggedIn())
                return;

            DateTime time = DateTime.Now;
            DogStatus status = dog.GetTime(ref time);

            if (DogStatus.StatusOk == status)
            {
                //Verbose("Time and date is " + time.ToString());
            }
        }


        public bool DogVerify()
        {
            Dog dog = LoginDefaultDemo();
            
            if ((null == dog) || !dog.IsLoggedIn())
                return false;

            // the string to be encryted/decrypted.
            //string text = "SuperDog is great";
            string textIn = DateTime.Now.ToString();
            string textOut;

            // convert the string into a byte array.
            byte[] data = UTF8Encoding.Default.GetBytes(textIn);

            // encrypt the data.
            DogStatus status = dog.Encrypt(data);

            if (DogStatus.StatusOk == status)
            {
                textOut = UTF8Encoding.Default.GetString(data);

                // decrypt the data.
                // on success we convert the data back into a 
                // human readable string.
                status = dog.Decrypt(data);

                if (DogStatus.StatusOk == status)
                {
                    textOut = UTF8Encoding.Default.GetString(data);

                    if (textIn == textOut)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Runs the API demo.
        /// </summary>
        public void RunDemo(string scope)
        {
            try
            {
                this.scope = scope;

                // Demonstrate the different login methods
                LoginDefaultAutoDemo();
                LoginLogoutDefaultDemo();
                LoginDisposeDemo();

                // Demonstrates how to get a list of available features
                GetInfoDemo();

                // run the API demo using the default feature
                // (ALWAYS present in every SuperDog)
                Dog dog = LoginDefaultDemo();
                SessionInfoDemo(dog);
                ReadWriteDemo(dog, FileId);
                ReadWritePosDemo(dog, FileId); 
                EncryptDecryptDemo(dog);
                TestTimeDemo(dog);
                LogoutDemo(ref dog);

                // run the API demo using feature id 42
                dog = LoginDemo(new DogFeature(DogFeature.FromFeature(42).Feature));
                SessionInfoDemo(dog);
                ReadWriteDemo(dog, FileId);
                ReadWritePosDemo(dog, FileId);
                EncryptDecryptDemo(dog);
                TestTimeDemo(dog);
                LogoutDemo(ref dog);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Demonstrates how to use to retrieve a XML containing all available features.
        /// </summary>
        protected void GetInfoDemo()
        {
            string queryFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                                                      "<dogformat root=\"dog_info\">" +
                                                        " <feature>" +
                                                        "  <attribute name=\"id\" />" +
                                                        "  <element name=\"license\" />" +
                                                        " </feature>" +
                                                        "</dogformat>";

            string info = null;

            DogStatus status = Dog.GetInfo(scope, queryFormat, vendorCodeString, ref info);

            if (DogStatus.StatusOk == status)
            {
                //Verbose("Fature Information:");
                //Verbose(info.Replace("\n", "\r\n          "));
            }
            else
            {
                //Verbose("");
            }
        }


        /// <summary>
        /// Demonstrates how to retrieve information from a dog.
        /// </summary>
        protected void SessionInfoDemo(Dog dog)
        {
            if ((null == dog) || !dog.IsLoggedIn())
                return;

            // firstly we will retrieve the dog info.
            string info = null;
            DogStatus status = dog.GetSessionInfo(Dog.KeyInfo,
                                                    ref info);
            if (DogStatus.StatusOk == status)
            {
                //Verbose("SuperDog Information:");
                //Verbose(info.Replace("\n", "\r\n          "));
            }
            else
                //Verbose("");

            // next the session info.
            status = dog.GetSessionInfo(Dog.SessionInfo, ref info);
            if (DogStatus.StatusOk == status)
            {
                //Verbose("Session Information:");
                //Verbose(info.Replace("\n", "\r\n          "));
            }
            else
            {
                //Verbose("");
            }
            // last the update information.
            status = dog.GetSessionInfo(Dog.UpdateInfo, ref info);
            if (DogStatus.StatusOk == status)
            {
                //Verbose("Update Information:");
                //Verbose(info.Replace("\n", "\r\n          "));
            }
            else
            {
                //Verbose("");
            }
        }
    }
}
