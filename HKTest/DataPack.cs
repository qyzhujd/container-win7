﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Xml;

namespace HKTest
{
    public struct P1Type0    
    {
        public string RecogFlag;
        public string Channel;
        public string Type;
        public string No1;
        public string No2;
        public string RelState;
        public string Operator;
        //RecogFlag As String * 1
        //channel As String * 2
        //type As String * 1
        //No1 As String * 12
        //No2 As String * 12
        //relstate As String * 1
        //operator As String * 4
    }

    public struct P1Type
    {
        public string RecogFlag;
        public string Channel;
        public string Type;
        public string No1;
        public string No2;
        public string TypeNo1;
        public string TypeNo2;
        public string RelState;
        public string Operator;

        public string F_ImagePath;
        public string FL_ImagePath;
        public string FR_ImagePath;
        public string B_ImagePath;
        public string BL_ImagePath;
        public string BR_ImagePath;

        public string ImageFiles;    //2014年8月1日新增
    }

    public struct P2Type
    {        
        public string Channel;
        public string Type;
        public string No1;
        public string No2;
        public string RelState;
        public string Operator;
        public string RelTime;
        public string Weight;
        public string CarNo;
        public string SealNo1;
        public string SealNo2;
        public string Level;    //新增布控级别， 0、1、2，其含义依次为：正常，警告，报警

        //channel As String * 2
        //type As String * 1
        //No1 As String * 11
        //No2 As String * 11
        //relstate As String * 1
        //operator As String * 4
        //reltime As String * 19
        //weight As String * 10
        //CarNo As String * 8
        //SealNo1 As String * 10
        //SealNo2 As String * 10
        //Level As String * 1                     '新增布控级别， 0、1、2，其含义依次为：正常，警告，报警
    }

    public static class DataPack
    {
        public static P1Type m_P1 = new P1Type();
        public static P2Type m_P2 = new P2Type();
        public static bool P2Received;   //p2报文接收标志
        public static string InString = "";

        public static void InitP10()
        {
            m_P1.RecogFlag = new string(' ',1);
            m_P1.Channel = new string(' ',2);
            m_P1.Type = new string(' ',1);
            m_P1.No1 = new string(' ',12);
            m_P1.No2  = new string(' ',12);
            m_P1.RelState  = new string(' ',1);
            m_P1.Operator  = new string(' ',4);
        }

        public static void InitP1()
        {
            m_P1.RecogFlag = new string(' ', 1);
            m_P1.Channel = new string(' ', 2);
            m_P1.Type = new string(' ', 1);
            m_P1.No1 = new string(' ', 12);
            m_P1.No2 = new string(' ', 12);
            m_P1.RelState = new string(' ', 1);
            m_P1.Operator = new string(' ', 4);

            //m_P1.ImageFiles = "";
        }

        public static void InitP2()
        {
            m_P2.Channel = new string(' ',2);
            m_P2.Type = new string(' ',1);
            m_P2.No1 = new string(' ',11);
            m_P2.No2  = new string(' ',11);
            m_P2.RelState  = new string(' ',1);
            m_P2.Operator  = new string(' ',4);        
            m_P2.RelTime = new string(' ',19);
            m_P2.Weight = new string(' ',10);
            m_P2.CarNo = new string(' ',8);
            m_P2.SealNo1 = new string(' ',10);
            m_P2.SealNo2 = new string(' ',10);
            m_P2.Level = new string(' ',1);
        }
    
        public static byte[] StructToBytes(object structObj)
        {
            int size = Marshal.SizeOf(structObj);
            IntPtr buffer = Marshal.AllocHGlobal(size);
            try
            {
                Marshal.StructureToPtr(structObj, buffer, true);
                byte[] bytes = new byte[size];
                Marshal.Copy(buffer, bytes, 0, size);
                return bytes;
            }
            finally
            {
                Marshal.FreeHGlobal(buffer);
            }
        }

        public static object BytesToStruct(byte[] bytes, Type strcutType)
        {
            int size = Marshal.SizeOf(strcutType);
            IntPtr buffer = Marshal.AllocHGlobal(size);
            try
            {
                Marshal.Copy(bytes, 0, buffer, size);
                return Marshal.PtrToStructure(buffer, strcutType);
            }
            finally
            {
                Marshal.FreeHGlobal(buffer);
            }
        }
        //读取文本内XML格式数据
        private static string GetXML(string path)
        {
            StreamReader textReader = new StreamReader(path, Encoding.Default);
            string textLine = "";
            string textData = "";
            while ((textLine = textReader.ReadLine()) != null)
            {
                textData += textLine + "\n";
            }
            textReader.Close();
            return textData;
        }
        public static P1Type StrbToP1(string string1)
        {
            m_P1.RecogFlag = string1.Substring(3, 1);   //StrConv(MidB$(string1, 4, 1), vbUnicode)
            m_P1.Channel = string1.Substring(4, 2);     //StrConv(MidB$(string1, 5, 2), vbUnicode)
            m_P1.Type = string1.Substring(6, 1);        //StrConv(MidB$(string1, 7, 1), vbUnicode)
            m_P1.No1 = string1.Substring(7,12);         //StrConv(MidB$(string1, 8, 12), vbUnicode)
            m_P1.No2 = string1.Substring(19,12);        //StrConv(MidB$(string1, 20, 12), vbUnicode)
            m_P1.RelState = string1.Substring(31,1);    //StrConv(MidB$(string1, 32, 1), vbUnicode)
            m_P1.Operator = string1.Substring(32,4);    //StrConv(MidB$(string1, 33, 4), vbUnicode)

            return m_P1;
        }
        //生成XML格式数据
        public static XmlDocument MakeXML(XmlDocument doc, P1Type P1)
        {
            //string localIP;//获取本机ip地址
            //using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            //{
            //    socket.Connect("8.8.8.8", 65530);
            //    IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
            //    localIP = endPoint.Address.ToString();
            //}
            XmlElement el = doc.CreateElement("GATHER_INFO");//创建根节点
            el.SetAttribute("I_E_TYPE", "I");
            el.SetAttribute("ARER_ID", "4734010001");
            el.SetAttribute("CHNL_NO", "4734010001");
            el.SetAttribute("SEQ_NO", DateTime.Now.ToString("yyyyMMddHHmmss"));
            //el.SetAttribute("SEQ_NO", "20201107151223");

            XmlElement el1 = doc.CreateElement("CONTA");

            XmlElement el11 = doc.CreateElement("CONTA_NUM");
            XmlElement el12 = doc.CreateElement("CONTA_ID_F");
            XmlElement el13 = doc.CreateElement("CONTA_ID_B");
            XmlElement el14 = doc.CreateElement("CONTA_MODEL_F");
            XmlElement el15 = doc.CreateElement("CONTA_MODEL_B");
            XmlElement el16 = doc.CreateElement("CONTA_F_PIC");
            XmlElement el17 = doc.CreateElement("CONTA_B_PIC");
            XmlElement el18 = doc.CreateElement("CONTA_FL_PIC");
            XmlElement el19 = doc.CreateElement("CONTA_FR_PIC");
            XmlElement el1A = doc.CreateElement("CONTA_BL_PIC");
            XmlElement el1B = doc.CreateElement("CONTA_BR_PIC");

            string LocalIP_Port = InitialValue.Network.PrefixInformation; //前缀

            if (P1.Type == "0" || P1.Type == "2")//长箱或双箱
            {
                //XmlElement el11 = doc.CreateElement("CONTA_NUM");
                if (P1.Type == "0")
                    if (InitialValue.Network.SendProtocalType == 2)
                        el11.InnerText = "1";
                    else
                        el11.InnerText = "0";
                else
                    el11.InnerText = "2";
                el1.AppendChild(el11);
                el.AppendChild(el1);
                //XmlElement el12 = doc.CreateElement("CONTA_ID_F");
                el12.InnerText = P1.No1; //"CMAU7325928"
                el1.AppendChild(el12);
                el.AppendChild(el1);
                //XmlElement el13 = doc.CreateElement("CONTA_ID_B");
                el13.InnerText = P1.No2;
                el1.AppendChild(el13);
                el.AppendChild(el1);
                //XmlElement el14 = doc.CreateElement("CONTA_MODEL_F");
                el14.InnerText = P1.TypeNo1;
                el1.AppendChild(el14);
                el.AppendChild(el1);
                //XmlElement el15 = doc.CreateElement("CONTA_MODEL_B");
                el15.InnerText = P1.TypeNo2;
                el1.AppendChild(el15);
                el.AppendChild(el1);
                //XmlElement el16 = doc.CreateElement("CONTA_F_PIC");
                //el16.InnerText = "http://10.79.104.68:10030//2020-07-30/093458F.jpg";
                //el16.InnerText = "http://10.79.104.68:10030" + P1.F_ImagePath;
                el16.InnerText = LocalIP_Port + P1.F_ImagePath;
                el1.AppendChild(el16);
                el.AppendChild(el1);
                //XmlElement el17 = doc.CreateElement("CONTA_B_PIC");
                el17.InnerText = LocalIP_Port + P1.B_ImagePath;
                el1.AppendChild(el17);
                el.AppendChild(el1);
                //XmlElement el18 = doc.CreateElement("CONTA_FL_PIC");
                el18.InnerText = LocalIP_Port + P1.FL_ImagePath;
                el1.AppendChild(el18);
                el.AppendChild(el1);
                //XmlElement el19 = doc.CreateElement("CONTA_FR_PIC");
                el19.InnerText = LocalIP_Port + P1.FR_ImagePath;
                el1.AppendChild(el19);
                el.AppendChild(el1);
                //XmlElement el1A = doc.CreateElement("CONTA_BL_PIC");
                el1A.InnerText = LocalIP_Port + P1.BL_ImagePath;
                el1.AppendChild(el1A);
                el.AppendChild(el1);
                //XmlElement el1B = doc.CreateElement("CONTA_BR_PIC");
                el1B.InnerText = LocalIP_Port + P1.BR_ImagePath;
                el1.AppendChild(el1B);
                el.AppendChild(el1);
            }
            else if (P1.Type == "1")//单箱
            {
                //XmlElement el11 = doc.CreateElement("CONTA_NUM");
                el11.InnerText = P1.Type;
                el1.AppendChild(el11);
                el.AppendChild(el1);
                //XmlElement el12 = doc.CreateElement("CONTA_ID_F");
                el12.InnerText = P1.No1; //"CMAU7325928"
                el1.AppendChild(el12);
                el.AppendChild(el1);
                //XmlElement el13 = doc.CreateElement("CONTA_ID_B");
                el13.InnerText = P1.No2;
                el1.AppendChild(el13);
                el.AppendChild(el1);
                //XmlElement el14 = doc.CreateElement("CONTA_MODEL_F");
                el14.InnerText = P1.TypeNo1;
                el1.AppendChild(el14);
                el.AppendChild(el1);
                //XmlElement el15 = doc.CreateElement("CONTA_MODEL_B");
                el15.InnerText = P1.TypeNo2;
                el1.AppendChild(el15);
                el.AppendChild(el1);
                //XmlElement el16 = doc.CreateElement("CONTA_F_PIC");
                //el16.InnerText = "http://10.79.104.68:10030//2020-07-30/093458F.jpg";
                el16.InnerText = LocalIP_Port + P1.F_ImagePath;
                el1.AppendChild(el16);
                el.AppendChild(el1);
                //XmlElement el17 = doc.CreateElement("CONTA_B_PIC");
                el17.InnerText = LocalIP_Port + P1.B_ImagePath;
                el1.AppendChild(el17);
                el.AppendChild(el1);
                //XmlElement el18 = doc.CreateElement("CONTA_FL_PIC");
                el18.InnerText = LocalIP_Port + P1.FR_ImagePath;
                el1.AppendChild(el18);
                el.AppendChild(el1);
                //XmlElement el19 = doc.CreateElement("CONTA_FR_PIC");
                el19.InnerText = LocalIP_Port + P1.BL_ImagePath;
                el1.AppendChild(el19);
                el.AppendChild(el1);
                ////XmlElement el1A = doc.CreateElement("CONTA_BL_PIC");
                //el1A.InnerText = "http://10.79.104.68:10030" + P1.BL_ImagePath;
                //el1.AppendChild(el1A);
                //el.AppendChild(el1);
                ////XmlElement el1B = doc.CreateElement("CONTA_BR_PIC");
                //el1B.InnerText = "http://10.79.104.68:10030" + P1.BR_ImagePath;
                //el1.AppendChild(el1B);
                //el.AppendChild(el1);
            }

            doc.AppendChild(el);//将跟节点添加到xml文档

            //doc.Save(".//XML文件.xml");
            return doc;
        }
        //public static string P1ToStrb(P1Type P1)
        //{
        //    //string ts1= "<GATHER_INFO I_E_TYPE=\"I\" ARER_ID=\"4734010001\" CHNL_NO=\"4734010001\" SEQ_NO=\"20201107151223\">< CONTA >< CONTA_NUM > 1 </ CONTA_NUM >< CONTA_ID_F > CMAU7325928 </ CONTA_ID_F >< CONTA_ID_B ></ CONTA_ID_B >< CONTA_MODEL_F > 45G1 </ CONTA_MODEL_F >< CONTA_MODEL_B ></ CONTA_MODEL_B >< CONTA_F_PIC > http://10.79.104.68:10030//2020-07-30/093458F.jpg</CONTA_F_PIC>< CONTA_B_PIC > http://10.79.104.68:10030//2020-07-30/093458B.jpg</CONTA_B_PIC>< CONTA_FL_PIC > http://10.79.104.68:10030//2020-07-30/093458FL.jpg</CONTA_FL_PIC>< CONTA_FR_PIC > http://10.79.104.68:10030//2020-07-30/093458FR.jpg</CONTA_FR_PIC>< CONTA_BL_PIC > http://10.79.104.68:10030//2020-07-30/093458BL.jpg</CONTA_BL_PIC>< CONTA_BR_PIC > http://10.79.104.68:10030//2020-07-30/093458BR.jpg</CONTA_BR_PIC></ CONTA ></ GATHER_INFO >";
        //    //string ts2 = "<GATHER_INFO I_E_TYPE=\"I\" ARER_ID=\"4734010001\" CHNL_NO=\"4734010001\" SEQ_NO=\"20201107151223\">< CONTA >< CONTA_NUM > 2 </ CONTA_NUM >< CONTA_ID_F > CXDU2366284 </ CONTA_ID_F >< CONTA_ID_B > ZGXU2245233 </ CONTA_ID_B >< CONTA_MODEL_F > 22G1 </ CONTA_MODEL_F >< CONTA_MODEL_B > 22G1 </ CONTA_MODEL_B >< CONTA_F_PIC > http://10.79.104.68:10030//2020-07-30/103546F.jpg</CONTA_F_PIC>< CONTA_B_PIC > http://10.79.104.68:10030//2020-07-30/103546B.jpg</CONTA_B_PIC>< CONTA_FL_PIC > http://10.79.104.68:10030//2020-07-30/103546FL.jpg</CONTA_FL_PIC>< CONTA_FR_PIC > http://10.79.104.68:10030//2020-07-30/103546FR.jpg</CONTA_FR_PIC>< CONTA_BL_PIC > http://10.79.104.68:10030//2020-07-30/103546BL.jpg</CONTA_BL_PIC>< CONTA_BR_PIC > http://10.79.104.68:10030//2020-07-30/103546BR.jpg</CONTA_BR_PIC></ CONTA ></ GATHER_INFO >";

        //    if(P1.Type=="2")                    //双箱
        //    {
        //        string ts = GetXML(".//双箱.txt");
        //        MidString(ref ts, P1.Type, 112);
        //        MidString(ref ts, P1.No1, 138);
        //        MidString(ref ts, P1.No2, 175);
        //        MidString(ref ts, P1.TypeNo1, 215);
        //        MidString(ref ts, P1.TypeNo2, 251);
        //        return ts;
        //    }
        //    else if(P1.Type == "1")             //单箱
        //    {
        //        string ts = GetXML(".//单箱.txt");
        //        MidString(ref ts, P1.Type, 112);
        //        MidString(ref ts, P1.No1, 138); //单箱 captureImage(0)
        //        MidString(ref ts, P1.TypeNo1, 204);
        //        return ts;
        //    }
        //    else                                //长箱
        //    {
        //        string ts = GetXML(".//单箱.txt");
        //        MidString(ref ts, P1.Type, 112);
        //        MidString(ref ts, P1.No2, 138); //长箱 captureImage(1)
        //        MidString(ref ts, P1.TypeNo2, 204);
        //        return ts;
        //    }
        //}
        public static string P1ToStrb(P1Type P1, int SendProtocalType)
        {
            string ts; int ll;

            if (P1.ImageFiles == null)
            {
                //Console.WriteLine("P1.ImageFiles == null");
                //ts = new string(' ', 36);
                //ll = 33;
                ts = SendProtocalType == 1 ? new string(' ', 46): new string(' ', 36);  //如果加上具体箱型号，如",22G1"，两个共10字节
                ll = SendProtocalType == 1 ? 43 : 33;
            }
            else
            {
                //ts = new string(' ', 36 + m_P1.ImageFiles.Length);
                //ll = 33 + m_P1.ImageFiles.Length;
                ts = SendProtocalType == 1 ? new string(' ', 46 + m_P1.ImageFiles.Length) : new string(' ', 36 + m_P1.ImageFiles.Length);  //如果加上具体箱型号，如",22G1"，两个共10字节
                ll = SendProtocalType == 1 ? 43 + m_P1.ImageFiles.Length : 33 + m_P1.ImageFiles.Length;
            }
           
            MidString(ref ts, Convert.ToString((char)1), 0);                     //MidB$(ts, 1, 1) = ChrB(1)
            //Console.WriteLine("ts0 : " + ts);
            MidString(ref ts, Convert.ToString((char)(ll / 256)), 1);          //MidB$(ts, 2, 2) = ChrB$(0) + ChrB$(33)
            //Console.WriteLine("ts1 : " + ts);
            MidString(ref ts, Convert.ToString((char)(ll % 256)), 2);          //MidB$(ts, 2, 2) = ChrB$(0) + ChrB$(33)
            //Console.WriteLine("ts2 : " + ts);
            //Console.WriteLine("P1.RecogFlag : " + ts);
            //Console.WriteLine("P1.Channel : " + ts);
            //Console.WriteLine("P1.Type : " + ts);
            //Console.WriteLine("P1.No1 : " + ts);
            //Console.WriteLine("P1.No2 : " + ts);
            //Console.WriteLine("P1.RelState : " + ts);
            //Console.WriteLine("P1.Operator : " + ts);
            MidString(ref ts, P1.RecogFlag, 3);                     //MidB$(ts, 4, 1) = StrConv(P1.RecogFlag, vbFromUnicode)
            //Console.WriteLine("P1.RecogFlag : " + ts);
            MidString(ref ts, P1.Channel, 4);                       //MidB$(ts, 5, 2) = StrConv(P1.channel, vbFromUnicode)
            //Console.WriteLine("P1.Channel : " + ts);
            MidString(ref ts, P1.Type, 6);                          //MidB$(ts, 7, 1) = StrConv(P1.type, vbFromUnicode)
            //Console.WriteLine("P1.Type : " + ts);
            MidString(ref ts, P1.No1, 7);

            string comma = ",";
            if (SendProtocalType == 1)
            {
                MidString(ref ts, comma, 19);
                MidString(ref ts, P1.TypeNo1, 20);
                MidString(ref ts, P1.No2, 24);
                MidString(ref ts, comma, 36);
                MidString(ref ts, P1.TypeNo1, 37);
                MidString(ref ts, P1.RelState, 41);
                MidString(ref ts, P1.Operator, 42);
                if (P1.ImageFiles != null)
                {
                    MidString(ref ts, P1.ImageFiles, 46);                   //新增图像文件名，20140701
                }
            }
            else
            {
                //MidB$(ts, 8, 12) = StrConv(P1.No1, vbFromUnicode)
                //Console.WriteLine("P1.No1 : " + ts);
                MidString(ref ts, P1.No2, 19);
                //MidB$(ts, 20, 12) = StrConv(P1.No2, vbFromUnicode)
                //Console.WriteLine("P1.No2 : " + ts);
                MidString(ref ts, P1.RelState, 31);
                //MidB$(ts, 32, 1) = StrConv(P1.relstate, vbFromUnicode)
                //Console.WriteLine("P1.RelState : " + ts);
                MidString(ref ts, P1.Operator, 32);                     //MidB$(ts, 33, 4) = StrConv(P1.operator, vbFromUnicode)
                //Console.WriteLine("P1.Operator : " + ts);
                if (P1.ImageFiles != null)
                {
                    MidString(ref ts, P1.ImageFiles, 36);                   //新增图像文件名，20140701
                }
            }

            return ts;
        }

        //置换字符串中间未知的子串
        public static void MidString(ref string str, string str1, int ii)
        {
            if (ii >= 0)
            {
                if (ii + str1.Length <= str.Length)
                    str = str.Substring(0, ii) + str1 + str.Substring(ii + str1.Length);
                else
                    str = str.Substring(0, ii) + str1.Substring(0, str.Length - ii); 
            }
        }

        public static P2Type StrbToP2(string string1)
        {
            if (m_P2.Channel == null) InitP2();

            m_P2.Channel = string1.Substring(3, 2);     //StrConv(MidB$(string1, 4, 2), vbUnicode)
            m_P2.Type = string1.Substring(5, 1);        // StrConv(MidB$(string1, 6, 1), vbUnicode)
            m_P2.No1 = string1.Substring(6, 11);        //    StrbToP2.No1 = StrConv(MidB$(string1, 7, 11), vbUnicode)
            m_P2.No2 = string1.Substring(17, 11);       //    StrbToP2.No2 = StrConv(MidB$(string1, 18, 11), vbUnicode)
            m_P2.RelState = string1.Substring(28, 1);        //    StrbToP2.relstate = StrConv(MidB$(string1, 29, 1), vbUnicode)
            m_P2.Operator = string1.Substring(29, 4);        //    StrbToP2.operator = StrConv(MidB$(string1, 30, 4), vbUnicode)
            m_P2.RelTime = string1.Substring(33, 19);            //    StrbToP2.reltime = StrConv(MidB$(string1, 34, 19), vbUnicode)
            m_P2.Weight = string1.Substring(52, 10);             //    StrbToP2.weight = StrConv(MidB$(string1, 53, 10), vbUnicode)
            m_P2.CarNo = string1.Substring(62, 8);               //    StrbToP2.CarNo = StrConv(MidB$(string1, 63, 8), vbUnicode)
            m_P2.SealNo1 = string1.Substring(70, 10);            //    StrbToP2.SealNo1 = StrConv(MidB$(string1, 71, 10), vbUnicode)
            m_P2.SealNo2 = string1.Substring(80, 10);            //    StrbToP2.SealNo2 = StrConv(MidB$(string1, 81, 10), vbUnicode)
            if (string1.Length >= 91)
                m_P2.Level = string1.Substring(90, 1);               //    StrbToP2.Level = StrConv(MidB$(string1, 91, 1), vbUnicode)
            else
                m_P2.Level = "";
            return m_P2;
        }


        public static string P2ToStrb(P2Type P2)
        {
            if (m_P2.Channel == null) InitP2();

            string ts = new string(' ', 91);

            MidString(ref ts, Convert.ToString((char)2), 0);            //    MidB$(ts, 1, 1) = ChrB(2)
            MidString(ref ts, Convert.ToString((char)0), 1);        //    MidB$(ts, 2, 2) = ChrB$(0) + ChrB$(87)
            MidString(ref ts, Convert.ToString((char)87), 2);       //    MidB$(ts, 2, 2) = ChrB$(0) + ChrB$(87)
            MidString(ref ts, P2.Channel, 3);                           //    MidB$(ts, 4, 2) = StrConv(p2.channel, vbFromUnicode)
            MidString(ref ts, P2.Type, 5);                              //    MidB$(ts, 6, 1) = StrConv(p2.type, vbFromUnicode)
            MidString(ref ts, P2.No1, 6);                       //    MidB$(ts, 7, 11) = StrConv(p2.No1, vbFromUnicode)
            MidString(ref ts, P2.No2, 17);                      //    MidB$(ts, 18, 11) = StrConv(p2.No2, vbFromUnicode)
            MidString(ref ts, P2.RelState, 28);                 //    MidB$(ts, 29, 1) = StrConv(p2.relstate, vbFromUnicode)
            MidString(ref ts, P2.Operator, 29);                 //    MidB$(ts, 30, 4) = StrConv(p2.operator, vbFromUnicode)
            MidString(ref ts, P2.RelTime, 33);                  //    MidB$(ts, 34, 19) = StrConv(p2.reltime, vbFromUnicode)
            MidString(ref ts, P2.Weight, 52);                   //    MidB$(ts, 53, 10) = StrConv(p2.weight, vbFromUnicode)
            MidString(ref ts, P2.CarNo, 62);                    //    MidB$(ts, 63, 8) = StrConv(p2.CarNo, vbFromUnicode)
            MidString(ref ts, P2.SealNo1, 70);                  //    MidB$(ts, 71, 10) = StrConv(p2.SealNo1, vbFromUnicode)
            MidString(ref ts, P2.SealNo2, 80);                  //    MidB$(ts, 81, 10) = StrConv(p2.SealNo2, vbFromUnicode)
            MidString(ref ts, P2.Level, 90);                    //    MidB$(ts, 91, 10) = StrConv(p2.Level, vbFromUnicode)
 
            return ts;
        }
    }
}
