﻿using OpenCvSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using HKTest.ONNX;
using DetectionResult = HKTest.ONNX.DetectionResult;
//using PaddleOCRSharp;
using Sdcb.PaddleInference;
using Sdcb.PaddleOCR;
using System.Drawing;
using Point = OpenCvSharp.Point;
using Size = OpenCvSharp.Size;
using Sdcb.PaddleOCR.Models;

namespace HKTest.Utils
{
    //集装箱信息结构体
    public struct ContainerData
    {
        public string ContainerNO;
        public string ContainerType;
        public string CameraType;   //前相机，前相机辅助，后相机，后相机辅助，中缝前,中缝辅助前，中缝后,中缝辅助后
        public string ImagePath;
        public string GrabTime;
        public int CheckResult;
        public float Confidence;  //识别置信度
        public PictureBox grab_pb;  //显示的图片控件
    }

    //到位状态枚举
    //wait状态，此状态下前侧到位检测开启，箱顶到位检测和后箱到位检测休眠
    //FrontArrived状态，此状态下后侧到位检测和箱顶到位检测休眠开启，前侧到位检测
    //TopArrived状态，此状态下后侧到位检测开启，前侧到位和箱顶到位检测休眠
    //BackArrived状态，此状态下结束流程，回到wait状态
    public enum ArrivedState
    {
        Wait,       //等待
        FrontArrived,       //前侧到位
        TopArrived,         //箱顶到位
        SideArrived,        //侧面到位
        BackArrived,         //后侧到位
        Recognizing          //识别中
    };

    //识别流程结构体
     public class ProcessInfo
    {
        //标识当前流程唯一性的验证码，六位随机数字
        public int verifyCode;
        //显示窗口图片清空标记
        public bool picClearFlag;
        //到位状态
        public ArrivedState state;
        //辅助状态，用于文本指示通过何种方式到位
        public string state1;
        //前侧是否检测到
        public bool stateF;
        //中缝是否检测到
        public bool stateM;
        //后侧是否检测到
        public bool stateB;
        //非集卡车是否检测到
        public bool stateVehicles;
        //空集卡车是否检测到
        public bool stateEmptyCar;
        //车头是否检测到
        public bool stateCarHead;
        //车头检测时间
        public long timeCarHead;
        //空车检测时间
        public long timeEmptyCar;
        //当前到位的箱号信息结构
        public List<ContainerData> dataList;       
        //临时存储文件夹路径
        public string tempPath;
        //当前流程对应的label控件
        public Label lbState;
        //上一次箱号
        public string lastNo1="";
        public string lastNo2="";

        public Mat CarHeadImage;
        public Mat EmptyCarImage;
    }

    class ContainerHelper
    {
        //public static PaddleDetector m_detector;
        //public static PaddleOcrDetector m_OcrDetector;
        //public static MyONNX detector = new MyONNX();
        //public static MyONNX OcrDetector = new MyONNX();
        public static string LastCarDate = "";  //最新通过车辆日期
        public static PaddleOcrRecognizer m_OcrRecognizer;
        public static bool OcrIsRuning = false;
        //初始化三个模型

        public static string InitialModelsONNX(ref MyONNX detector, ref MyONNX OcrDetector, ref PaddleOcrRecognizer OcrRecognizer)
        {
            string rtnString;

            //初始化ONNX检测器
            try
            {
                detector.ONNX_Load(InitialValue.modelDir.BaseDir + @"\DetContainer.onnx", InitialValue.modelDir.BaseDir + @"\labelContainer.txt");
                OcrDetector.ONNX_Load(InitialValue.modelDir.BaseDir + @"\DetOCR.onnx", InitialValue.modelDir.BaseDir + @"\labelOCR.txt");
                rtnString = "初始化完成，系统配置了GPU！等待车辆进入...";
            }
            catch (Exception e1)
            {
                detector = new MyONNX();
                detector.ONNX_Load_CPU(InitialValue.modelDir.BaseDir + @"\DetContainer.onnx", InitialValue.modelDir.BaseDir + @"\labelContainer.txt");
                OcrDetector.ONNX_Load_CPU(InitialValue.modelDir.BaseDir + @"\DetOCR.onnx", InitialValue.modelDir.BaseDir + @"\labelOCR.txt");
                rtnString = "初始化完成，系统没有配置GPU！等待车辆进入...";
            }


            if (m_OcrRecognizer != null)
            {
                OcrRecognizer = m_OcrRecognizer.Clone();
            }
            else
            {
                RecognizationModel theModel;
                //if (rtnString == "初始化完成，系统配置了GPU！等待车辆进入...")
                try
                {
                    theModel = RecognizationModel.FromDirectory(InitialValue.modelDir.RecognitionOCR, InitialValue.modelDir.RecognitionOCR + @"\en_dict.txt", ModelVersion.V3);
                    OcrRecognizer = new PaddleOcrRecognizer(theModel, PaddleDevice.Gpu());
                }
                catch
                {
                    theModel = RecognizationModel.FromDirectory(InitialValue.modelDir.RecognitionOCR, InitialValue.modelDir.RecognitionOCR + @"\en_dict.txt", ModelVersion.V3);
                    OcrRecognizer = new PaddleOcrRecognizer(theModel, PaddleDevice.Mkldnn());
                }
                m_OcrRecognizer = OcrRecognizer;
            }

            return rtnString;
        }


        /// <summary>
        /// 生成与旧随机数不同的六位随机数
        /// </summary>
        /// <param name="oldCode"></param>
        /// <returns></returns>
        public static int codeGet(int oldCode)
        {
            Random rd = new Random();
            int newCode = rd.Next(100000,1000000);
            while(newCode == oldCode)
            {
                newCode = rd.Next(100000, 1000000);
            }
            return newCode;
        }

        //计算左右切边边界
        public static void marginComputing(Mat frame, string fileName)
        {
            Rect rect = new Rect(0, 0, frame.Width, frame.Height);   //存储的区域

            int row0 = frame.Height / 8;   //在该行找左右边界

            if (fileName.Contains("F"))
            {
                Mat<Vec3b> mat3 = new Mat<Vec3b>(frame);
                var indexer = mat3.GetIndexer();

                for (int c = 0; c < frame.Cols / 2; c++)
                {
                    Vec3b color = indexer[row0, c];
                    if (color.Item0 + color.Item1 + color.Item2 > 0)
                    {
                        rect.Left = Math.Max(0, c);   //左边界
                        break;
                    }
                }
                for (int c = frame.Cols / 2; c < frame.Cols; c++)
                {
                    Vec3b color = indexer[row0, c];
                    if (color.Item0 + color.Item1 + color.Item2 == 0)
                    {
                        rect.Width = c - rect.Left;  //宽度
                        break;
                    }
                    else if (c == frame.Cols - 1)
                    {
                        rect.Width = c - rect.Left + 1;  //宽度
                        break;
                    }
                }
                mat3.Dispose();

                if (fileName.Contains("FF"))
                {
                    if (InitialValue.Video[1].CalibParas2 == "")  //不切边，则用默认值
                    {
                        InitialValue.Video[1].leftMargin2 = 0;
                        InitialValue.Video[1].widthMargin2 = frame.Width;
                    }
                    else
                    {
                        InitialValue.Video[1].leftMargin2 = rect.Left;
                        InitialValue.Video[1].widthMargin2 = rect.Width;
                    }
                }
                else
                {
                    if (InitialValue.Video[1].CalibParas1 == "")  //不切边，则用默认值
                    {
                        InitialValue.Video[1].leftMargin1 = 0;
                        InitialValue.Video[1].widthMargin1 = frame.Width;
                    }
                    else
                    {
                        InitialValue.Video[1].leftMargin1 = rect.Left;
                        InitialValue.Video[1].widthMargin1 = rect.Width;
                    }
                }
            }

            if (fileName.Contains("B"))
            {
                Mat<Vec3b> mat3 = new Mat<Vec3b>(frame);
                var indexer = mat3.GetIndexer();

                for (int c = 0; c < frame.Cols / 2; c++)
                {
                    Vec3b color = indexer[row0, c];
                    if (color.Item0 + color.Item1 + color.Item2 > 0)
                    {
                        rect.Left = Math.Max(0, c);   //左边界
                        break;
                    }
                }
                for (int c = frame.Cols / 2; c < frame.Cols; c++)
                {
                    Vec3b color = indexer[row0, c];
                    if (color.Item0 + color.Item1 + color.Item2 == 0)
                    {
                        rect.Width = c - rect.Left;  //宽度
                        break;
                    }
                    else if (c == frame.Cols - 1)
                    {
                        rect.Width = c - rect.Left + 1;  //宽度
                        break;
                    }
                }
                mat3.Dispose();

                if (InitialValue.Video[0].CalibParas1 == "")
                {
                    rect.Left = 0;
                    rect.Width = frame.Width;
                 }

                if (fileName.Contains("BB"))
                {
                    if (InitialValue.Video[0].CalibParas2 == "")  //不切边，则用默认值
                    {
                        InitialValue.Video[0].leftMargin2 = 0;
                        InitialValue.Video[0].widthMargin2 = frame.Width;
                    }
                    else
                    {
                        InitialValue.Video[0].leftMargin2 = rect.Left;
                        InitialValue.Video[0].widthMargin2 = rect.Width;
                    }
                }
                else
                {
                    if (InitialValue.Video[0].CalibParas1 == "")  //不切边，则用默认值
                    {
                        InitialValue.Video[0].leftMargin1 = 0;
                        InitialValue.Video[0].widthMargin1 = frame.Width;
                    }
                    else
                    {
                        InitialValue.Video[0].leftMargin1 = rect.Left;
                        InitialValue.Video[0].widthMargin1 = rect.Width;
                    }
                }
            }
        }
        /// <summary>
        /// 保存图像到指定路径文件夹
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="savePath"></param>
        /// <param name="fileName"></param>
        ///  <param name="imageType"></param>  1:前侧，2：后侧，3：中间
        /// <returns></returns>
        public static Boolean ImgSave(Mat frame, string rootPath, string subPath, string fileName)
        {
            if (frame == null)
                    return false;

            if (InitialValue.Settings.SaveFlag == 0)
            {
                return false;
            }

            //若该路径下文件夹不存在，则创建文件夹
            string filePath = Path.Combine(rootPath, subPath);

            try
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                //string filePath = Path.Combine(savePath, fileName);
                filePath = Path.Combine(filePath, fileName);

                //保存图像到该文件夹
                //Cv2.ImWrite(filePath, frame.Clone(rect));
                Cv2.ImWrite(filePath, frame);
            }
            catch (Exception ee)
            {
                throw new Exception("图像存盘出错：" + ee.ToString());
            }

            return true;
        }

        /// <summary>
        /// 清理该文件夹下所有文件
        /// </summary>
        /// <param name="imgPath">图像文件夹路径</param>
        public static void ImgClear(string imgPath)
        {
            DirectoryInfo path = new DirectoryInfo(imgPath);
            foreach (FileInfo f in path.GetFiles())
            {
                f.Delete();
            }
        }

        //根据GrabTime和SendProtocalType确定存盘路径
        public static string getFilePath(ContainerData myData, ProcessInfo myController)
        {
            string tmpPath="";

            if (InitialValue.Settings.SaveFlag == 2)  //H986协议 
            {
                if (myController.dataList.Count > 0)     
                {
                    tmpPath = myController.dataList.FirstOrDefault().GrabTime;    //取最早的采集图像时间，因为整个箱子需要用一个时间
                }
                else
                {
                    tmpPath = myData.GrabTime;
                }
                tmpPath = tmpPath.Substring(0, 19).Replace("-", @"\").Replace(":", "").Replace(" ", @"\");
            }
            else
            {
                tmpPath = myData.GrabTime;
                if ((InitialValue.Network.SendProtocalType == 2) || (InitialValue.Network.SendProtocalType == 3))    //2,3: 软筑协议  
                {
                    tmpPath = tmpPath.Substring(0, 10).Replace("-", @"\");
                }
                else //英迪协议
                {
                    tmpPath = tmpPath.Substring(0, 10).Replace("-", "").Substring(2);
                }
            }

            return tmpPath;
        }

        ////imageType:"前相机"，"中缝", "后相机"
        public static string getFileName(ContainerData myData, string imageType, ProcessInfo myController)
        {
            string path = "";
            string imageName = "";

            if (InitialValue.Settings.SaveFlag == 2)   //H986协议   
            {
                switch (imageType)
                {
                    case "前相机":
                        imageName = "001.jpg";
                        break;
                    case "前相机辅助":
                        imageName = "002.jpg";
                        break;
                    case "中缝":
                        imageName = "005.jpg";
                        break;
                    case "中缝辅助":
                        imageName = "006.jpg";
                        break;
                    case "后相机":
                        imageName = "003.jpg";
                        break;
                    case "后相机辅助":
                        imageName = "004.jpg";
                        break;
                }
                if (myController.dataList.Count > 0)
                {
                    path = myController.dataList.FirstOrDefault().GrabTime;    //取最早的采集图像时间，引为整个箱子需要用一个时间
                }
                else
                {
                    path = myData.GrabTime;
                }
                path = path.Replace(":", "").Replace(" ", "-");
                imageName = path + @"-"+imageName;
            }
            else         //network.sendprotocoltype:2,3: 软筑协议，0，1英迪协议
            { 
                imageName = DateTime.Now.ToString("HHmmss");
                switch (imageType)
                {
                    case "前相机":
                        imageName = imageName + "F.jpg";
                        break;
                    case "前相机辅助":
                        imageName = imageName + "FF.jpg";
                        break;
                    case "中缝":
                        imageName = imageName + "M.jpg";
                        break;
                    case "中缝辅助":
                        imageName = imageName + "MM.jpg";
                        break;
                    case "后相机":
                        imageName = imageName + "B.jpg";
                        break;
                    case "后相机辅助":
                        imageName = imageName + "BB.jpg";
                        break;
                }
            }

            return imageName;
        }


        /// <summary>
        /// 将对应文件夹下文件移动到目标文件夹
        /// </summary>
        /// <param name="srcPath"></param>
        public static void ImgMove(string srcPath)
        {
            //将个字符串合并到一个路径中
            string savePath = Path.Combine(InitialValue.Settings.ImagePath, DateTime.Now.ToString("yyMMdd"));

            //复制图像
            if (InitialValue.Settings.SaveFlag == 1)
                CopyDir(srcPath, savePath);

            //清理图像
            ImgClear(srcPath);
        }

        /// <summary>
        /// 将源文件夹下的所有文件复制到目标文件夹下
        /// </summary>
        /// <param name="srcPath"></param>
        /// <param name="aimPath"></param>
        public static void CopyDir(string srcPath, string aimPath)
        {
            try
            {
                // 检查目标目录是否以目录分割字符结束如果不是则添加之
                if (aimPath[aimPath.Length - 1] != Path.DirectorySeparatorChar)
                    aimPath += Path.DirectorySeparatorChar;
                // 判断目标目录是否存在如果不存在则新建之
                if (!Directory.Exists(aimPath))
                    Directory.CreateDirectory(aimPath);
                // 得到源目录的文件列表，该里面是包含文件以及目录路径的一个数组
                //如果你指向copy目标文件下面的文件而不包含目录请使用下面的方法
                //string[] fileList = Directory.GetFiles(srcPath);
                string[] fileList = Directory.GetFileSystemEntries(srcPath);
                //遍历所有的文件和目录
                foreach (string file in fileList)
                {
                    //先当作目录处理如果存在这个目录就递归Copy该目录下面的文件

                    if (Directory.Exists(file))
                        CopyDir(file, aimPath + Path.GetFileName(file));
                    //否则直接Copy文件
                    else
                        File.Copy(file, aimPath + Path.GetFileName(file), true);
                }
            }
            catch (Exception ee)
            {
                throw new Exception(ee.ToString());
            }
        }


        /// <summary>
        /// 进行箱号识别(先检测再OCR)
        /// </summary>
        /// <param name="frame">捕获画面</param>
        /// <param name="myData">箱号数据结构体</param>
        /// <param name="kFactor">图像放大倍数</param>
        /// kFactor 放大倍数
        public static void NoRecognize(MyONNX OcrDetector, PaddleOcrRecognizer OcrRecognizer, Mat frame, ref ContainerData myData, float kFactor, int seamPosition)
        {
            myData.ContainerType = "????";
            myData.ContainerNO = "???????????";

           try
            {
                //将图像放大为原来的三倍，有助于识别
                //Cv2.Resize(frame, frame, new Size((int)(frame.Width * kFactor), (int)(frame.Height * kFactor)), 0, 0, InterpolationFlags.Linear);
                //区域检测，返回检测结果，文本检测的返回结果没有置信度
                resultPP[] rects = OcrDetector.Run(frame);
                //foreach (RotatedRect rect in rects)
                //{
                //    //将检测框绘制到图像上
                //    Point2f[] fourPoint2f = rect.Points();
                //    for (int i = 0; i < 4; i++)
                //    {
                //        Cv2.Line(frame, new OpenCvSharp.Point(fourPoint2f[i % 4].X, fourPoint2f[i % 4].Y), new OpenCvSharp.Point(fourPoint2f[(i + 1) % 4].X, fourPoint2f[(i + 1) % 4].Y), new Scalar(20, 21, 237), 3);
                //    }
                //}

                //存放文本识别结果
                List<PaddleOcrRecognizerResult> resultList = new List<PaddleOcrRecognizerResult>();

                //获取识别结果

                if (seamPosition <= 0) seamPosition = frame.Height;
                var rects0 = rects.Where(x => (x.Rect.Top < seamPosition) && ((x.Rect.X > frame.Width * 0.3) || (x.Rect.Width > x.Rect.Height)) ).ToArray();  //对于垂直箱号，x必须>frame.Width * 0.3

                Mat[] mats =
                rects0.Select(rect => 
                {
                    Mat roi = frame.Clone(GetCropedRect(rect.Rect, frame.Size()));   //[GetCropedRect(rect.BoundingRect(), frame.Size())];
                    bool wider = roi.Width > roi.Height;
                    if (wider)
                    {
                        return roi;
                    }
                    else
                    {
                        Cv2.Transpose(roi, roi);
                        Cv2.Flip(roi, roi, FlipMode.X);
                        return roi;
                    }
                })
                .ToArray();

                try
                {
                    for (int kk = 0; kk < mats.Length; kk++)
                    {
                        PaddleOcrRecognizerResult result = paddleOcrRun(OcrRecognizer, mats[kk]); //OcrRecognizer.Run(mats[kk]);

                        //if ((result.Text.Length > 5) && (result.Score<0.9) && (!DataCheckOK(result.Text)))
                        if (!DataCheckOK(result.Text))
                        {//顶上箱号反掉的反过来识别一下看看
                            Cv2.Flip(mats[kk], mats[kk], FlipMode.XY);
                            PaddleOcrRecognizerResult result1 = paddleOcrRun(OcrRecognizer, mats[kk]); // OcrRecognizer.Run(mats[kk]); 
                            if (result1.Score > result.Score)
                                resultList.Add(result1);
                            else
                                resultList.Add(result);
                        }
                        else
                        {
                            resultList.Add(result);
                        }
                    }
                }
                catch { }
                finally
                {
                    foreach (Mat mat in mats)
                    {
                        mat.Dispose();
                    }
                }

                float confidence = 0; 
                string tempString = NoContact(resultList.ToArray(), ref confidence);
                myData.ContainerNO = tempString.Substring(0, 11);
                myData.ContainerType = tempString.Substring(11, 4);
                myData.Confidence = confidence;
            }
            finally
            {
                ////detector.Dispose();
                ////recognizer.Dispose();
            }
        }


        //水平方向双文本框检测识别，用于中缝检测
        //从检测结果中判定是否为中缝图像，因为中缝图像的两个箱号不同，并且通常一个正一个反
        //Frame是图像，results是检测结果
        //返回正反标识，1为正，-1为负，0未知
        public static int OcrDetection2(PaddleOcrRecognizer OcrRecognizer, Mat frame, resultPP[] results, ref string CtnNo1, ref string CtnNo2)
        {
            bool PNflag1 = true, PNflag2 = true;

            if (results.Count() == 2)
            {
                Mat roi = frame.Clone(GetCropedRect(results[0].Rect,frame.Size()));
                OcrDetectionPN(OcrRecognizer, roi, ref CtnNo1, ref PNflag1);

                roi = frame.Clone(results[1].Rect);
                OcrDetectionPN(OcrRecognizer, roi, ref CtnNo2, ref PNflag2);

                roi.Dispose();

                if (PNflag1 && !PNflag2)
                {  //一正一反
                    //if (DistanceCalculate(CtnNo1, CtnNo2) >= 4)    //两个箱号差异较大，至少有4个字符不相同
                        return 1;
                }
                else
                {
                    return -1;
                }
        }
            else
            {
                return 0;
            }
        }

        //水平方向文本检测识别
        //从检测结果中提取文本框，并识别为文本返回
        //Frame是图像，results是检测结果
        //返回文本框的垂直位置，如果是-1，则没有识别到
        public static int OcrDetection(PaddleOcrRecognizer OcrRecognizer, Mat frame, resultPP[] results, ref string CtnNo)
        {
            //对最顶上的一个水平文本块进行识别
            var text1 = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Text_Block" && x.Rect.Width > x.Rect.Height * 1.8 && x.Rect.Y > frame.Height * 0.08 && x.Rect.Y < frame.Height * 0.5).OrderBy(x => x.Rect.Y).ToArray();
            CtnNo = "";
            float score1 = 0, score2 = 0;

            if (text1.Length > 0)
            {
                //Mat roi = frame[text1.Rect];
                Mat roi = frame.Clone(GetCropedRect(text1[0].Rect,frame.Size())); 

                string NoString = OcrImage(OcrRecognizer, roi, ref score1);

                if (!DataCheckOK(NoString))
                {
                    if (CheckNo(NoString) < 10)
                    {
                        return -1;  
                    }
                    else
                    {  //反转不识别，否则容易识别到后箱顶部
                        //Cv2.Flip(roi, roi, FlipMode.XY);  //图像水平垂直翻转
                        //string NoStringF = OcrImage(OcrRecognizer, roi, ref score2);
                        //roi.Dispose();

                        //if (score2 > 0.5 && ((DistanceCalculate(NoStringF, VideoForm.CurrentNo1) < 3) || (DistanceCalculate(NoStringF, VideoForm.CurrentNo2) < 3)))
                        //{  //看跟前一次识别的箱号是否一致，如一致说明采集到反的箱号，则放弃本次触发.返回-2
                        //    return -2;
                        //}
                    }
                }
                else
                {
                    roi.Dispose();
                }

                if (CheckNo(NoString) >= 10)  //识别出10个字符就算有箱号
                {
                    CtnNo = NoString;
                    return text1[0].Rect.Y;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }
        
        //水平方向文本检测识别,做正反两次识别，那个置信度高，就返回高的结果，以及正反标志
        //从检测结果中提取文本框，并识别为文本返回
        //roi是图像
        //返回文本框的垂直位置，如果是-1，则没有识别到
        public static int OcrDetectionPN(PaddleOcrRecognizer OcrRecognizer, Mat roi, ref string CtnNo, ref bool PNflag)
        {
            CtnNo = "";
            float score1 = 0, score2 = 0;

            PNflag = true;

            string NoString = OcrImage(OcrRecognizer, roi,ref score1);

            if (!DataCheckOK(NoString))
            {
                Cv2.Flip(roi, roi, FlipMode.XY);  //图像水平垂直翻转
                string NoStringF = OcrImage(OcrRecognizer, roi, ref score2);
                roi.Dispose();

                if ( (score1 > score2) && (Math.Max(score1,score2)>0.5) )
                {
                    PNflag = true;
                }
                else if (Math.Max(score1, score2) > 0.5)
                {
                    NoString = NoStringF;
                    PNflag = false;
                }
                else
                {
                    return -1;   //没有识别出来
                }
            }
            else
            {
                roi.Dispose();
            }

            if (CheckNo(NoString) >= 7)  //识别出7个字符就算有箱号
            {
                CtnNo = NoString;
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public static int CheckNo(string NoString)
        {
            int cnt = 0;

            if (NoString.Length == 0)
            {
                return -1;
            }
            else if (NoString.Length > 11)
            {
                cnt = 11;
            }
            else
            {
                cnt = NoString.Length;
            }

            int kk = 0;
            for (int i = 0; i < cnt; i++)
            {
                char tmpChr = NoString[i];
                if ((i < 3) && Char.IsLetter(tmpChr))
                {
                    kk++;
                }
                if (i == 3)
                {
                    if (tmpChr == 'U')
                        kk++;
                    else
                        kk--;
                }
                if ((i >= 4) && Char.IsDigit(tmpChr))
                {
                    kk++;
                }
            }

            return kk;
        }

        //垂直方向文本检测识别
        public static int OcrDetectionV(PaddleOcrRecognizer OcrRecognizer, Mat frame, resultPP[] results, ref string CtnNo)
        {
            //对最顶上的一个垂直文本块进行识别
            var text1 = results.Where(x => (x.Confidence > 0.5) && (x.LabelName == "Text_Block") && (x.Rect.Width < x.Rect.Height / 5) && (x.Rect.Y > frame.Height * 0.15) && (x.Rect.Y < frame.Height * 0.35) && (x.Rect.X > frame.Width * 0.3)).OrderBy(x => x.Rect.Y).ToArray();
            CtnNo = "";
            float score = 0;
            if (text1.Length > 0)
            {
                Mat roi =  frame.Clone(GetCropedRect(text1[0].Rect, frame.Size()));  //frame[text1.Rect];
                //旋转图像为水平
                Cv2.Transpose(roi, roi);
                Cv2.Flip(roi, roi, FlipMode.X);
 
                string NoString = OcrImage(OcrRecognizer, roi, ref score);

                roi.Dispose();

                CtnNo = NoString;

                if ((score>0.5) && (CheckNo(NoString) >= 10) )  //识别出10个字符就算有箱号
                {
                    CtnNo = NoString;
                    return text1[0].Rect.Y;
                }
                else
                {
                    Console.WriteLine("unrecognized No:" + NoString);
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }
        /// <summary>
        ///对图像块进行文字识别,不检测，返回识别字符串
        /// </summary>
        /// <param name="frame">图像块</param>
        public static string OcrImage(PaddleOcrRecognizer OcrRecognizer, Mat frame, ref float score)
        {
            PaddleOcrRecognizerResult result = new PaddleOcrRecognizerResult();

            result = paddleOcrRun(OcrRecognizer, frame);
 
            score = result.Score;
            if (result.Text.Length >= 11)
            {
                return result.Text.Substring(0, 11);
            }
            else
            {
                return NullPs(result.Text);
            }

            //            recognizer.Dispose();
        }

        public static PaddleOcrRecognizerResult paddleOcrRun(PaddleOcrRecognizer OcrRecognizer, Mat frame)
        {
            while (OcrIsRuning)
            { }

            OcrIsRuning = true;
            PaddleOcrRecognizerResult result = new PaddleOcrRecognizerResult();
            try
            {
                result = OcrRecognizer.Run(frame);
            }
            catch (Exception e)
            { }

            OcrIsRuning = false;
            
            return result;
        }

        public static string NullPs(string str)
        {
            if (str == null)
            {
                return "";
            }
            else
            {
                return str;
            }
        }
        
        //检查箱号是否通过校验
        public static bool DataCheckOK(string ContainerNo)
        {
            if (ContainerNo == null)
            {
                return false;
            }
            if (ContainerNo.Length < 11) 
                return false;
            
            ContainerNo = ContainerNo.Substring(0, 11);

            if (!System.Text.RegularExpressions.Regex.IsMatch(ContainerNo.Substring(0, 4), @"^[A-Z]*$"))   //大写字母
                return false;

            if (!System.Text.RegularExpressions.Regex.IsMatch(ContainerNo.Substring(4, 7), @"^[0-9]*$"))  //数字
                return false;

            if (ContainerNo.Contains("?"))
                return false;

            if ( DataCheck(ContainerNo.Substring(0, 10)).ToString() == ContainerNo.Substring(10) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
  
        public  static bool StaticImageDetection(Mat Frame1, Mat Frame2)
        {
            return false;

            Mat result=new Mat();
            double minVal, maxVal;
            Point minLoc, maxLoc;

            //long t1 = DateTime.Now.Ticks;
            Cv2.Resize(Frame1, Frame1, new Size(Frame1.Width/3, Frame1.Height/3), 0, 0, InterpolationFlags.Linear);
            Cv2.Resize(Frame2, Frame2, new Size(Frame2.Width/3, Frame2.Height/3), 0, 0, InterpolationFlags.Linear);
            Frame2 = Frame2[new Rect(1, 1, Frame2.Width - 2, Frame2.Height - 2)];
            Cv2.MatchTemplate(Frame1, Frame2, result, TemplateMatchModes.CCoeffNormed);
            //corValue = result.At<float>(0,0);
            result.MinMaxLoc(out minVal, out maxVal, out minLoc, out maxLoc);
            //long t2 = (DateTime.Now.Ticks - t1) / 10000;
            
            result.Dispose();

            if (maxVal > 0.96)
                return true;
            else
                return false;
        }
        
        /// <summary>
    /// 根据识别到箱号的前十位计算校验码
    /// 如果箱号中存在？的情况，返回11
    /// </summary>
    /// <param name="Result">识别到的箱号</param>
    /// <returns></returns>
        public static int DataCheck(String Result)
        {
            int AscValue, p = 0, pp = 1;
            char[] cc = Result.ToCharArray();

            for (int i = 0; i < 4; i++)
            {
                AscValue = cc[i];
                if (AscValue == 63)
                {
                    return 11;
                }
                if (AscValue == 65)
                {
                    p += 10 * pp;
                }
                else if (AscValue <= 75)
                {
                    p += (AscValue - 54) * pp;
                }
                else if (AscValue <= 85)
                {
                    p += (AscValue - 53) * pp;
                }
                else if (AscValue <= 90)
                {
                    p += (AscValue - 52) * pp;
                }
                pp *= 2;
            }

            for (int i = 0; i < 6; i++)
            {
                AscValue = cc[i + 4];
                if (AscValue == 63) return 11;
                p += (AscValue - 48) * pp;
                pp *= 2;
            }

            int p1 = ((p % 11)) % 10;

            return p1;
        }

        //NoGather后一次性存储所有图像
        public static  void  imageSaveAll(ProcessInfo myController)
        {
            string path = "";
            string imgName = "";

            if (myController.stateEmptyCar)   //空集卡
            {
                path = DateTime.Now.ToString("yyyy-MM-dd").Replace("-",@"\");
                imgName = DateTime.Now.ToString("HHmmss-");
                ContainerHelper.ImgSave(myController.CarHeadImage, InitialValue.Settings.ImagePath1, path, imgName + "CarHead.jpg");
                ContainerHelper.ImgSave(myController.EmptyCarImage, InitialValue.Settings.ImagePath1, path, imgName + "CarBody.jpg");
            }
            else if (myController.stateVehicles)  //社会车辆
            {
                path = DateTime.Now.ToString("yyyy-MM-dd").Replace("-", @"\");
                imgName = DateTime.Now.ToString("HHmmss-") + "Vehicle.jpg";
                ContainerHelper.ImgSave(myController.CarHeadImage, InitialValue.Settings.ImagePath1, path, imgName);
            }
            else
            {
                ContainerData[] dt = myController.dataList.ToArray();
                for (int i = 0; i < dt.Length; i++)
                {
                    if (dt[i].grab_pb != null)
                    {
                        path = System.IO.Path.GetDirectoryName(dt[i].ImagePath);
                        imgName = System.IO.Path.GetFileName(dt[i].ImagePath);

                        if (dt[i].grab_pb.Image != null)
                        {
                            Mat frame = OpenCvSharp.Extensions.BitmapConverter.ToMat((System.Drawing.Bitmap)dt[i].grab_pb.Image);
                            ContainerHelper.ImgSave(frame, InitialValue.Settings.ImagePath, path, imgName);
                            if (!DataCheckOK(dt[i].ContainerNO))
                            {
                                ContainerHelper.ImgSave(frame, InitialValue.Settings.ImagePath, DateTime.Now.ToString("yyyy-MM").Replace("-", @"\") + @"\Error", imgName);
                            }
                        }
                    }
                }
            }
        }

        //myNoUpdate1:显示箱号1；myNoUpdate2:显示箱号2； myTypeUpdate:显示箱形；
        public static void NoGather(ProcessInfo myController, Action<string, string> myNoUpdate1, Action<string, string> myNoUpdate2,Action<string> myTypeUpdate, Action<string,string,string,string,int> mySendData)
        {
            //清空当次识别的结果
            VideoForm.CurrentNo1 = "";
            VideoForm.CurrentType1 = "";
            VideoForm.CurrentNo2 = "";
            VideoForm.CurrentType2 = "";
            VideoForm.CurrentType = 0;

            List<ContainerData> dataListF = new List<ContainerData>();
            List<ContainerData> dataListB = new List<ContainerData>();
            List<ContainerData> sqlList = new List<ContainerData>();
            //新建综合箱号结构体
            ContainerData myDataF = new ContainerData();
            ContainerData myDataB = new ContainerData();
            ContainerData myData1 = new ContainerData();
            ContainerData myData2 = new ContainerData();

            myDataF.CameraType = "前相机";
            myDataB.CameraType = "后相机";
            myDataF.CheckResult = 0;
            myDataB.CheckResult = 0;

            //分出前后箱号数据
            foreach (ContainerData dt in myController.dataList.ToArray())
            {
                if (dt.CameraType.Contains("前"))
                {
                    dataListF.Add(dt);
                }
                else
                {
                    dataListB.Add(dt);
                }
            }

            if (dataListF.Count == 0)  //只有后侧 
            {
                NoExtract(dataListB, ref myDataB, myController);
                myNoUpdate1(myDataB.ContainerNO, myDataB.ContainerType);
                myData1 = myDataB;
                myData1.ImagePath = "...";
                myData1.CameraType = "综合";
                //最后赋箱型
                if (myDataB.ContainerType != "????")
                {
                    myData1.ContainerType = myDataB.ContainerType;
                }
                if (myDataB.ContainerType[0] == '4' || myDataB.ContainerType[0] == 'L' || myData1.ContainerType == "????")
                {
                    myData1.CameraType += "-长箱";
                    myTypeUpdate("长箱");
                }
                else
                {
                    myData1.CameraType += "-短箱";
                    myTypeUpdate("短箱");
                }
                sqlList.Add(myDataB);
                sqlList.Add(myData1);
            }
            else if (dataListB.Count == 0)  //只有前侧两张图
            {
                //从前箱号数据中综合出myDataF
                NoExtract(dataListF, ref myDataF, myController);
                myNoUpdate1(myDataF.ContainerNO, myDataF.ContainerType);
                myData1 = myDataF;
                myData1.ImagePath = "...";
                myData1.CameraType = "综合";
                //最后赋箱型
                if (myDataF.ContainerType != "????")
                {
                    myData1.ContainerType = myDataF.ContainerType;
                }
                if (myDataF.ContainerType[0] == '4' || myDataF.ContainerType[0] == 'L' || myData1.ContainerType == "????")
                {
                    myData1.CameraType += "-长箱";
                    myTypeUpdate("长箱");
                }
                else
                {
                    myData1.CameraType += "-短箱";
                    myTypeUpdate("短箱");
                }
                sqlList.Add(myDataF);
                sqlList.Add(myData1);
            }
            else // if (dataListB.ToArray().Length != 0)  //前后都有
            {
                NoExtract(dataListF, ref myDataF, myController);
                NoExtract(dataListB, ref myDataB, myController);
                string ShortNo1 = RemoveQuestionMarks(myDataF.ContainerNO);
                string ShortNo2 = RemoveQuestionMarks(myDataB.ContainerNO);
                int NoDistance = DistanceCalculate(ShortNo1, ShortNo2);  //两箱号间距离
                //判定箱型
                if (DataCheckOK(myDataF.ContainerNO) && DataCheckOK(myDataB.ContainerNO) && myDataF.ContainerNO != myDataB.ContainerNO)
                {//双箱?
                    if (NoDistance == 1 && (myDataF.ContainerType[0] == '4' || myDataF.ContainerType[0] == 'L' || myDataB.ContainerType[0] == '4' || myDataB.ContainerType[0] == 'L'))
                    {////居然有只差一位但校验都通过的2个箱号！！！，是1长箱！！！！！
                        if (myDataF.ContainerType[0] == '4' || myDataF.ContainerType[0] == 'L')
                        {
                            myData1 = myDataF;
                        }
                        else
                        {
                            myData1 = myDataB;
                        }
                        myTypeUpdate("长箱");
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合-长箱";
                        myNoUpdate1(myData1.ContainerNO, myData1.ContainerType);
                        sqlList.Add(myDataF);
                        sqlList.Add(myDataB);
                        sqlList.Add(myData1);
                    }
                    else if ( myDataB.ContainerType[0] == '4' || myDataB.ContainerType[0] == 'L')
                    {  //后侧箱型是长箱，则是长箱
                        myData1 = myDataB;

                        myTypeUpdate("长箱");
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合-长箱";
                        myNoUpdate1(myData1.ContainerNO, myData1.ContainerType);
                        sqlList.Add(myDataF);
                        sqlList.Add(myDataB);
                        sqlList.Add(myData1);
                    }
                    else
                    {  //双箱
                        myNoUpdate1(myDataF.ContainerNO, myDataF.ContainerType);
                        myNoUpdate2(myDataB.ContainerNO, myDataB.ContainerType);
                        myTypeUpdate("双箱");
                        myData1 = myDataF;
                        myData1.ImagePath = myDataB.ContainerNO + "," + myDataB.ContainerType;
                        myData1.CameraType = "综合-前箱";
                        myData2 = myDataB;
                        myData2.ImagePath = myDataF.ContainerNO + "," + myDataF.ContainerType;
                        myData2.CameraType = "综合-后箱";
                        sqlList.Add(myDataF);
                        sqlList.Add(myDataB);
                        sqlList.Add(myData1);
                        sqlList.Add(myData2);
                    }
                }
                else if (myDataF.ContainerType[0] == '4' || myDataF.ContainerType[0] == 'L' || myDataB.ContainerType[0] == '4' || myDataB.ContainerType[0] == 'L')
                {
                    //说明是长箱
                    //若前箱可以通过校验
                    if ( (myDataF.CheckResult == 1) && (myDataB.CheckResult == 1) )
                    {
                        if (myDataF.Confidence > myDataB.Confidence)   //选择置信度高的结果
                            myData1 = myDataF;
                        else
                            myData1 = myDataB;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }
                    else if (myDataF.CheckResult == 1)
                    {
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }
                    //若后箱箱号可以通过校验
                    else if (myDataB.CheckResult == 1)
                    {
                        //更新箱号和CheckResult
                        myData1 = myDataB;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }
                    ////若前箱字母和后箱数字组合可以通过校验
                    else if (DataCheckOK(myDataF.ContainerNO.Substring(0, 4) + myDataB.ContainerNO.Substring(4, 7)))
                    {
                        myData1 = myDataF;
                        myData1.ContainerNO = myDataF.ContainerNO.Substring(0, 4) + myDataB.ContainerNO.Substring(4);
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                        myData1.CheckResult = 1;
                    }
                    ////若前箱数字和后箱字母组合可以通过校验
                    else if (DataCheckOK(myDataB.ContainerNO.Substring(0, 4) + myDataF.ContainerNO.Substring(4, 7)))
                    {
                        myData1 = myDataF;
                        myData1.ContainerNO = myDataB.ContainerNO.Substring(0, 4) + myDataF.ContainerNO.Substring(4);
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                        myData1.CheckResult = 1;
                    }
                    //都不行就发前箱的
                    else
                    {
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }

                    //最后赋箱型
                    if ((myDataF.ContainerType[0] == '4') || (myDataF.ContainerType[0] == 'L'))
                    {
                        myData1.ContainerType = myDataF.ContainerType;
                    }
                    else
                    {
                        myData1.ContainerType = myDataB.ContainerType;
                    }

                    myData1.CameraType += "-长箱";
                    myTypeUpdate("长箱");

                    myNoUpdate1(myData1.ContainerNO, myData1.ContainerType);
                    sqlList.Add(myDataF);
                    sqlList.Add(myDataB);
                    sqlList.Add(myData1);
                }
                else if ((ShortNo1.Length < 8 || ShortNo2.Length < 8) && (NoDistance > 3) && !myController.stateM && (myDataF.ContainerType[0] == '2' || myDataB.ContainerType[0] == '2'))
                {//有一个箱号没识别出，又没有检测到中缝，但识别出箱形为短箱，则综合为一短箱
                    myTypeUpdate("短箱");
                    if (ShortNo1.Length > ShortNo2.Length)
                    {//用前侧结果
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合-短箱";
                    }
                    else
                    {
                        myData1 = myDataB;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合-短箱";
                    }
                    myNoUpdate1(myData1.ContainerNO, myData1.ContainerType);
                    sqlList.Add(myDataF);
                    sqlList.Add(myDataB);
                    sqlList.Add(myData1);
                }
                else if (myDataF.ContainerNO == myDataB.ContainerNO)   //前后箱号相同
                {
                    if ((myDataF.ContainerType[0] == '2') || (myDataB.ContainerType[0] == '2'))
                    {  //短箱
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合-短箱";

                        if (myDataB.ContainerType[0] == '2')
                        {
                            myData1.ContainerType = myDataB.ContainerType;
                        }

                        myNoUpdate1(myData1.ContainerNO, myData1.ContainerType);
                        myTypeUpdate("短箱");
                        sqlList.Add(myDataF);
                        sqlList.Add(myDataB);
                        sqlList.Add(myData1);
                    }
                    else
                    { //长箱
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合-长箱";

                        if (myDataF.ContainerType == "????")
                        {
                            myData1.ContainerType = myDataB.ContainerType;
                        }

                        myNoUpdate1(myData1.ContainerNO, myData1.ContainerType);
                        myTypeUpdate("长箱");
                        sqlList.Add(myDataF);
                        sqlList.Add(myDataB);
                        sqlList.Add(myData1);
                    }
                }
                else //说明是双箱
                {
                    myNoUpdate1(myDataF.ContainerNO, myDataF.ContainerType);
                    myNoUpdate2(myDataB.ContainerNO, myDataB.ContainerType);
                    myTypeUpdate("双箱");
                    myData1 = myDataF;
                    myData1.ImagePath = myDataB.ContainerNO + "," + myDataB.ContainerType;
                    myData1.CameraType = "综合-前箱";
                    myData2 = myDataB;
                    myData2.ImagePath = myDataF.ContainerNO + "," + myDataF.ContainerType;
                    myData2.CameraType = "综合-后箱";
                    sqlList.Add(myDataF);
                    sqlList.Add(myDataB);
                    sqlList.Add(myData1);
                    sqlList.Add(myData2);
                }
            }

            //判断是否与前一箱号相同
            if ( (InitialValue.Settings.RepeatRecogFlag == 1) || ((DistanceCalculate(myData1.ContainerNO, myController.lastNo1) > 0) && (DistanceCalculate(myData1.ContainerNO, myController.lastNo2) > 0) &&
                 (DistanceCalculate(myData2.ContainerNO, myController.lastNo1) > 0) && (DistanceCalculate(myData2.ContainerNO, myController.lastNo2) > 0)) )
            {
                myController.lastNo1 = myData1.ContainerNO;
                myController.lastNo2 = myData2.ContainerNO;

                //存入数据库
                save2Sql(sqlList);
 
                //发送箱号
                mySendData(VideoForm.CurrentNo1, VideoForm.CurrentType1, VideoForm.CurrentNo2, VideoForm.CurrentType2, VideoForm.CurrentType);
                
                //一次性图像存盘
                imageSaveAll(myController);
            }
        }

        //从多张图片结果中综合出一个结果
        public static void NoExtract(List<ContainerData> dataList, ref ContainerData myData,ProcessInfo myController)
        {
            if (dataList.Count==0)
            {
                return;
            }

            //写入一下时间数据
            myData.GrabTime = dataList[0].GrabTime;
            //写入一下图像路径
            myData.ImagePath = dataList[0].ImagePath;
            myData.ContainerType = "????";
            //先遍历寻找箱型
            foreach (ContainerData dt in dataList)
            {
                if (dt.ContainerType != "????")
                {
                    myData.ContainerType = dt.ContainerType;
                    break;
                }
            }

            float confidence = 0;
            //再遍历匹配箱号,找到置信度最高的结果
            foreach (ContainerData dt in dataList)
            {
                if ( (dt.CheckResult == 1) && (dt.Confidence > confidence) )
                {
                    myData.ContainerNO = dt.ContainerNO;
                    myData.Confidence = dt.Confidence;
                    myData.CheckResult = 1;
                    confidence = dt.Confidence;
                }
            }
            if (confidence>0) //匹配到就直接return结束流程
            {
                return;
            }

            //能走到这里说明没匹配到能通过校验箱号，所以通过循环来遍历寻找合适的
            //存放前四位箱号信息
            List<string> noListF = new List<string>();
            //存放后七位箱号信息
            List<string> noListS = new List<string>();
            //提取数据
            foreach (ContainerData dt in dataList)
            {
                noListF.Add(dt.ContainerNO.Substring(0, 4));
                noListS.Add(dt.ContainerNO.Substring(4));
            }
            //保个底
            myData.ContainerNO = PadString(SortStrings(noListF.ToArray())[0], 4) + PadString(SortStrings(noListS.ToArray())[0], 7);
            //遍历匹配
            foreach (string dtF in noListF)
            {
                foreach (string dtS in noListS)
                {
                    if (DataCheckOK(dtF + dtS.Substring(0, 7)))
                    {
                        myData.ContainerNO = dtF + dtS;
                        myData.CheckResult = 1;
                        myData.Confidence = 0.5f;  //拼接的结果置信度为0.5
                        return;
                    }
                }
            }
        }

        //编辑距离算法
        public static int DistanceCalculate(string source, string target)
        {
            source = NullPs(source);
            target = NullPs(target);
            if ( ((source == "???????????") || (source == "")) && ((target == "???????????") || (target=="")) )
            {
                return 11;  //如果两个箱号都没有认出来或为空，则认为完全不同
            }
    
            int[,] dp = new int[source.Length + 1, target.Length + 1];

            for (int i = 0; i <= source.Length; i++)
                dp[i, 0] = i;

            for (int j = 0; j <= target.Length; j++)
                dp[0, j] = j;

            for (int i = 1; i <= source.Length; i++)
            {
                for (int j = 1; j <= target.Length; j++)
                {
                    int cost = (source[i - 1] == target[j - 1]) ? 0 : 1;
                    dp[i, j] = Math.Min(Math.Min(dp[i - 1, j] + 1, dp[i, j - 1] + 1), dp[i - 1, j - 1] + cost);
                }
            }
            return dp[source.Length, target.Length];
        }

        public static void save2Sql(List<ContainerData> dataList)
        {
            //用于建立数据库连接
            SQLiteConnection mConn = new SQLiteConnection();
            mConn.ConnectionString = @"data source = " + System.IO.Directory.GetCurrentDirectory() + "/ContainerDB.db";
            mConn.Open();

            try
            {
                foreach (ContainerData dt in dataList)
                {
                    SQLiteCommand cmd = new SQLiteCommand();
                    cmd.Connection = mConn;
                    cmd.CommandText = "INSERT INTO [TABLE] (ContainerNo,ContainerType,CameraType,ImagePath,GrabTime,CheckResult) VALUES (@ContainerNo,@ContainerType,@CameraType,@ImagePath,@GrabTime,@CheckResult)";
                    cmd.Parameters.Add("ContainerNo", System.Data.DbType.String).Value = dt.ContainerNO;
                    cmd.Parameters.Add("ContainerType", System.Data.DbType.String).Value = dt.ContainerType;
                    cmd.Parameters.Add("CameraType", System.Data.DbType.String).Value = dt.CameraType;
                    cmd.Parameters.Add("ImagePath", System.Data.DbType.String).Value = dt.ImagePath;
                    cmd.Parameters.Add("GrabTime", System.Data.DbType.String).Value = dt.GrabTime;
                    cmd.Parameters.Add("CheckResult", System.Data.DbType.Int32).Value = dt.CheckResult;
                    cmd.ExecuteNonQuery();
                }
            }catch(Exception e)
            {

            }
            mConn.Close();
        }

        public static Rect GetCropedRect(Rect rect, OpenCvSharp.Size size)
        {
            int left = ClampValue(rect.Left, 0, size.Width-1);
            int right = ClampValue(rect.Right, 0, size.Width-1);
            if (right < left) right = left;
            int top = ClampValue(rect.Top, 0, size.Height-1);
            int bottom = ClampValue(rect.Bottom, 0, size.Height-1);
            if (bottom < top) bottom = top;

            return Rect.FromLTRB(
                left,
                top,
                right,
                bottom);
        }

        private static int ClampValue(int x, int min, int max)
        {
            if (x < min) return min;
            else
            {
                if (x > max) 
                    return max;
                else
                    return x;
            }
        }

        /// <summary>
        /// 对paddleocr的单张箱号识别结果进行初处理
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private static string NoContact(PaddleOcrRecognizerResult[] results, ref float Confidence)
        {
            //声明三个部分，字母部分，数字部分，箱号部分
            string lettersPart, numbersPart, typePart;
            //匹配箱号
            Regex typeReg = new Regex("[4,2,A-Z][0-9,A-Z][G,U,L,R][0,1,2,3]");
            lettersPart = "????";
            numbersPart = "???????";
            typePart = "????";
            String ContainerNo = "";


            //存放箱号字母部分的字符数组
            string[] letterArray = { };
            //存放箱号数字部分的字符数组
            string[] numberArray = { };

            foreach (PaddleOcrRecognizerResult result in results)
            {
                if ((result.Score > 0.5) && DataCheckOK(result.Text) && (result.Text.Substring(3, 1) == "U"))
                {
                    ContainerNo = result.Text.Substring(0, 11);
                    Confidence = result.Score;
                    break;
                }
            }

            if (ContainerNo == "")  //未找到通过校验的箱号
            {

                //寻找箱号的七位数字，存入数组
                foreach (PaddleOcrRecognizerResult result in results)
                {
                    if (result.Score > 0.5)
                    {
                        numberArray = MergeArrays(numberArray, MatchNumbers(result.Text.Replace("O", "0")));
                    }
                }
                numberArray = FindLongestStrings(numberArray);

                //寻找箱号的前四位字母，存入列表
                //寻找箱号的四位字母，存入数组
                foreach (PaddleOcrRecognizerResult result in results)
                {
                    if (result.Score > 0.5)
                    {
                        letterArray = MergeArrays(letterArray, MatchLetters(result.Text.Replace("0", "O")));
                    }
                }
                letterArray = FindLongestStrings(letterArray);

                //经过校验组合出箱号，如果没有，就把字母部分和数字部分各自赋值为数组的第一个元素
                if (letterArray.Length != 0)
                {
                    lettersPart = letterArray[0];
                }
                if (numberArray.Length != 0)
                {
                    numbersPart = numberArray[0];
                }
                //要判定一下返回的结果是否位数足够
                if (letterArray.Length != 0 && numberArray.Length != 0 && letterArray[0].Length == 4 && numberArray[0].Length == 7)
                {
                    foreach (string letters in letterArray)
                    {
                        foreach (string numbers in numberArray)
                        {
                            if (DataCheckOK(letters + numbers.Substring(0, 7)))
                            {
                                lettersPart = letters;
                                numbersPart = numbers;
                                goto endLoop;
                            }
                        }
                    }
                endLoop:;
                }

                ContainerNo = $"{PadString(lettersPart, 4)}{PadString(numbersPart, 7)}";
                Confidence = 0.5f;
            }

            //再寻找箱型数据
            foreach (PaddleOcrRecognizerResult result in results)
            {
                if (result.Score > 0.5 && result.Text.Length < 5)
                {
                    if (typeReg.IsMatch(result.Text))
                    {
                        //返回匹配的第一个结果
                        typePart = typeReg.Match(result.Text).ToString();
                        if ( (typePart.Substring(0,1) == "4") || (typePart.Substring(0,1) == "2") )
                            break;
                    }
                }
            }
            return ContainerNo + typePart;
        }

        /// <summary>
        /// 图像校正函数
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="input">形如"109,94,539,87,448,370,165,366,91,95,518,91,436,374,167,370"</param>
        public static void ImgCalib(ref Mat frame, Mat warpMatrix)
        {
            try
            {
                Cv2.WarpPerspective(frame, frame, warpMatrix, frame.Size());
            }
            catch(Exception ee)
            {
            }
        }

        /// <summary>
        /// 返回字符串中的所有七位数字，若没有，则返回相对最长的结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] MatchNumbers(string input)
        {
            List<string> results = new List<string>();
            //匹配七位数字
            for (int i = 0; i <= input.Length - 7; i++)
            {
                string substring = input.Substring(i, 7);

                if (IsNumeric(substring))
                {
                    results.Add(substring);
                }
            }
            //若没有符合条件的，则返回相对最长的
            if (results.ToArray().Length == 0)
            {
                string longestNumber = string.Empty;
                string currentNumber = string.Empty;

                foreach (char c in input)
                {
                    if (char.IsDigit(c))
                    {
                        currentNumber += c;
                    }
                    else
                    {
                        if (currentNumber.Length > longestNumber.Length)
                        {
                            longestNumber = currentNumber;
                        }
                        currentNumber = string.Empty;
                    }
                }

                // 处理最后一个数字序列
                if (currentNumber.Length > longestNumber.Length)
                {
                    longestNumber = currentNumber;
                }

                results.Add(longestNumber);
            }
            return results.ToArray();
        }

        //判断字符串中是否都为数字
        public static bool IsNumeric(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 返回字符串中所有四位字母，若没有，则返回相对最长的结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] MatchLetters(string input)
        {
            Regex lettersReg = new Regex("[A-Z]{0,3}U");
            List<string> results = new List<string>();
            //匹配四位字母且最后一位为U的
            for (int i = 0; i <= input.Length - 4; i++)
            {
                string substring = input.Substring(i, 4);
                if (IsAlphabetic(substring) && substring[substring.Length - 1] == 'U')
                {
                    results.Add(substring);
                }
            }
            //若没有符合条件的，则返回相对最长的
            if (results.ToArray().Length == 0)
            {
                if (lettersReg.IsMatch(input))
                {
                    //返回匹配的第一个结果
                    results.Add(lettersReg.Match(input).ToString());
                }
            }
            return results.ToArray();
        }

        //判断字符串中是否都为字母
        public static bool IsAlphabetic(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsLetter(c))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 将字符串数组 B 的内容放入字符串数组 A 中
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        public static string[] MergeArrays(string[] A, string[] B)
        {
            // 创建一个新的数组，大小为 A.Length + B.Length
            string[] result = new string[A.Length + B.Length];

            // 复制 A 数组的内容到结果数组
            Array.Copy(A, result, A.Length);

            // 复制 B 数组的内容到结果数组的末尾
            Array.Copy(B, 0, result, A.Length, B.Length);

            return result;
        }

        /// <summary>
        /// 输入字符串，返回使用？填充到指定位数的字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static string PadString(string str, int N)
        {
            if (str.Length < N)
            {
                int diff = N - str.Length;
                string padding = new string('?', diff);
                return str + padding;
            }

            return str;
        }

        /// <summary>
        /// 移除字符串中所有？
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveQuestionMarks(string str)
        {
            if (str == null)
                return "";
            else
                return str.Replace("?", "");
        }

        /// <summary>
        /// 筛选出字符串数组中字符长度最长的那些元素
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string[] FindLongestStrings(string[] array)
        {
            // 计算最长字符串的长度
            int maxLength = 0;
            foreach (string str in array)
            {
                if (str.Length > maxLength)
                {
                    maxLength = str.Length;
                }
            }

            // 筛选出最长字符串
            List<string> longestStrings = new List<string>();
            foreach (string str in array)
            {
                if (str.Length == maxLength)
                {
                    longestStrings.Add(str);
                }
            }

            // 将最长字符串转换为数组并返回
            return longestStrings.ToArray();
        }


        /// <summary>
        /// 对字符串数组中的字符串进行去除问号后从大到小的排序
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] SortStrings(string[] input)
        {
            // 去除字符串中的 "?"
            string[] processedStrings = input.Select(s => s.Replace("?", "")).ToArray();

            // 按照处理后的字符串长度进行排序
            Array.Sort(processedStrings, (a, b) => b.Length.CompareTo(a.Length));

            return processedStrings;
        }

        //存日志文件
        public static void saveLogFile(string message)
        {
            string LogFileName = ".\\" + DateTime.Now.ToString("yyyy-MM-") + "LogFile.log";
            if (!File.Exists(LogFileName))//判断日志文件是否为当月
                File.Create(LogFileName).Close();//创建文件
            System.IO.StreamWriter writer = File.AppendText(LogFileName);//文件中添加文件流
            writer.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + message);
            writer.Flush();
            writer.Close();
        }

        public static void getCarCountFromLogFile(bool initFlag)
        {
            string dateString = DateTime.Now.ToString("yyyy-MM-dd");
            string LogFileName = ".\\Cars.log";

            if (initFlag)
            {
                if (!File.Exists(LogFileName)) //判断日志文件是否存在
                    VideoForm.carCount = 0;
                else
                {
                    string[] lines = File.ReadAllLines(LogFileName);
                    if (lines.Length == 0)
                    {
                        VideoForm.carCount = 0;
                    }
                    else
                    {
                        if (lines[0].Split(':')[0] == dateString)
                            VideoForm.carCount = Convert.ToInt32(lines[0].Split(':')[1]);
                        else
                            VideoForm.carCount = 0;
                    }
                }
            }
            else
            {
                if (!File.Exists(LogFileName)) //判断日志文件是否存在
                {
                    if (dateString != LastCarDate)
                    {
                        VideoForm.carCount = 1;
                    }
                    else
                        VideoForm.carCount += 1;
                }
                else
                {
                    if (dateString != LastCarDate)
                    {
                        VideoForm.carCount = 1;
                    }
                    else
                    {
                        string[] lines = File.ReadAllLines(LogFileName);
                        if (lines.Length == 0)
                        {
                            VideoForm.carCount += 1;
                        }
                        else
                        {
                            VideoForm.carCount = Convert.ToInt32(lines[0].Split(':')[1]) + 1;
                        }
                    }
                }
            }
            
            LastCarDate = dateString;
            File.WriteAllText(LogFileName, dateString + ":" + VideoForm.carCount.ToString());
        }
    }
}
