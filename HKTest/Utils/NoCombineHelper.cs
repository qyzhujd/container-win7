﻿using HKTest.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContainerDetector.Utils
{   
    /// <summary>
    /// 用来进行集装箱编码识别结果的综合
    /// </summary>
    public static class NoCombineHelper
    {
        //建立公司名字典
        public static Dictionary<string, int> NameDB = new Dictionary<string, int>();
        public static string[] NameString;

        /// <summary>
        /// 从包含所有合格公司名的长字符串中拆分出所有公司，存入字典
        /// </summary>
        /// <param name="NameString1">包含所有合格公司名的长字符串</param>
        public static void Initial(string NameString1)
        {
            NameString = NameString1.Split(',');

            for (int i = 0; i < NameString.Length; i++)
            {
                if (!string.IsNullOrEmpty(NameString[i])) NameDB.Add(NameString[i].Trim(), i);
            }
        }

        /// <summary>
        /// 快速核对公司名称，字典中存在则返回4，不存在则返回0
        /// </summary>
        /// <param name="ContainerNo">识别出的箱号</param>
        /// <returns></returns>
        public static int CheckCompanyName1(String ContainerNo)
        {
            String tmpName = ContainerNo.Substring(0, 4);

            if (NameDB.ContainsKey(tmpName))
                return 4;
            else
                return 0;
        }

        /// <summary>
        /// 判定识别到的箱号是否正确
        /// 错误的几种情况
        /// 1.箱号长度小于10
        /// 2.识别到的箱号校验码与计算得出的校验码不同
        /// </summary>
        /// <param name="Result"></param>
        /// <returns></returns>
        public static bool DataCheckOK(String Result)
        {
            try
            {
                if (Result.Length < 11)
                {
                    return false;
                }
                else if (Result.Substring(10, 1) == "?")
                {
                    return false;
                }
                else if (Convert.ToInt32(Result.Substring(10, 1)) == DataCheck(Result))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// 根据识别到箱号的前十位计算校验码
        /// 如果箱号中存在？的情况，返回11
        /// </summary>
        /// <param name="Result">识别到的箱号</param>
        /// <returns></returns>
        public static int DataCheck(String Result)
        {
            int AscValue, p = 0, pp = 1;
            char[] cc = Result.ToCharArray();

            for (int i = 0; i < 4; i++)
            {
                AscValue = cc[i];
                if (AscValue == 63)
                {
                    return 11;
                }
                if (AscValue == 65)
                {
                    p += 10 * pp;
                }
                else if (AscValue <= 75)
                {
                    p += (AscValue - 54) * pp;
                }
                else if (AscValue <= 85)
                {
                    p += (AscValue - 53) * pp;
                }
                else if (AscValue <= 90)
                {
                    p += (AscValue - 52) * pp;
                }
                pp *= 2;
            }

            for (int i = 0; i < 6; i++)
            {
                AscValue = cc[i + 4];
                if (AscValue == 63) return 11;
                p += (AscValue - 48) * pp;
                pp *= 2;
            }

            int p1 = ((p % 11)) % 10;

            return p1;
        }

        /// <summary>
        /// 为识别出的箱号赋分
        /// 初始为0分，存在一个？加一分，识别出了I和1加0.23分，第四位识别不是U加1分
        /// 分数越高可信度越低
        /// </summary>
        /// <param name="String0">识别出的箱号</param>
        /// <returns></returns>
        public static int GetMissedCharCount(String String0)
        {
            double kk = 0;
            for (int i = 0; i < String0.Length; i++)
            {
                string str = String0.Substring(i, 1);
                if (str == "?")
                    kk++;
                else if ((str == "I") || (str == "1"))
                    kk += 0.23;
            }

            if (String0.Substring(3, 1) != "U")
                kk++;

            return Convert.ToInt32(kk);
        }

        /// <summary>
        /// 交换变量，用于交换出置信度最大的箱号
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        public static void SwapVariable(ref string s1, ref string s2)
        {
            string ts = s2;
            s2 = s1;
            s1 = ts;
        }
        public static void SwapVariable(ref int s1, ref int s2)
        {
            int ts = s2;
            s2 = s1;
            s1 = ts;
        }

        /// <summary>
        /// 如果箱号数组中有两个箱号的处理方式
        /// </summary>
        /// <param name="CharString">箱号数组</param>
        /// <param name="MissedCharCount">每个箱号的对应得分</param>
        /// <returns></returns>
        public static string CombineNo2(string[] CharString, int[] MissedCharCount)
        {
            if (MissedCharCount[0] == 0)
            {
                if ((MissedCharCount[1] == 0) && (CheckCompanyName1(CharString[0]) == 4) && (CheckCompanyName1(CharString[1]) == 4))
                    return CharString[1];
                else
                    return CharString[0];
            }
            else
            {
                string tmp1 = CharString[0].Substring(0, 4) + CharString[1].Substring(4, 7);
                string tmp2 = CharString[1].Substring(0, 4) + CharString[0].Substring(4, 7);
                if ((GetMissedCharCount(tmp1) == 0) && DataCheckOK(tmp1))
                    return tmp1;
                else if ((GetMissedCharCount(tmp2) == 0) && DataCheckOK(tmp2))
                    return tmp2;
                else
                    return CharString[0];
            }
        }

        /// <summary>
        /// 用于拼接箱号
        /// </summary>
        /// <param name="CharString">从拍摄到的图像中识别出的箱号字符串数组</param>
        /// <returns></returns>
        public static string CombineNoX(params string[] CharString)
        {
            //slen是字符串数组的长度
            int slen = CharString.Length;
            //创建一个等长的字符串数组
            string[] CountString = new string[slen];
            //创建一个等长的整数数组
            int[] MissedCharCount = new int[slen];

            //遍历箱号
            for (int i = 0; i < slen; i++)
            {
                //如果箱号数组中出现空对象，返回空字符串
                if (CharString[i] == null)
                {
                    return "";
                }
                //ll为箱号长度
                int ll = CharString[i].Length;

                //若箱号长度等于11且无法通过箱号校验，则在末尾添加一个？号
                if ((ll == 11) && (!DataCheckOK(CharString[i])))
                {
                    CharString[i] = CharString[i] + "?";
                }
                //若箱号长度小于11，则在箱号末尾补？补到长度为12
                else if (CharString[i].Length < 11)
                {
                    CharString[i] = CharString[i] + new string(Convert.ToChar("?"), 12 - ll);
                }
                //为箱号赋分，分值越高越不可信
                MissedCharCount[i] = GetMissedCharCount(CharString[i]);
            }

            //如果箱号数组中只有一个箱号，直接返回
            if (slen == 1)
                return CharString[0];

            //如果箱号数组中有多个箱号，则按照得分值低在前的顺序排序
            for (int i = 0; i < slen; i++)
                for (int j = i + 1; j < slen; j++)
                {
                    if (MissedCharCount[i] > MissedCharCount[j])
                    {
                        SwapVariable(ref CharString[i], ref CharString[j]);
                        SwapVariable(ref MissedCharCount[i], ref MissedCharCount[j]);
                    }
                }

            //两个箱号
            if (slen == 2)
            {
                return CombineNo2(CharString, MissedCharCount);
            }

            //三个以上箱号
            if ((MissedCharCount[0] == 0) && (MissedCharCount[1] > 0))     //一个箱号准确
            {
                //若公司名检查也通过，则直接返回识别结果1
                if (CheckCompanyName1(CharString[0]) == 4)
                    return CharString[0];
                //若识别结果1的公司名检查不通过，则看得分第二的箱号，公司名是否通过，通过且得分小于等于2分，返回识别结果2
                else if ((CheckCompanyName1(CharString[1]) == 4) && (MissedCharCount[1] <= 2))
                    return CharString[1];
                //否则返回识别结果1
                else
                    return CharString[0];
            }
            //两个箱号准确的情况下，优先返回公司名检查正确的箱号
            else if ((MissedCharCount[0] == 0) && (MissedCharCount[1] == 0) && (MissedCharCount[2] > 0))     //二个箱号准确
            {   
                if (CharString[0] == CharString[1])
                    return CharString[0];
                else
                {
                    if (CharString[0].Substring(0, 4) == CharString[2].Substring(0, 4))
                        return CharString[0];
                    else if (CharString[1].Substring(0, 4) == CharString[2].Substring(0, 4))
                        return CharString[1];
                    else
                    {
                        if (CheckCompanyName1(CharString[0]) == 4)
                            return CharString[0];
                        else if (CheckCompanyName1(CharString[1]) == 4)
                            return CharString[1];
                        else
                            return CharString[0];
                    }
                }
            }
            //三个箱号准确的情况下，优先返回公司名检查正确的箱号
            else if ((MissedCharCount[0] == 0) && (MissedCharCount[1] == 0) && (MissedCharCount[2] == 0))   //三个箱号准确
            {
                if (CharString[0] == CharString[1])
                    return CharString[0];
                else if (CharString[0] == CharString[2])
                    return CharString[0];
                else if (CharString[1] == CharString[2])
                    return CharString[1];
                else
                {
                    if (CheckCompanyName1(CharString[0]) == 4)
                        return CharString[0];
                    else if (CheckCompanyName1(CharString[1]) == 4)
                        return CharString[1];
                    else if (CheckCompanyName1(CharString[2]) == 4)
                        return CharString[2];
                    else
                        return CharString[0];
                }
            }
            //这种对应识别结果比较糟糕的情况，就拿前两个结果随便处理一下返回
            else
            {
                return CombineNo2(CharString, MissedCharCount);
            }
        }

        //private static void CharactorRecognize(ProcessInfo controller)
        //{
        //    //string数组，用来存放识别结果，顺序是后，前，左一，左二，右一，右二
        //    //初始化paddleocr检测器
        //    PaddleOcrDetector detector = new PaddleOcrDetector("./output/det_inference/");
        //    //初始化paddleocr识别器
        //    PaddleOcrRecognizer reconizer = new PaddleOcrRecognizer(RecognizationModel.FromDirectory("./output/rec_inference/Student/", "./myLabel.txt", ModelVersion.V3));

        //    using (PaddleOcrAll all = new PaddleOcrAll(detector, null, reconizer)
        //    {
        //        AllowRotateDetection = true, /* 允许识别有角度的文字 */
        //        Enable180Classification = false, /* 允许识别旋转角度大于90度的文字 */
        //    })
        //    {
        //        //后侧
        //        if (!(controller.backImg is null))
        //        {
        //            PaddleOcrResult result = all.Run(controller.backImg);
        //            Console.WriteLine("Detected all texts: \n" + result.Text);
        //            foreach (PaddleOcrResultRegion region in result.Regions)
        //            {
        //                Console.WriteLine($"Text: {region.Text}, Score: {region.Score}, RectCenter: {region.Rect.Center}, RectSize:    {region.Rect.Size}, Angle: {region.Rect.Angle}");
        //            }
        //        }
        //        //前侧
        //        if (!(controller.frontImg is null))
        //        {
        //            PaddleOcrResult result = all.Run(controller.backImg);
        //            Console.WriteLine("Detected all texts: \n" + result.Text);
        //            foreach (PaddleOcrResultRegion region in result.Regions)
        //            {
        //                Console.WriteLine($"Text: {region.Text}, Score: {region.Score}, RectCenter: {region.Rect.Center}, RectSize:    {region.Rect.Size}, Angle: {region.Rect.Angle}");
        //            }
        //        }
        //        //左一
        //        if (!(controller.leftImg1 is null))
        //        {
        //            PaddleOcrResult result = all.Run(controller.backImg);
        //            Console.WriteLine("Detected all texts: \n" + result.Text);
        //            foreach (PaddleOcrResultRegion region in result.Regions)
        //            {
        //                Console.WriteLine($"Text: {region.Text}, Score: {region.Score}, RectCenter: {region.Rect.Center}, RectSize:    {region.Rect.Size}, Angle: {region.Rect.Angle}");
        //            }
        //        }
        //        //左二
        //        if (!(controller.leftImg2 is null))
        //        {
        //            PaddleOcrResult result = all.Run(controller.backImg);
        //            Console.WriteLine("Detected all texts: \n" + result.Text);
        //            foreach (PaddleOcrResultRegion region in result.Regions)
        //            {
        //                Console.WriteLine($"Text: {region.Text}, Score: {region.Score}, RectCenter: {region.Rect.Center}, RectSize:    {region.Rect.Size}, Angle: {region.Rect.Angle}");
        //            }
        //        }
        //        //右一
        //        if (!(controller.rightImg1 is null))
        //        {
        //            PaddleOcrResult result = all.Run(controller.backImg);
        //            Console.WriteLine("Detected all texts: \n" + result.Text);
        //            foreach (PaddleOcrResultRegion region in result.Regions)
        //            {
        //                Console.WriteLine($"Text: {region.Text}, Score: {region.Score}, RectCenter: {region.Rect.Center}, RectSize:    {region.Rect.Size}, Angle: {region.Rect.Angle}");
        //            }
        //        }
        //        //右二
        //        if (!(controller.rightImg2 is null))
        //        {
        //            PaddleOcrResult result = all.Run(controller.backImg);
        //            Console.WriteLine("Detected all texts: \n" + result.Text);
        //            foreach (PaddleOcrResultRegion region in result.Regions)
        //            {
        //                Console.WriteLine($"Text: {region.Text}, Score: {region.Score}, RectCenter: {region.Rect.Center}, RectSize:    {region.Rect.Size}, Angle: {region.Rect.Angle}");
        //            }
        //        }
        //    }
        //}


    }
}
