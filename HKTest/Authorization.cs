﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Text;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Configuration;
using System.Xml;
using System.Linq;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Management;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;

namespace HKTest
{
    public static class Authorization
    {
        [DllImport("winmm")]
        public static extern uint timeGetTime();

        public static string MachineID="";

        public static int PassCheck()
        {
            //DogDemo.DogDemo MyDog = new DogDemo.DogDemo();

            //if (InitialValue.Settings.GateName == "实验道口")
            //    return 0;

            //if (!MyDog.DogVerify())
            //    return 1;
            //else
            //{
                if (InitialValue.Settings.RegNo.Trim() == "")
                {
                    return 2;
                }
                else
                {
                    if (!RegNoVerification(InitialValue.Settings.RegNo.Trim()))
                        return 2;
                    else
                        return 0;
                }
            //}

        }

        //验证注册ID
        public static bool RegNoVerification(string RegNo)
        {
            if  (MachineID =="")
                MachineID = getMachineID();     //GetMac();
            //return true;
            string GeneratedNo = GenerateRegNo(MachineID);
        
            if (RegNo != GeneratedNo)
                return false;
            else
                return true;
        }

        public static string getMachineID()
        {
            string[] mocString;
            string MachineID = "";

            //MachineID = GetMac();
            mocString = GetMoc();
            for (int i = 0; i < 2; i++)
            {
                mocString[i] = mocString[i].Replace(":", "");
                if (mocString[i].Length > 6)
                    MachineID += mocString[i].Substring(mocString[i].Length - 6, 6);
                else
                    MachineID += mocString[i];
            }

            return MachineID;
        }

        public static string GenerateRegNo(string MachineID)
        {
            string RegNo, t1;
            int tt;

            RegNo = "";
            for (int i = 0; i < MachineID.Length; i++)
            {
                t1 = MachineID.Substring(i, 1);
                if (IsNumeric(t1))
                {
                    tt = (i + Convert.ToInt32(t1)) % 10;
                }
                else
                {
                    byte[] array = Encoding.ASCII.GetBytes(t1);
                    tt = (i + array[0]) % 10;
                }
                RegNo = tt.ToString() + RegNo;
            }
          
            return RegNo;
        }
        
        public static bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^[+-]?/d*[.]?/d*$");
        }


        public static string GetCpu()
        {
            string str="";

            ManagementClass mcCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection mocCpu = mcCpu.GetInstances();
            foreach (ManagementObject m in mocCpu)
            {
                str = m["ProcessorId"].ToString();
                break;
            }

            return str;
        }

        public static string GetMac()
        {
            string str = "";

            ManagementClass mcMAC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection mocMAC = mcMAC.GetInstances();
            foreach (ManagementObject m in mocMAC)
            {
                if ((bool)m["IPEnabled"])
                {
                    str = m["MacAddress"].ToString();
                    break;
                }
            }

            return str.Replace(":","A");
        }
        
        public static string[] GetMoc()
        {
            string[] str = new string[3];

            ManagementClass mcCpu = new ManagementClass("win32_Processor");
            ManagementObjectCollection mocCpu = mcCpu.GetInstances();
            foreach (ManagementObject m in mocCpu)
            {
                str[0] = m["ProcessorId"].ToString();
            }

            ManagementClass mcHD = new ManagementClass("win32_logicaldisk");
            ManagementObjectCollection mocHD = mcHD.GetInstances();
            foreach (ManagementObject m in mocHD)
            {
                if (m["DeviceID"].ToString() == "C:")
                {
                    str[1] = m["VolumeSerialNumber"].ToString();
                    break;
                }
            }

            //不知什么原因，执行时延时太长了！！！放弃
            ManagementClass mcMAC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection mocMAC = mcMAC.GetInstances();
            foreach (ManagementObject m in mocMAC)
            {
                if ((bool)m["IPEnabled"])
                {
                    str[2] = m["MacAddress"].ToString().Replace(":","");
                    break;
                }
            }

            //str[2] = GetMacByNetworkInterface().ToArray().FirstOrDefault();
            //if (str[2] == null)
            //{
            //    MessageBox.Show("无网络连接，程序退出！");
            //    System.Environment.Exit(0);
            //}
            return str;
        }
        //返回描述本地计算机上的网络接口的对象(网络接口也称为网络适配器)。
        public static NetworkInterface[] NetCardInfo()
        {
            return NetworkInterface.GetAllNetworkInterfaces();
        }

        // 通过NetworkInterface读取网卡Mac 
        public static List<string> GetMacByNetworkInterface()
        {
            List<string> macs = new List<string>();
            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface ni in interfaces)
            {
                if (ni.Description.Contains("Ethernet"))
                    macs.Add(ni.GetPhysicalAddress().ToString());
            }
            return macs;
        }
    }
}