﻿using HKTest.Models;
using HKTest.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
//using PaddleOCRSharp;
using Sdcb.PaddleInference;
using Sdcb.PaddleOCR;
using OpenCvSharp;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace HKTest
{
    public partial class VideoForm : Form
    {
        //用于OCR识别的六张图对应的PB数组
        private PictureBox[] picImage = new PictureBox[8];
        //用于显示视频流对应的PB数组
        private PictureBox[] picBox = new PictureBox[2];
        //后侧检测模块
        private BackDetectorModel backModel;
        //前侧检测模块
        private FrontDetectorModel frontModel;
        //多线程共享变量
        private ProcessInfo myController;
        //窗口时钟

        //当次识别的结果
        public static int carCount = 0;
        public static string CurrentNo1 = "";
        public static string CurrentType1 = "";
        public static string CurrentNo2 = "";
        public static string CurrentType2 = "";
        public static int CurrentType = 0;    //0短箱，1双箱，2长箱, 4-空车，5-社会车辆

        //前侧触发时间、前后侧识别次数
        public static long CaptureTime1;
        public static long timeDuration;  //前后侧采集时间差
        public static int carSpeedLine;  //以图像运动行数为单位的车速 
        public static int frameNumF = 0;     //前侧识别帧数
        public static int frameNumB = 0;     //后侧识别帧数

        //计时器用静态变量
        public static int tCount = 0;
 
        //图像静止标志
        public static bool ImageStaticFlag1;
        public static bool ImageStaticFlag2;

        //手动触发标记
        public static bool FrontTriggerFlag;
        public static bool BackTriggerFlag;

        //显示标记框标志
        public static bool ShowBoxRectFlag;
        public static Point FrontP1, FrontP2;          //前视频箱子矩形框的左上角和右下角
        public static Point BackP1, BackP2;          //后视频箱子矩形框的左上角和右下角

        //网络通信类
        TcpServerRel TcpServer;
        public static string tcpMessage0;   //上一次网络通信状态
        public static string tcpMessage;    //当前网络通信状态
        private System.Threading.Timer tmTiming;          //时间显示及日常处理定时器
        private System.Threading.Timer tmNetworking;          //通信处理定时器
        public static long ActiveTimeRel = 0;          //最新收到通信数据的时间

        //用于窗体resize
        private Dictionary<string, Point> Location0 = new Dictionary<string, Point>();
        private Dictionary<string, Size> Size0 = new Dictionary<string, Size>();
        private Dictionary<string, Font> Font0 = new Dictionary<string, Font>();

        public VideoForm()
        {
            InitializeComponent();

            //InitializeMediaControl();  //移到定时器中，以便先显示窗口

            InitializationNetworking();

            //FrontP1.X = Convert.ToInt32(InitialValue.Video[0].x1 * FrontVideoPB.Size.Width);
            //FrontP1.Y = Convert.ToInt32(InitialValue.Video[0].y1 * FrontVideoPB.Size.Height);
            //FrontP2.X = Convert.ToInt32(InitialValue.Video[0].x2 * FrontVideoPB.Size.Width);
            //FrontP2.Y = Convert.ToInt32(InitialValue.Video[0].y2 * FrontVideoPB.Size.Height);

            //BackP1.X = Convert.ToInt32(InitialValue.Video[1].x1 * FrontVideoPB.Size.Width);
            //BackP1.Y = Convert.ToInt32(InitialValue.Video[1].y1 * FrontVideoPB.Size.Height);
            //BackP2.X = Convert.ToInt32(InitialValue.Video[1].x2 * FrontVideoPB.Size.Width);
            //BackP2.Y = Convert.ToInt32(InitialValue.Video[1].y2 * FrontVideoPB.Size.Height); 
            
            lbDateTime.Text = DateTime.Now.ToString();
            lblGateName.Text = InitialValue.Settings.GateName;

            ContainerHelper.getCarCountFromLogFile(true);

            ContainerHelper.saveLogFile("程序启动！");
            Console.WriteLine("程序启动！" );
        }


        private void InitializationNetworking()
        {
            TcpServer = new TcpServerRel();

            lblNetworking.Text = "等待连接！";

            try
            {
                tmTiming = new System.Threading.Timer(new TimerCallback(timerTiming), null, 0, 250);
                tmNetworking = new System.Threading.Timer(new TimerCallback(timerNetworking), null, 0, 1000);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (InitialValue.Network.SendProtocalType < 2) //本端作为服务端，连接客户端
            {
                TcpServer.tcc = new Thread(TcpServer.TcpConnectClient);  //初次启动线程等待连接
                TcpServer.tcc.IsBackground = true;
                TcpServer.tcc.Start();
            }
            else 
            {
                TcpServer.TcpConnectServer(); //本端作为客户端，初次尝试连接服务端
            }
        }


        private void InitializeMediaControl()
        {
            //识别流程结构体初始化
            myController = new ProcessInfo();
            myController.verifyCode = 111111;
            
            //设置临时存储图像的temp文件夹
            myController.tempPath = Path.Combine(Environment.CurrentDirectory, "temp");
            if (!Directory.Exists(myController.tempPath))
            {
                Directory.CreateDirectory(myController.tempPath);
            }
            //初始状态为wait
            myController.state = ArrivedState.Wait;
            //初始化箱号信息List
            myController.dataList = new List<ContainerData>();
            myController.lbState = this.lbState;


            //检测模块初始化
            PictureBox[] pbArrayF = { FImgPB1, FImgPB2 };
            frontModel = new FrontDetectorModel(ref myController, FrontVideoPB, pbArrayF, new Action(picImageClear), lbState, FCameraState, NoGatherAll);
            PictureBox[] pbArrayB = { BImgPB1, BImgPB2, BTImgPB1, BTImgPB2, FTImgPB1, FTImgPB2 };
            backModel = new BackDetectorModel(ref myController, BackVideoPB, pbArrayB, new Action(picImageClear), lbState, BCameraState, NoGatherAll);
        }

        //清理picImage
        private void picImageClear()
        {
            if (myController.picClearFlag == true) return;

            myController.picClearFlag = true;

            for (int i = 0; i < 8; i++)
            {
                picImage[i].Image = null;
            }

            if (FTab.InvokeRequired)
            {
                FTab.Invoke(new Action(() =>
                {
                    FTab.SelectedIndex = 0;
                }));
            }
            if (BTab.InvokeRequired)
            {
                BTab.Invoke(new Action(() =>
                {
                    BTab.SelectedIndex = 2;
                }));
            }

            if (textNo1.InvokeRequired)
            {
                textNo1.Invoke(new Action(() =>
                {
                    textNo1.Text = "";
                    textType1.Text = "";
                    textNo2.Text = "";
                    textType2.Text = "";
                }));
            }
            else
            {
                textNo1.Text = "";
                textType1.Text = "";
                textNo2.Text = "";
                textType2.Text = "";
            }
        }

        private void timeUpdate(object a)
        {
            //每秒钟更新一次时间
            if (lbDateTime.InvokeRequired)
            {
                lbDateTime.Invoke(new Action(() =>
                {
                    lbDateTime.Text = DateTime.Now.ToString();// +"  "+  myController.state.ToString();
                }));
            }
            if (lblNetworking.InvokeRequired)
            {
                lblNetworking.Invoke(new Action(() =>
                {
                    //lblNetworking.Text = DateTime.Now.ToString();// +"  "+  myController.state.ToString();
                }));
            }          
        }

        /// <summary>
        /// 窗体加载时完成的事
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            //保存初始控件位置
            for (int i = 0; i < this.Controls.Count; i++)
            {
                Location0.Add(this.Controls[i].Name, this.Controls[i].Location);
                Size0.Add(this.Controls[i].Name, this.Controls[i].Size);
                Font0.Add(this.Controls[i].Name, this.Controls[i].Font);
            }
            //初始化图片显示控件，并为其绑定双击事件
            picImage = new PictureBox[8] { FImgPB1, FImgPB2, FTImgPB1,FTImgPB2, BTImgPB1, BTImgPB2, BImgPB1,BImgPB2};
            for(int i = 0; i < 8; i++)
            {
                picImage[i].DoubleClick += new System.EventHandler(this.picImage_DoubleClick);
                picImage[i].SizeMode = PictureBoxSizeMode.StretchImage;
            }
            //初始化视频显示控件
            picBox = new PictureBox[2] { BackVideoPB, FrontVideoPB };
            for (int i = 0; i < 2; i++)
            {
                //设置图像显示方式为拉伸适应
                picBox[i].SizeMode = PictureBoxSizeMode.StretchImage;
                //为图像控件绑定双击事件
                picBox[i].DoubleClick += new System.EventHandler(this.picBox_DoubleClick);
            }
            //设置抓图区默认显示后图
            BTab.SelectedTab = BImg1;
        }
        

        /// <summary>
        /// 该函数用于配合tab页图像的放大缩小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picImage_DoubleClick(object sender, EventArgs e)
        {
            //将事件的发送者转换为pictureBox
            PictureBox pic = (PictureBox)sender;
            //pictureBox的父控件是TabPage，TabPage的父控件是TabControl
            TabControl tab = (TabControl)pic.Parent.Parent;
            //tabControl的宽度，这是人为设定好的
            int w2 = 480;

            //获取该TabControl控件的Tag值，若为0，代表双击的是左半区Tab，否则代表双击的是右半区Tab
            if (tab.Tag.ToString() == "0")
            {
                if (tab.Width > w2 + 2)
                {
                    tab.Location = new System.Drawing.Point(0, 292);
                    tab.Size = new System.Drawing.Size(480, 270);
                }
                else
                {
                    tab.Location = new System.Drawing.Point(0, 0);
                    tab.Size = new System.Drawing.Size(PnlVideo.Width, PnlVideo.Height);
                }
            }
            else
            {
                if (tab.Width > w2 + 2)
                {
                    tab.Location = new System.Drawing.Point(481, 292);
                    tab.Size = new System.Drawing.Size(480, 270);
                }
                else
                {
                    tab.Location = new System.Drawing.Point(0, 0);
                    tab.Size = new System.Drawing.Size(PnlVideo.Width, PnlVideo.Height);
                }
            }
            //使用此方法使其被置于顶层
            tab.BringToFront();
        }

        /// <summary>
        /// 该函数用于配合tab页图像的放大缩小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl_Resize(object sender, EventArgs e)
        {
            TabControl tab = (TabControl)sender;

            int ii = Convert.ToInt32(tab.Tag) * 4;
            for (int i = 0 + ii; i < 4 + ii; i++)
            {
                if (picImage[i] != null) 
                {
                    picImage[i].Size = new Size(tab.Size.Width - 8, tab.Size.Height - 26);
                }
            }
        }

        /// <summary>
        /// 该函数用于配合视频流预览图像的放大缩小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_DoubleClick(object sender, EventArgs e)  //预览窗口双击放大恢复
        {
            //获取事件的发送者，将其转变为PictureBox类型
            PictureBox pic = (PictureBox)sender;
            //pictureBox的宽度，这是人为设定好的
            int w2 = 480;
            //若当前被双击的PictureBox的宽度大于w2，说明处于窗口放大状态，因此双击后应当恢复四窗口
            //恢复的方法是遍历所有窗口，将其位置、大小重设，并设置可见
            if (pic.Width > w2 + 2)  //恢复四窗口
            {
                if (pic.Tag.ToString() == "0")
                {
                    pic.Location = new System.Drawing.Point(0, 21);
                    pic.Size = new System.Drawing.Size(480, 270);
                }
                else
                {
                    pic.Location = new System.Drawing.Point(481, 21);
                    pic.Size = new System.Drawing.Size(480, 270);
                }
            }
            else  //放大窗口
            //放大的方法是将待放大的窗口的位置、大小重设，其他的设为不可见
            {
                pic.Location = new System.Drawing.Point(0, 0);
                pic.Size = new System.Drawing.Size(PnlVideo.Width, PnlVideo.Height);
                pic.BringToFront();
            }

        }

        /// <summary>
        /// 窗口的resize事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VideoForm_Resize(object sender, EventArgs e)
        {
            double kx, ky;
            //kx比Form的宽度少8，ky比Form的高度少5
            kx = this.Width / 977.0; ky = this.Height / 763.0;
            for (int i = 0; i < this.Controls.Count; i++)
            {
                Control contr = this.Controls[i];
                string nm = contr.Name;
                contr.Location = new Point(Convert.ToInt32(Location0[nm].X * kx), Convert.ToInt32(Location0[nm].Y * ky));
                contr.Size = new Size(Convert.ToInt32(Size0[nm].Width * kx), Convert.ToInt32(Size0[nm].Height * ky));
                contr.Font = new Font(contr.Font.Name, Convert.ToSingle(kx) * Font0[nm].Size);
            }
        }

        private void ImageShow2Pb(Mat frame, PictureBox pb)
        {
            if (pb.InvokeRequired)
            {
                pb.Invoke(new Action(() =>
                {
                    pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                    //System.GC.Collect();
                }));
            }
        }


        private void NoGatherAll()
        {
            if (myController.stateEmptyCar)   //空车
            {
                int CurrentType = CmbSelectedItemSetGet(cmbContainerType, "空车");  
                //cmbContainerType.SelectedIndex = 4;
                //发送箱号
                SendData("", "", "", "", 4);

                TextUpdate1("", "");
                TextUpdate2("", "");
                CmbUpdate("空车");

                //一次性图像存盘
                ContainerHelper.imageSaveAll(myController);
                ImageShow2Pb(myController.CarHeadImage, FImgPB1);
                ImageShow2Pb(myController.EmptyCarImage, BImgPB1);
            }
            else if (myController.stateVehicles) //社会车辆
            {
                int CurrentType = CmbSelectedItemSetGet(cmbContainerType, "社会车辆");
                //cmbContainerType.SelectedIndex = 5;


                //发送箱号
                SendData("", "", "", "", 5);

                TextUpdate1("", "");
                TextUpdate2("", "");
                CmbUpdate("社会车辆");

                //一次性图像存盘
                //TextUpdate1("", "");
                //TextUpdate2("", "");

                ContainerHelper.imageSaveAll(myController);
                ImageShow2Pb(myController.CarHeadImage, FImgPB1);
            }
            else
            {
                //箱号综合
                ContainerHelper.NoGather(myController, TextUpdate1, TextUpdate2, CmbUpdate, SendData);
            }

            //清空dataList
            myController.dataList = new List<ContainerData>();
        }

        private void TextUpdate1(string containerNo,string containerType)
        {
            //CurrentNo1 = containerNo;     //保留当前箱号
            //Console.WriteLine("C:" + CurrentNo1);
            //CurrentType1 = containerType;     //保留当前箱形

            ControlTextSetGet(textNo1, containerNo); //设置当前箱号
            ControlTextSetGet(textType1, containerType);
        }

        private void TextUpdate2(string containerNo, string containerType)
        {
            //CurrentNo2 = containerNo;      //保留当前箱号
            //CurrentType2 = containerType;     //保留当前箱形

            ControlTextSetGet(textNo2, containerNo);  //设置当前箱号
            ControlTextSetGet(textType2, containerType);   
        }

        private void CmbUpdate(string containerType)
        {
           CurrentType = CmbSelectedItemSetGet(cmbContainerType, containerType);  
        }

        //对控件的text属性进行赋值，返回当前的文本，当str == null时不赋值，仅读取
        public static string ControlTextSetGet(Control ctrl, string str = null)
        {
            string rtnString = "";

            if (ctrl.InvokeRequired)
            {
                ctrl.Invoke(new Action(() =>
                {
                    if (str != null)
                    {
                        ctrl.Text = str;
                    }
                    rtnString = ctrl.Text;
                }));
            }
            else
            {
                if (str != null)
                {
                    if (str != null)
                    {
                        ctrl.Text = str;
                    }
                    rtnString = ctrl.Text;
                }
            }
            return rtnString;
        }


        //对combobox控件的selectedItem属性进行赋值，返回当前的SelectedIndex；当str == null时，仅读取SelectedIndex
        private int CmbSelectedItemSetGet(ComboBox ctrl, string str = null)
        {
            int rtnInt = -1;

            if (ctrl.InvokeRequired)
            {
                ctrl.Invoke(new Action(() =>
                {
                    if (str != null)
                    {
                        ctrl.SelectedItem = str;
                    }
                    rtnInt = ctrl.SelectedIndex;
                }));
            }
            else
            {
                if (str != null)
                {
                    ctrl.SelectedItem = str;
                }
                rtnInt = ctrl.SelectedIndex;
            }

            return rtnInt;
        }

        private void buttonStatistics_Click(object sender, EventArgs e)
        {
            //new Form1().ShowDialog();
            new Form1().Show();
        }

        public void button1_Click(object sender, EventArgs e)
        {
            //string ContainerNo1 = ControlTextSetGet(textNo1);
            //string ContainerType1 = ControlTextSetGet(textType1);
            //string ContainerNo2 = ControlTextSetGet(textNo2);
            //string ContainerType2 = ControlTextSetGet(textType2);
            //int ContainerType = CmbSelectedItemSetGet(cmbContainerType);
            if (textNo1.Text == "")
            {
                MessageBox.Show("无箱号！");
                return;
            }
            if (textType1.Text == "")
            {
                MessageBox.Show("无箱型！");
                return;
            }
            if (cmbContainerType.SelectedIndex < 0)
                cmbContainerType.SelectedIndex = 0;

            if (InitialValue.Network.SendProtocalType < 2) //本端作为服务端，连接客户端
            {
                if (!TcpServer.TcpConnectClientRetry())
                {
                    lblNetworking.Text = "等待连接！";
                }
                else
                {
                    SendData(textNo1.Text, textType1.Text, textNo2.Text, textType2.Text, cmbContainerType.SelectedIndex);
                }
            }
            else
            {
                TcpServer.TcpConectServerRetry();

                if (TcpServer.client == null)
                {
                    lblNetworking.Text = "等待连接！";
                }
                else
                {
                    if (TcpServer.client.Connected)
                    {
                        SendData(textNo1.Text, textType1.Text, textNo2.Text, textType2.Text, cmbContainerType.SelectedIndex);
                    }
                    else
                    {
                        lblNetworking.Text = "等待连接！";
                    }
                }
            }
        }

        public void SendData(string ContainerNo1, string ContainerType1, string ContainerNo2, string ContainerType2, int ContainerType)
        {
            if (ContainerNo1.Contains("?") || ContainerNo2.Contains("?"))
            {
                //Console.WriteLine("集装箱号有错!");
                ControlTextSetGet(lbState, "集装箱号有错!");
                Console.Beep(4000, 300);
                if ( (InitialValue.Network.ErrorNoSend == 0) && (InitialValue.Settings.ImagePath1 == "") )
                {
                    return;
                } 
            }

            //ControlTextSetGet(lblNetworking, "发送" + ContainerNo1);

            //Console.WriteLine("rel_SendData");
            string tmpstring = "           ";
            if (ContainerNo1.Length > 11)
                ContainerNo1 = ContainerNo1.Substring(0, 11);
            else if (ContainerNo1.Length < 11)
                ContainerNo1 = ContainerNo1 + tmpstring.Substring(0,11-ContainerNo1.Length);
            if (ContainerNo2.Length > 11)
                ContainerNo2 = ContainerNo2.Substring(0, 11);
            else if (ContainerNo2.Length < 11)
                ContainerNo2 = ContainerNo2 + tmpstring.Substring(0, 11 - ContainerNo2.Length);

            if (ContainerType1.Length > 4)
                ContainerType1 = ContainerType1.Substring(0, 4);
            else if (ContainerType1.Length < 4)
                ContainerType1 = ContainerType1 + tmpstring.Substring(0, 4 - ContainerType1.Length);
            if (ContainerType2.Length > 4)
                ContainerType2 = ContainerType2.Substring(0, 4);
            else if (ContainerType2.Length < 4)
                ContainerType2 = ContainerType2 + tmpstring.Substring(0, 4 - ContainerType2.Length);

            DataPack.P2Received = false;
            rel_SendData(ContainerNo1, ContainerType1, ContainerNo2, ContainerType2, ContainerType);

            //Console.WriteLine("Network");
            if (InitialValue.Network.P1SendDuration > 0)  //重新发送
            {
                //Console.WriteLine("tmRepeat");
                tmRepeatSend.Interval = InitialValue.Network.P1SendDuration;
                tmRepeatSend.Enabled = true;
            }
            //Console.WriteLine("end");
        }

      //如果是null，则返回空字符串
        private string NullPs(string str)
        {
            if (str==null)
            {
                return "";
            }
            else
            {
                return str;
            }
        }


        //新老协议发送数据
        private void rel_SendData(string ContainerNo1, string ContainerType1, string ContainerNo2, string ContainerType2, int ContainerType)
        {
            DataPack.m_P1.RecogFlag = "0";

            try
            {
                DataPack.m_P1.F_ImagePath = NullPs(myController.dataList.Where(x => x.CameraType == "前相机").FirstOrDefault().ImagePath);
                DataPack.m_P1.FR_ImagePath = NullPs(myController.dataList.Where(x => x.CameraType == "前相机辅助").FirstOrDefault().ImagePath);
                DataPack.m_P1.B_ImagePath = NullPs(myController.dataList.Where(x => x.CameraType == "后相机").FirstOrDefault().ImagePath);
                DataPack.m_P1.BL_ImagePath = NullPs(myController.dataList.Where(x => x.CameraType == "后相机辅助").FirstOrDefault().ImagePath);
                DataPack.m_P1.FL_ImagePath = DataPack.m_P1.FR_ImagePath;               
                DataPack.m_P1.BR_ImagePath = DataPack.m_P1.BL_ImagePath;
            }
            catch { }
            if (ContainerType == 2)   //长箱
            {
                DataPack.m_P1.Type = "0";
            }
            else if (ContainerType == 1)  //双箱
            {
                DataPack.m_P1.Type = "2";
                try
                {
                    DataPack.m_P1.FL_ImagePath = NullPs(myController.dataList.Where(x => x.CameraType == "中缝前").FirstOrDefault().ImagePath);
                    DataPack.m_P1.BR_ImagePath = NullPs(myController.dataList.Where(x => x.CameraType == "中缝辅助后").FirstOrDefault().ImagePath);
                }
                catch { }
            }
            else if (ContainerType == 0)  //短箱
            {
                DataPack.m_P1.Type = "1";
            }
            else
            {
                DataPack.m_P1.Type = ContainerType.ToString();
            }
            DataPack.m_P1.Channel = "";
            if ((InitialValue.Network.SendProtocalType == 2) || (InitialValue.Network.SendProtocalType == 3))    //2：软筑协议，3：软筑协议+，不能有空格           
            {
                DataPack.m_P1.No1 = ContainerNo1.Trim();
                DataPack.m_P1.No2 = ContainerNo2.Trim();
                DataPack.m_P1.TypeNo1 = ContainerType1.Trim();
                DataPack.m_P1.TypeNo2 = ContainerType2.Trim();
            }
            else
            {
                DataPack.m_P1.No1 = ContainerNo1;
                DataPack.m_P1.No2 = ContainerNo2;
                DataPack.m_P1.TypeNo1 = ContainerType1;
                DataPack.m_P1.TypeNo2 = ContainerType2;
            }
            DataPack.m_P1.RelState = "";
            DataPack.m_P1.Operator = "";


            if (Math.Abs(InitialValue.Settings.SaveFlag) == 2)  //用于H986传递图像识别时间，清华同方协议,包含了图像文件路径
            {
                //       pack1.channel = Chr$(Right(Year(CapTime), 2)) + Chr$(Month(CapTime))
                //       pack1.operator = Chr$(Day(CapTime)) + Chr$(Hour(CapTime) + 48) + Chr$(Minute(CapTime) + 48) + Chr$(Second(CapTime) + 48)
                string CapTimeString = myController.dataList.FirstOrDefault().GrabTime;
                if (string.IsNullOrEmpty(CapTimeString))
                {
                    CapTimeString = "2099/12/30 00:00:00";
                }
                DateTime CapTime = Convert.ToDateTime(CapTimeString);
                char year = Convert.ToChar(Convert.ToInt32(CapTime.Year.ToString().Substring(2, 2)));
                char month = Convert.ToChar(Convert.ToInt32(CapTime.Month.ToString()));
                char day = Convert.ToChar(Convert.ToInt32(CapTime.Day.ToString()));
                char hour = Convert.ToChar(Convert.ToInt32(CapTime.Hour.ToString()) + 48);
                char minute = Convert.ToChar(Convert.ToInt32(CapTime.Minute.ToString()) + 48);
                char second = Convert.ToChar(Convert.ToInt32(CapTime.Second.ToString()) + 48);
                DataPack.m_P1.Channel = new StringBuilder().Append(year).Append(month).ToString();
                DataPack.m_P1.Operator = new StringBuilder().Append(day).Append(hour).Append(minute).Append(second).ToString();
            }

            if ( (InitialValue.Network.SendProtocalType == 2) || (InitialValue.Network.SendProtocalType == 3) )    //2：软筑协议，3：软筑协议+
            {
                XmlDocument doc = new XmlDocument();
                doc = DataPack.MakeXML(doc, DataPack.m_P1);
                string tmpString = doc.InnerXml;//InnerXml:xml格式字符串；InnerText：xml文本数据
                //TcpServer.TcpConnectServer(); //本端作为客户端，连接服务端
                TcpServer.SendData_New(tmpString, 0);
            }
            else      //0：为初始协议，1：为初始协议+具体箱型号,（英迪、同方）
            {
                string tmpString = DataPack.P1ToStrb(DataPack.m_P1, InitialValue.Network.SendProtocalType);
                byte[] tmpArray = Encoding.UTF8.GetBytes(tmpString);
                //tcpServerRel.SendData(tmpArray);
                TcpServer.SendData_Old(tmpArray, tmpString);   //本端作为服务端，发送给客户端
            }

            ContainerHelper.getCarCountFromLogFile(false);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("功能待完成...");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void FCameraState_Click(object sender, EventArgs e)
        {

        }

        private void FTImgPB2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void cmbContainerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentType = cmbContainerType.SelectedIndex;
        }

        private void lbDateTime_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void VideoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("确定要关闭程序吗?", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
            {
                e.Cancel = true;  //取消窗体关闭操作
            }
            else
            {
                ContainerHelper.saveLogFile("程序结束！");
                Console.WriteLine("程序结束！");
                try
                {
                    if (TcpServer.sckServer != null)  //关闭通信连接
                    {
                        TcpServer.sckServer.Close();
                        Thread.Sleep(20);
                    }
                    //                    ContainerHelper.DisposeModels();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                    System.Environment.Exit(0);
                }
                catch 
                { }
            }
        }

        //在线程中显示串口label
        private void lbStateShow(Label lbState, string msg, bool concatFlag)
        {
            if (lbState.InvokeRequired)
            {
                lbState.Invoke(new Action(() =>
                {
                    if (concatFlag)
                        lbState.Text = lbState.Text + msg;
                    else
                        lbState.Text = msg;
                }));
            }

            if (lblCarCount.InvokeRequired)
            {
                lblCarCount.Invoke(new Action(() =>
                {
                    lblCarCount.Text = carCount.ToString();
                }));
            }
        }
        
        //时间显示及刷新定时器
        private void timerTiming(object a)
        {
            if (VideoForm.tCount == 0)
            {
                lbStateShow(lbDateTime, DateTime.Now.ToString() + "    (" + VideoForm.frameNumF.ToString() + "," + VideoForm.frameNumB.ToString() + ")",false);
                //lbDateTime.Text = DateTime.Now.ToString() + "    (" + VideoForm.frameNumF.ToString() + "," + VideoForm.frameNumB.ToString() + ")";    // +"  "+  myController.state.ToString();
                VideoForm.frameNumF = 0;
                VideoForm.frameNumB = 0;

                if ((DateTime.Now.Minute == 0) && (DateTime.Now.Second == 0))
                {
                    string tmpstring = InitialValue.CheckDiskState(false);
                    if (tmpstring != "")
                    {
                        lbStateShow(label1, tmpstring, false);
//                        label1.Text = tmpstring;
                    }
                }
            }

            VideoForm.tCount += 1;
            if (VideoForm.tCount == 4)  //每隔四次（1s）复位
                VideoForm.tCount = 0;


            if (radioButton1.InvokeRequired)
            {
                radioButton1.Invoke(new Action(() =>
                {
                    radioButton1.Checked = !ImageStaticFlag1;
                }));
            }
            if (radioButton2.InvokeRequired)
            {
                radioButton2.Invoke(new Action(() =>
                {
                    radioButton2.Checked = !ImageStaticFlag2;
                }));
            }
            //radioButton1.Checked = !ImageStaticFlag1;
            //radioButton2.Checked = !ImageStaticFlag2;

            if (tcpMessage != tcpMessage0)
            {
                lbStateShow(lblNetworking, tcpMessage, false);
//                lblNetworking.Text = tcpMessage;
                tcpMessage0 = tcpMessage;
            }
        }

        private void timerNetworking(object a)
        {
            if ((DateTime.Now.Second % 2 == 0) && (InitialValue.Network.SendProtocalType < 2)) //每隔两秒处理一次, 本端作为服务端，连接客户端
            {
                if (!TcpServer.SendhandShake(5))
                {
                    VideoForm.tcpMessage = "已断开连接，等待重联！";
                    TcpServer.TcpConnectClientRetry();
                }
            }

            if ((DateTime.Now.Second % 10 == 0) && ((InitialValue.Network.SendProtocalType == 2))) //每隔10秒处理一次,软筑
            {
                //TcpServer.TcpConectServerRetry();

                if (TcpServer.client == null)
                {
                    VideoForm.tcpMessage = "等待连接！";
                }
            }

            //下面处理接收数据
            if (InitialValue.Network.SendProtocalType == 2)
            {
                return; //软筑无接收数据
            }

            if (TcpServer.sckServer == null)
            {
                return;
            }

            try
            {
                byte[] data = new byte[4096];
                int cnt = TcpServer.sckServer.Receive(data);
                if (cnt > 0)
                    tcpServerRel_DataArrival((byte[])data.Take(cnt));
            }
            catch { }
        }

        //老的recogCS中的数据接受处理程序
        private void tcpServerRel_DataArrival(byte[] ByteData)
        {
            //object Data = "";
            //tcpServerRel.GetData(ref Data, vbArray + vbByte, tcpServerRel.BytesReceived);
            //byte[] ByteData = (byte[])Data;
            string StringData = Encoding.UTF8.GetString(ByteData);

            byte[] SendArray = new byte[3] { 0, 0, 0 };   //发送命令字节数组

            VideoForm.ActiveTimeRel = DateTime.Now.Ticks/10000;     //纪录收到数据时间
            //txtRecogString.Text = ActiveTimeRel.ToString();

            while (StringData.Length >= 3)
            {
                int ln = StringData.Length;
                int ln1 = ByteData[1] * 256 + ByteData[2] + 3;

                if (ln1 > 100)
                {
                    DataPack.InString = "";   
                    return;
                }

                if (ln1 > ln)
                {
                    DataPack.InString = StringData;  //暂存上次收到的数据，因为报文还没有收完
                    return;
                }
                else if (ln1 < ln)
                {
                    DataPack.InString = StringData.Substring(ln1);
                    StringData = StringData.Substring(0, ln1);
                }
                else
                {
                    DataPack.InString = "";
                }

                if (ln1 == 3)
                {
                    int ii = ByteData[0];
                    if (ii == 1)
                    {
                        //            DoEvents
                    }
                    else if (ii == 2)
                    {
                    }
                    else if (ii == 4)
                    {
                        SendArray[0] = 4;
                        TcpServer.sckServer.Send(SendArray);
                    }
                    else if (ii == 5)
                    {
                        SendArray[0] = 5;       //握手信号
                        TcpServer.sckServer.Send(SendArray);
                    }
                    else if (ii == 6)
                    {
                        //this.WindowState = FormWindowState.Maximized;       //业务处理完成，极大化窗口
                    }
                }
                else if (ln1 == 13)  //重量
                {
                    //lblWeight.Text = Convert.ToInt32(StringData.Substring(3, 10)).ToString();
                    //SendArray[0] = 3;          //重量
                    //tcpServerRel.SendData(SendArray);
                }
                else if (ln1 == 36)
                {
                }
                else if ((ln1 == 91) || (ln1 == 90))  //91:p2新报文;90:p2新老报文
                {
                    DataPack.P2Received = true;
                    DataPack.m_P2 = DataPack.StrbToP2(StringData);

                    SendArray[0] = 2;       //握手信号
                    TcpServer.sckServer.Send(SendArray);

                    if (DataPack.m_P2.RelState == "0")
                    {
                        //tmGateDown.Enabled = true;
                        //GateDowning = true;
                    }
                }

                StringData = DataPack.InString;
            }
        }


        private void tmRepeatSend_Tick(object sender, EventArgs e)
        {
            if (DataPack.P2Received == false)
            {
                //if (tcpServerRel.CtlState == sckConnected)
                {
                    //rel_SendData(true);
                }
                //else
                {
                    //tmRepeat.Enabled = false;
                }
            }
        }

        private void textNo1_TextChanged(object sender, EventArgs e)
        {
            if (textNo1.Text.Length >= 11)
            {
                CurrentNo1 = textNo1.Text;
                Console.WriteLine("CurrentNo1:" + CurrentNo1);
            }
        }

        private void textNo2_TextChanged(object sender, EventArgs e)
        {
            if (textNo2.Text.Length >= 11)
            {
                CurrentNo2 = textNo2.Text;
                Console.WriteLine("CurrentNo2:" + CurrentNo2);
            }
        }

        private void textType1_TextChanged(object sender, EventArgs e)
        {
            if (textType1.Text.Length >= 4)
            {
                CurrentType1 = textType1.Text;
                Console.WriteLine("CurrentType1:" + CurrentType1);
            }

        }

        private void textType2_TextChanged(object sender, EventArgs e)
        {
            if (textType2.Text.Length >= 4)
            {
                CurrentType2 = textType2.Text;
            }
        }

        private void tmMediaInit_Tick(object sender, EventArgs e)
        {
            tmMediaInit.Enabled = false;
            lbState.Text = "正在初始化识别模型...";
            Application.DoEvents();
            InitializeMediaControl();
        }

        private void FCameraState_DoubleClick(object sender, EventArgs e)
        {
            FrontTriggerFlag = true;
        }

        private void BCameraState_DoubleClick(object sender, EventArgs e)
        {
            BackTriggerFlag = true;
        }

        private void BackVideoPB_Paint(object sender, PaintEventArgs e)
        {
            if (VideoForm.ShowBoxRectFlag)
            {
                Pen pen = new Pen(Color.Red, 3);
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;     //绘制线的格式

                FrontP1.X = Convert.ToInt32(InitialValue.Video[0].x1 * BackVideoPB.Size.Width);
                FrontP1.Y = Convert.ToInt32(InitialValue.Video[0].y1 * BackVideoPB.Size.Height);
                FrontP2.X = Convert.ToInt32(InitialValue.Video[0].x2 * BackVideoPB.Size.Width);
                FrontP2.Y = Convert.ToInt32(InitialValue.Video[0].y2 * BackVideoPB.Size.Height);

                BackP1.X = Convert.ToInt32(InitialValue.Video[1].x1 * BackVideoPB.Size.Width);
                BackP1.Y = Convert.ToInt32(InitialValue.Video[1].y1 * BackVideoPB.Size.Height);
                BackP2.X = Convert.ToInt32(InitialValue.Video[1].x2 * BackVideoPB.Size.Width);
                BackP2.Y = Convert.ToInt32(InitialValue.Video[1].y2 * BackVideoPB.Size.Height); 
                
                int dx = Convert.ToInt32(BackVideoPB.Size.Width * 0.06);
                int dy = Convert.ToInt32(BackVideoPB.Size.Height * 0.06);
                {
                    e.Graphics.DrawRectangle(pen, VideoForm.BackP1.X + dx, VideoForm.BackP1.Y + dy, VideoForm.BackP2.X - VideoForm.BackP1.X - dx * 2, VideoForm.BackP2.Y - VideoForm.BackP1.Y - dy * 2);
                    e.Graphics.DrawRectangle(pen, VideoForm.BackP1.X - dx, VideoForm.BackP1.Y - dy, VideoForm.BackP2.X - VideoForm.BackP1.X + dx * 2, VideoForm.BackP2.Y - VideoForm.BackP1.Y + dy * 2);
                }
                pen.Dispose();
            }
        }

        private void label9_DoubleClick(object sender, EventArgs e)
        {
            string filePath = Path.Combine(".\\temp", DateTime.Now.ToString("HHmmss")+"F.jpg");

            //保存图像到该文件夹
            try
            {
                Cv2.ImWrite(filePath, OpenCvSharp.Extensions.BitmapConverter.ToMat((System.Drawing.Bitmap)FrontVideoPB.Image));
            }
            catch { }
        }

        private void label10_DoubleClick(object sender, EventArgs e)
        {
            string filePath = Path.Combine(".\\temp", DateTime.Now.ToString("HHmmss") + "B.jpg");

            //保存图像到该文件夹
            try
            {
                Cv2.ImWrite(filePath, OpenCvSharp.Extensions.BitmapConverter.ToMat((System.Drawing.Bitmap)BackVideoPB.Image));
            }
            catch { }
        }

        private void BImgPB2_Click(object sender, EventArgs e)
        {

        }

        private void FrontVideoPB_Paint(object sender, PaintEventArgs e)
        {
            if (VideoForm.ShowBoxRectFlag)
            {
                Pen pen = new Pen(Color.Red, 3);
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;     //绘制线的格式

                FrontP1.X = Convert.ToInt32(InitialValue.Video[0].x1 * FrontVideoPB.Size.Width);
                FrontP1.Y = Convert.ToInt32(InitialValue.Video[0].y1 * FrontVideoPB.Size.Height);
                FrontP2.X = Convert.ToInt32(InitialValue.Video[0].x2 * FrontVideoPB.Size.Width);
                FrontP2.Y = Convert.ToInt32(InitialValue.Video[0].y2 * FrontVideoPB.Size.Height);

                BackP1.X = Convert.ToInt32(InitialValue.Video[1].x1 * FrontVideoPB.Size.Width);
                BackP1.Y = Convert.ToInt32(InitialValue.Video[1].y1 * FrontVideoPB.Size.Height);
                BackP2.X = Convert.ToInt32(InitialValue.Video[1].x2 * FrontVideoPB.Size.Width);
                BackP2.Y = Convert.ToInt32(InitialValue.Video[1].y2 * FrontVideoPB.Size.Height); 
                
                int dx = Convert.ToInt32(FrontVideoPB.Size.Width * 0.06);
                int dy = Convert.ToInt32(FrontVideoPB.Size.Height * 0.06);
                {
                    e.Graphics.DrawRectangle(pen, VideoForm.FrontP1.X + dx, VideoForm.FrontP1.Y + dy, VideoForm.FrontP2.X - VideoForm.FrontP1.X - dx * 2, VideoForm.FrontP2.Y - VideoForm.FrontP1.Y - dy * 2);
                    e.Graphics.DrawRectangle(pen, VideoForm.FrontP1.X - dx, VideoForm.FrontP1.Y - dy, VideoForm.FrontP2.X - VideoForm.FrontP1.X + dx * 2, VideoForm.FrontP2.Y - VideoForm.FrontP1.Y + dy * 2);
                }

                pen.Dispose();
            }
        }

        private void lblGateName_DoubleClick(object sender, EventArgs e)
        {
            ShowBoxRectFlag = !ShowBoxRectFlag;
        }
    }
}
