﻿using HKTest.Utils;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using Sdcb.PaddleDetection;
using Sdcb.PaddleInference;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HKTest.Models
{
    class LeftDetectorModel
    {
        //用于到位判定的状态位
        private bool arrivedState;
        private bool lastArrivedState;
        //控制隔帧入队的flag
        private bool pushFlag;
        //用于存储各种标志位
        public ProcessInfo myController;
        //读取到的原图放入rawImgQueue队列，供目标检测使用
        private ConcurrentQueue<Mat> rawImgQueue;
        //存放第一张抓拍图像的pictureBox
        private PictureBox m_grab_pb1;
        //存放第二张抓拍图像的pictureBox
        private PictureBox m_grab_pb2;
        //存放视频图像的pictureBox
        private PictureBox m_video_pb;
        //图像读取线程
        private Thread imgReadThread;
        //目标检测线程
        private Thread imgRecognizeThread;

        public LeftDetectorModel(ProcessInfo controller, PictureBox video_pb, PictureBox grab_pb1,PictureBox grab_pb2)
        {
            //初始化arrivedState与lastArrivedState为false
            arrivedState = false;
            lastArrivedState = false;
            //控制隔帧入队的flag;
            pushFlag = true;
            myController = controller;
            //原图队列
            rawImgQueue = new ConcurrentQueue<Mat>();
            //视频流读取线程
            imgReadThread = new Thread(ImgRead);
            //设置视频流pictureBox
            m_video_pb = video_pb;
            //目标检测线程
            imgRecognizeThread = new Thread(ImgRecognize);
            //设置抓图pictureBox
            m_grab_pb1 = grab_pb1;
            m_grab_pb2 = grab_pb2;
        }


        private void ImgRead()
        {
            Mat frame = new Mat();
            bool ret;
            VideoCapture myCapture = new VideoCapture(InitialValue.Video[2].VideoURL);
            if (!myCapture.IsOpened())
            {
                Console.WriteLine("摄像头打开失败");
                return;
            }
            if (InitialValue.ReadMode == "0")    //隔帧读取视频流
            {
                while (true)
                {
                    ret = myCapture.Grab();
                    if (!ret)
                    {
                        Console.WriteLine("流读取失败");
                        break;
                    }
                    else
                    {
                        //隔帧放入检测队列，并保持其中有三张图像
                        if (pushFlag)
                        {
                            frame = myCapture.RetrieveMat();
                            //不使用Clone，队列中每一张图像都是相同的
                            rawImgQueue.Enqueue(frame.Clone());
                            pushFlag = false;
                        }
                        else
                        {
                            pushFlag = true;
                        }
                        if (rawImgQueue.Count > 3)
                        {
                            Mat tmp;
                            rawImgQueue.TryDequeue(out tmp);
                            tmp.Dispose();
                        }
                        //将图像发送到前台显示
                        if (m_video_pb.InvokeRequired)
                        {
                            m_video_pb.Invoke(new Action(() =>
                            {
                                m_video_pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                                System.GC.Collect();
                            }));
                        }
                    }
                }
            }
            else    //正常读取视频流
            {
                while (true)
                {
                    ret = myCapture.Grab();
                    if (!ret)
                    {
                        Console.WriteLine("流读取失败");
                        break;
                    }
                    else
                    {
                        frame = myCapture.RetrieveMat();

                        //不使用Clone，队列中每一张图像都是相同的
                        rawImgQueue.Enqueue(frame.Clone());
                        pushFlag = true;

                        if (rawImgQueue.Count > 3)
                        {
                            Mat tmp;
                            rawImgQueue.TryDequeue(out tmp);
                            tmp.Dispose();
                        }
                        //将图像发送到前台显示
                        if (m_video_pb.InvokeRequired)
                        {
                            m_video_pb.Invoke(new Action(() =>
                            {
                                m_grab_pb1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                                System.GC.Collect();
                            }));
                        }
                    }
                }
            }
        }

        private void ImgRecognize()
        {
            Mat frame = new Mat();
            //避免显示出的图像有三根线
            Mat tempFrame1 = new Mat();
            string modelDir = "./inference_model/LRSide/ppyolo_r18vd_coco";
            PaddleDetector detector;
            DetectionResult[] results;

            while (true)
            {
                try
                {
                    detector = new PaddleDetector(modelDir, Path.Combine(modelDir, "infer_cfg.yml"));
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("PaddleDetector检测器初始化失败", e);
                }
            }

            while (true)
            {
                //从原图队列中取出图像进行识别
                if (rawImgQueue.Count > 0)
                {
                    rawImgQueue.TryDequeue(out frame);
                    frame.CopyTo(tempFrame1);

                    if (!frame.Empty())
                    {
                        if (myController.state> ArrivedState.Wait)
                        {
                            try
                            {
                                //paddleDetection目标检测
                                results = detector.Run(DrawLine(tempFrame1));
                                lastArrivedState = arrivedState;
                                IsInPlace(results);
                            }
                            catch
                            {
                                continue;
                            }
                            //若满足到位判定条件，则将到位图像送入前台显示
                            if (!lastArrivedState && arrivedState)
                            {
                                myController.state = ArrivedState.SideArrived;
                                //新建箱号信息结构体，来保存此次识别的信息
                                ContainerData myData = new ContainerData();
                                //此次抓拍时间
                                myData.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                //保存图像文件名
                                string ImgName = $"{DateTime.Now.ToString("HHmmss")}L.jpg";
                                //图像路径
                                myData.ImagePath = Path.Combine("ImgSave", DateTime.Now.ToString("yyMMdd"), ImgName);
                                //相机机位
                                myData.CameraType = "左相机";
                                //创建tempFrame，需保证frame的尺寸是640*480
                                Mat tempFrame = new Mat();
                                Cv2.CopyMakeBorder(frame, tempFrame, 0, 20, 0, 0, borderType: BorderTypes.Constant, new Scalar(0, 0, 0));
                                //左侧校准
                                LeftCalib(ref frame);
                                //文字识别
                                ContainerHelper.NoRecognize(frame, ref myData, 1920f / frame.Width);
                                //将识别结果打印在tempFrame上
                                string temStr = $"{myData.ContainerNO} {myData.ContainerType}";
                                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(0, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 0.6, new Scalar(255, 255, 255), 2);
                                //识别结果校验
                                //                                if (ContainerHelper.DataCheck(myData.ContainerNO.Substring(0, 10)).ToString() == myData.ContainerNO.Substring(10))
                                if (ContainerHelper.DataCheckOK(myData.ContainerNO))
                                {
                                    myData.CheckResult = 1;
                                }
                                else
                                {
                                    myData.CheckResult = 0;
                                }
                                //保存图像
                                ContainerHelper.ImgSave(tempFrame, myController.tempPath, ImgName);
                                ////若还未抓拍到第一张图像，则存储在图像1
                                //if (myController.leftImg1 is null)
                                //{
                                //    //保存图像
                                //    myController.leftImg1 = frame.Clone();
                                //    ContainerHelper.ImgSave(frame, "FL");
                                //    //将图像发送到前台显示
                                if (m_grab_pb1.InvokeRequired)
                                {
                                    m_grab_pb1.Invoke(new Action(() =>
                                    {
                                        m_grab_pb1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(tempFrame);
                                        System.GC.Collect();
                                    }));
                                }
                                //箱号识别结果放入List
                                myController.dataList.Add(myData);
                                //    //休眠0.5s后继续等待下一次抓拍
                                //    myController.leftOcrFlag = false;
                                //    var task = new Task(async () =>
                                //    {
                                //        await Task.Delay(500);
                                //        myController.leftOcrFlag = true;
                                //    });
                                //    task.Start();
                                //}
                                //若已抓拍到了第一张图像，则存储在图像2
                                //else
                                //{
                                //    //将图像发送到前台显示
                                //    if (m_grab_pb2.InvokeRequired)
                                //    {
                                //        m_grab_pb2.Invoke(new Action(() =>
                                //        {
                                //            m_grab_pb2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                                //            System.GC.Collect();
                                //        }));
                                //    }
                                //}
                            }
                        }
                    }
                }
            }
        }


        // 添加黑线函数
        private Mat DrawLine(Mat frame)
        {
            //给图片加上三条从左至右的黑线
            int width = frame.Width;
            int height = frame.Height;

            OpenCvSharp.Point startPoint1 = new OpenCvSharp.Point(0, height / 4);
            OpenCvSharp.Point endPoint1 = new OpenCvSharp.Point(width, height / 4);
            Cv2.Line(frame, startPoint1, endPoint1, Scalar.Black, 5, LineTypes.AntiAlias);

            OpenCvSharp.Point startPoint2 = new OpenCvSharp.Point(0, height / 2);
            OpenCvSharp.Point endPoint2 = new OpenCvSharp.Point(width, height / 2);
            Cv2.Line(frame, startPoint2, endPoint2, Scalar.Black, 5, LineTypes.AntiAlias);

            OpenCvSharp.Point startPoint3 = new OpenCvSharp.Point(0, height * 3 / 4);
            OpenCvSharp.Point endPoint3 = new OpenCvSharp.Point(width, height * 3 / 4);
            Cv2.Line(frame, startPoint3, endPoint3, Scalar.Black, 5, LineTypes.AntiAlias);

            return frame;
        }

        //到位判定函数
        private void IsInPlace(DetectionResult[] results)
        {
            //判定条件：置信度大于0.5，目标结构大于等于2个
            if (results.Where(x => x.Confidence >= 0.5 && x.LabelName == "Cross" && x.Rect.X < 600 && x.Rect.X > 500).ToArray().Length >= 2)
            {
                arrivedState = true;
            }
            else
            {
                arrivedState = false;
            }
        }

        /// <summary>
        /// 左侧相机校准
        /// </summary>
        /// <param name="frame"></param>
        private void LeftCalib(ref Mat frame)
        {
            Point2f[] srcPt = new Point2f[4];
            Point2f[] dstPt = new Point2f[4];
            srcPt[0] = new Point2f(23, 33);
            srcPt[1] = new Point2f(631, 56);
            srcPt[2] = new Point2f(567, 480);
            srcPt[3] = new Point2f(0, 480);
            dstPt[0] = new Point2f(6, 33);
            dstPt[1] = new Point2f(616, 56);
            dstPt[2] = new Point2f(604, 480);
            dstPt[3] = new Point2f(48, 480);

            Mat warpmatrix = Cv2.GetPerspectiveTransform(srcPt, dstPt);
            Cv2.WarpPerspective(frame, frame, warpmatrix, frame.Size());
        }

        //启动线程
        public void ThreadStart()
        {
            imgReadThread.Start();
            imgRecognizeThread.Start();
        }
    }
}
