﻿using HKTest.Utils;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using HKTest.ONNX;
//using PaddleOCRSharp;
using Sdcb.PaddleInference;
using Sdcb.PaddleOCR;

namespace HKTest.Models
{
    class BackDetectorModel
    {
        //运行状态标志位
        private bool running;
        //用于后侧到位判定的状态位
        private bool arrivedState;
        //用于中缝到位判定的标志位
        private bool arrivedStateM;
        //用于存储各种标志位
        public ProcessInfo myController;
        //读取到的原图放入rawImgQueue队列，供目标检测使用
        private ConcurrentQueue<Mat> rawImgQueue;
        //存放抓拍图像的pictureBox
        private PictureBox[] m_grab_pb;
        //存放视频图像的pictureBox
        private PictureBox m_video_pb;
        //图像读取线程
        private Thread imgReadThread;
        //目标检测线程
        private Thread LoopRecognizeThread;
        //显示状态的label控件
        private Label m_lbState;
        //显示相机状态的label
        private Label cameraState;
        //picImage清除图像
        private Action picImgeClear;
        //文字识别Action
        private Action m_myNoGather;
        //检测模型
        //private  PaddleDetector detector = null;
        //private  PaddleOcrDetector OcrDetector = null;
        private MyONNX detector = new MyONNX();
        private MyONNX OcrDetector = new MyONNX();
        private PaddleOcrRecognizer OcrRecognizer = null;

        /// <summary>
        /// 后侧检测构造函数
        /// </summary>
        /// <param name="controller">存储各种标志位</param>
        /// <param name="video_pc">用于视频预览的pictureBox</param>
        /// <param name="grab_pc">用于显示抓拍图像的pictureBox</param>
        public BackDetectorModel(ref ProcessInfo controller,PictureBox video_pb,PictureBox[] grab_pb, Action myPicImageClear, Label lbState,Label BCameraState, Action myNoGather)
        {
            running = true;
            arrivedState = false;
            arrivedStateM = false;
            myController = controller;
            //原图队列
            rawImgQueue = new ConcurrentQueue<Mat>();
            //设置视频流pictureBox
            m_video_pb = video_pb;
            //设置抓图pictureBox，是一个数组
            m_grab_pb = grab_pb;
            //picImage清除函数
            picImgeClear = myPicImageClear;
            m_lbState = lbState;
            m_myNoGather = myNoGather;
            cameraState = BCameraState;

            //视频流读取线程
            imgReadThread = new Thread(ImgRead);
            imgReadThread.IsBackground = true;
            imgReadThread.Start();

            //初始化识别模型
            string tmpString = ContainerHelper.InitialModelsONNX(ref detector, ref OcrDetector, ref OcrRecognizer);
            if (tmpString != "")
                lbState.Text = tmpString;

            //目标检测线程
            LoopRecognizeThread = new Thread(LoopRecognize);
            LoopRecognizeThread.IsBackground = true;
            LoopRecognizeThread.Start();
        }


        private void ImgRead()
        {
            while (running)
            {
                Mat frame = new Mat();
                bool ret;
                try
                {
                    using (VideoCapture myCapture = new VideoCapture(InitialValue.Video[0].VideoURL))
                    {
                        lbStateShow(cameraState, "后相机（已连接）", false);

                        while (true)
                        {
                            try
                            {
                                if (InitialValue.Settings.FrameRateFlag == 1)
                                    ret = myCapture.Grab();
                                frame = myCapture.RetrieveMat();
                                //if (!ret)
                                if (frame.Empty())
                                {
                                    lbStateShow(cameraState, "后相机（正在建立连接...）", false);

                                    break;
                                }
                                else
                                {
                                    //frame = myCapture.RetrieveMat();
                                    //不使用Clone，队列中每一张图像都是相同的
                                    rawImgQueue.Enqueue(frame.Clone());

                                    if (rawImgQueue.Count > 3)
                                    {
                                        Mat tmp;
                                        if (rawImgQueue.TryDequeue(out tmp))
                                            tmp.Dispose();
                                    }
                                    //将图像发送到前台显示
                                    ImageShow2Pb(frame, m_video_pb);
                                }
                                //Thread.Sleep(1);
                            }
                            catch (Exception e)
                            {

                            }
                            Application.DoEvents();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception occurred: {ex.Message}, reconnecting...");

                    lbStateShow(cameraState, "后相机（正在建立连接...）", false);
                }

                if (running)
                {
                    Thread.Sleep(100); // Wait a bit before trying to reconnect
                }
            }
        }

        //在主界面状态label上显示信息
        private void lbStateShow(Label lbState, string msg, bool concatFlag)
        {
            if (lbState.InvokeRequired)
            {
                lbState.Invoke(new Action(() =>
                {
                    if (concatFlag)
                        lbState.Text = lbState.Text + msg;
                    else
                        lbState.Text = msg;
                }));
            }
        }

        //图像检测线程运行的函数，将检测器初始化一次之后，一直运行识别
        private void LoopRecognize()
        {
            Mat frame1 = new Mat();
            Mat frame11 = new Mat();   //用于暂存不校正的图像
            Mat frame2 = new Mat();
            string ctnNo = "??????????";
            Mat preFrame = new Mat();
            //PaddleDetector detector;
            resultPP[] results;
            int y1 = -1;  //第一帧的中缝垂直位置
            int y2 = -1;  //第二帧的中缝垂直位置

            while (rawImgQueue.Count == 0) { Application.DoEvents(); }
            //先取最新的一帧图像
            //rawImgQueue.TryDequeue(out preFrame);
            while (!rawImgQueue.TryDequeue(out preFrame)) { Application.DoEvents(); }

            Rect subImageRect = new Rect(preFrame.Width/4, preFrame.Height*2/3, preFrame.Width/2, preFrame.Height/3);  //取中下部的一部分图像
            preFrame = preFrame.Clone(subImageRect);

            arrivedStateM = false;

            while (true)
            {
                //从原图队列中取出图像进行识别
                if (rawImgQueue.Count > 0)
                {
                    while (rawImgQueue.Count > 0)  //取最新的一帧图像
                    //rawImgQueue.TryDequeue(out frame1);
                    while (!rawImgQueue.TryDequeue(out frame1)) { Application.DoEvents(); }

                    frame11 = frame1.Clone();    //暂存后用

                    if (!frame1.Empty())
                    {
                        //检测是否静止图像
                        if (ContainerHelper.StaticImageDetection(preFrame, frame1.Clone(subImageRect)))
                        {
                            preFrame = frame1.Clone(subImageRect);
                            VideoForm.ImageStaticFlag2 = true;
                            if (!VideoForm.BackTriggerFlag)   //没有手动触发
                                continue;  //是静止图像，放弃后面的检测
                        }
                        else
                        {
                            preFrame = frame1.Clone(subImageRect);
                            VideoForm.ImageStaticFlag2 = false;
                        }

                        VideoForm.frameNumB += 1;

                        if (InitialValue.Video[0].CalibParas1 != "")   //图像校正
                            ContainerHelper.ImgCalib(ref frame1, InitialValue.Video[0].warpMatrix1);

                        //计算左右切边位置,OcrRecognizer初始识别一次，因为第一次通常都非常慢
                        if (InitialValue.Video[0].leftMargin1 == -1)
                        {
                            ContainerHelper.marginComputing(frame1, "B");

                            Mat roi = frame1.Clone(new Rect(new OpenCvSharp.Point(100, 100), new OpenCvSharp.Size(200, 40)));
                            PaddleOcrRecognizerResult result = OcrRecognizer.Run(roi);
                        }
                        
                        //if (myController.state > ArrivedState.Wait && myController.state < ArrivedState.Recognizing)
                        if ( ((myController.stateM == false) || (myController.stateB == false)) && (myController.stateCarHead == true) || (myController.stateF == true) )   //前后侧独立拍摄
                        {
                            try
                            {
                                results = detector.Run(frame1);
                            }
                            catch
                            {
                                results = new resultPP[0];
                                continue;
                            }

                            if (!myController.stateM)
                            {
                                y1 = IsInPlaceM(frame1, results);

                                //中缝检测
                                if (arrivedStateM)
                                {
                                    myController.stateM = arrivedStateM;   //检测到中缝标志

                                    if (myController.stateF == false) 
                                        picImgeClear();

                                    //Thread.Sleep(100);
                                    while (rawImgQueue.Count == 0) { Application.DoEvents(); }
                                    //rawImgQueue.TryDequeue(out frame2);
                                    while (!rawImgQueue.TryDequeue(out frame2)) { Application.DoEvents(); }
                                    results = detector.Run(frame2);
                                    y2 = IsInPlaceM(frame2, results);  //第二帧的中缝垂直位置

                                    int tm = 500;
                                    string fCount = "F1-";
                                    for (int i = 0; i < 4; i++)
                                    {
                                        if ((y1 - y2 > frame1.Height / 6) || (i == 3))
                                        {
                                            if (y1 - y2 > 20)
                                            {
                                                tm = (int)((i + 1) * 50 * (1 + (frame1.Height - y2 + 0.1) / (y1 - y2)));
                                            }
                                            fCount = "F" + (i + 1).ToString() + "-";
                                            break;
                                        }
                                        else if (y2 < 0)
                                        {
                                            break;   //检测不出文本框
                                        }

                                        //取下一帧再计算y2
                                        while (rawImgQueue.Count == 0) { Application.DoEvents(); }
                                        //rawImgQueue.TryDequeue(out frame2);
                                        while (!rawImgQueue.TryDequeue(out frame2)) { Application.DoEvents(); }
                                        results = detector.Run(frame2);
                                        y2 = IsInPlaceM(frame2, results);  //第i+1帧的第一个中缝垂直位置

                                        //MRecognizeImage("中缝", frame1, m_grab_pb[2], m_grab_pb[4]);
                                        //MRecognizeImage("中缝辅助", frame2, m_grab_pb[3], m_grab_pb[5]);
                                        //中缝图象只显示，不识别
                                        ImageShow2Pb(frame11, m_grab_pb[2]);
                                        ImageShow2Pb(frame11, m_grab_pb[4]);
                                        ImageShow2Pb(frame2, m_grab_pb[3]);
                                        ImageShow2Pb(frame2, m_grab_pb[5]);
                                    }

                                    if (myController.stateF == false) 
                                        lbStateShow(m_lbState, "检测到双箱！", false);
                                    else
                                        lbStateShow(m_lbState, "检测到双箱！", true);
                                    Console.WriteLine("M:" + myController.state1 + "," + tm.ToString() + "," + DateTime.Now.ToString());

                                    Thread.Sleep(tm);
                                }
                            }

                            //后侧检测，若满足到位判定条件，则将到位图像送入前台显示
                            ctnNo = IsInPlaceB(frame1, results);

                            //if  (myController.stateEmptyCar) //空车集卡
                            //{
                                //m_myNoGather();

                                ////重启检测流程
                                //DetectRestart(InitialValue.Settings.FrontTimeout - 4);
                            //}
                            if (arrivedState)
                            {
                                myController.stateB = true;

                                if ((myController.stateF == false) && (myController.stateM == false))
                                    picImgeClear();

                                VideoForm.timeDuration = (DateTime.Now.Ticks - VideoForm.CaptureTime1) / 10000;
                                Console.WriteLine("Time:" + VideoForm.timeDuration.ToString() + "," + VideoForm.carSpeedLine.ToString() + "," + DateTime.Now.ToString());

                                if ((myController.stateF == false) && (myController.stateM == false)) 
                                    lbStateShow(m_lbState, "后侧到位！", false);
                                else
                                    lbStateShow(m_lbState, "后侧到位！", true);
                                Console.WriteLine("B:" + myController.state1 + "," + ctnNo);

                                Thread.Sleep(300);
                                //采集第二幅图象
                                //rawImgQueue.TryDequeue(out frame2);
                                while (!rawImgQueue.TryDequeue(out frame2)) { Application.DoEvents(); }

                                long t1 = DateTime.Now.Ticks/10000;
                                while (true)  //看前侧是否检测到，前后侧独立检测
                                {
                                    long t2 = DateTime.Now.Ticks / 10000 - t1;
                                    if (myController.stateF == true)
                                    {
                                        myController.state = ArrivedState.Recognizing;
                                        break;
                                    }
                                    if (t2>8000) //间隔超过8s
                                    {
                                        if (myController.stateCarHead == true)   //检测到车头
                                            myController.state = ArrivedState.Recognizing;
                                        break;
                                    }
                                    else
                                        Application.DoEvents();
                                }

                                if (myController.state != ArrivedState.Recognizing)
                                {
                                    DetectRestart(2);
                                    continue;   //前侧超时没有拍到，属误拍，放弃
                                }

                                ////检测状态置为Recognizing,原来前后测顺序检测，则直接进行识别
                                //myController.state = ArrivedState.Recognizing;

                                ////透视校正
                                if (InitialValue.Video[0].CalibParas2 != "")
                                {
                                    ContainerHelper.ImgCalib(ref frame2, InitialValue.Video[0].warpMatrix2);
                                }
                                //计算左右切边位置
                                if (InitialValue.Video[0].leftMargin2 == -1)
                                    ContainerHelper.marginComputing(frame2, "BB");

                                BRecognizeImage("后相机", frame1, m_grab_pb[0]);
                                BRecognizeImage("后相机辅助", frame2, m_grab_pb[1]);

                                //中缝图像识别
                                if (m_grab_pb[2].Image != null)
                                {
                                    frame1 = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)m_grab_pb[2].Image);
                                    MRecognizeImage("中缝", frame1, m_grab_pb[4], m_grab_pb[2], 1, y1);   //对于后像机所拍到的图像，前箱的文字是正的，后箱的文字是反的
                                }
                                if (m_grab_pb[3].Image != null)
                                {
                                    frame2 = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)m_grab_pb[3].Image);
                                    MRecognizeImage("中缝辅助", frame2, m_grab_pb[5], m_grab_pb[3], 2, y2);
                                }

                                myController.stateEmptyCar = false;   //有箱号识别出来，说明不是空车或社会车辆
                                myController.stateVehicles = false;

                                //综合识别结果
                                m_myNoGather();

                                //重启检测流程
                                DetectRestart(InitialValue.Settings.FrontTimeout - 4);
                                //刷新状态提示label
                            }
                        }
                    }
                }

                Application.DoEvents();
            }
        }

        private void ImageShow2Pb(Mat frame, PictureBox pb)
        {
            if (pb.InvokeRequired)
            {
                pb.Invoke(new Action(() =>
                {
                    pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                    //System.GC.Collect();
                }));
            }
        }

 
        private string IsInPlaceB(Mat frame, resultPP[] results)
        {
            string ctnNo = "??????????";

            if (VideoForm.BackTriggerFlag)    //界面手动触发
            {
                myController.stateCarHead = true;
                myController.timeCarHead = DateTime.Now.Ticks;
                arrivedState = true;
                return ctnNo;
            }

            arrivedState = false;

            if (results.Length == 0)
            {
                return ctnNo;  //没有目标返回
            }

            var bumps = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Bump" && x.Rect.Y < frame.Height * 0.22).OrderBy(x => x.Rect.Y).ToArray();
            var corners = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Corner" && x.Rect.Y < frame.Height * 0.22).OrderBy(x => x.Rect.Y).ToArray();
            var corners1 = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Corner" && x.Rect.Y < frame.Height * 0.7).OrderBy(x => x.Rect.Y).ToArray();
            var locks = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Lock" && x.Rect.Y < frame.Height * 0.22).OrderBy(x => x.Rect.Y).ToArray();
            var boxs = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box" && x.Rect.Width < x.Rect.Height*2 && x.Rect.Y < frame.Height * 0.23 && x.Rect.Y > 0).ToArray();
            var seams = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Seam").ToArray();
            var textBoxs = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Text_Block").OrderBy(x => x.Rect.Y).ToArray();
            var carheads = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Car_Head").ToArray();

            //需没有Seam和carhead,且corners1数少于3个
            if ((seams.Length > 0) || (carheads.Length > 0) || (corners1.Length>=3) )
            {
                return ctnNo;   //检测到中缝或车头，且corners1数大于2个，说明不是后侧，返回
            }

            //判定条件1：有Box，且垂直位置小于0.2H
            if ((boxs.Length > 0) && (bumps.Length + locks.Length + textBoxs.Length >= 1))
            {
                myController.state1 = "Box";    //检测到箱子
                arrivedState = true;
                return ctnNo;
            }
            //判定条件1：置信度大于0.5，Bump数量达到2个以上,且门角数量1-2个,有一个文本框
            else if ((bumps.Length > 2) && (corners.Length > 0) && (corners.Length < 3) && (textBoxs.Length > 0))
            {
                myController.state1 = "Bump+Corner";    //对应无门侧面
                arrivedState = true;
                return ctnNo;
            }
            //判定条件2：置信度大于0.5，门锁达到1个以上,且门角数量达到1-2个,有一个文本框
            else if ((locks.Length > 1) && (corners.Length > 0) && (corners.Length < 3)) // && (textBoxs.Length > 0))
            {
                if (locks[0].Rect.Y > corners[corners.Length-1].Rect.Y)  //lock不能在corner之上，否则很有可能是车头缝隙
                { 
                    myController.state1 = "Lock";     //对应有门侧面
                    arrivedState = true;
                    return ctnNo;
                }
            }
            else  //看文本框的识别情况
            {
                string HVflag = "H";
                //看有没有水平文本识别出来
                int yy = ContainerHelper.OcrDetection(OcrRecognizer, frame, results, ref ctnNo);
                //if (yy < 0)
                //{//看有没有垂直文本识别出来
                //    ctnNo = "??????????";
                //    yy = ContainerHelper.OcrDetectionV(OcrRecognizer, frame, results, ref ctnNo);
                //    HVflag = "V";
                //}
                if (yy > 0)
                {
                    if ( ((bumps.Length >= 2) || (locks.Length >= 1)) && (yy > frame.Height * 0.05)  &&  (yy < frame.Height * 0.2))  //有其他目标,文本框必须小于高度的20%
                    {
                        myController.state1 = "Text+x" + HVflag;
                        arrivedState = true;
                        return ctnNo;
                    }
                }
            }

            //寻找文本框位置
            if ((arrivedState == false) && (textBoxs.Length >= 2) )  //没有识别出文本框, 但有一个水平，一个垂直文本块，则认为检测到
            {
                resultPP text1 = textBoxs[0];
                resultPP text2 = textBoxs[1];  //一个水平，一个垂直文本块
                if ((text2.Rect.Width < text2.Rect.Height / 2) && (text1.Rect.Width > text1.Rect.Height * 1.8) && (text1.Rect.Y < frame.Height * 0.25))
                {
                    myController.state1 = "Text2";     //对应有两个文本框
                    arrivedState = true;
                    return ctnNo;
                }
            }

            //var emptycars = results.Where(x => x.Confidence > 0.6 && x.LabelName == "Empty_Car").OrderBy(x => x.Rect.Y).ToArray();

            //if ((emptycars.Length > 0) && (textBoxs.Length == 0))  //检测到空车标志
            //{
            //    if ((emptycars.Length == 1) && (emptycars[0].Rect.Height < frame.Height * 0.5))
            //        return ctnNo;

            //    myController.stateEmptyCar = true;
            //    if (!myController.stateCarHead)
            //        myController.timeCarHead = DateTime.Now.Ticks;
            //    lbStateShow(m_lbState, "检测到空车进入！", false);
            //    Console.WriteLine("F:检测到空车进入," + DateTime.Now.ToString("HHmmss"));

            //    if (InitialValue.Settings.ImagePath1 == "")
            //        myController.EmptyCarImage = null;
            //    else if (myController.EmptyCarImage == null)
            //        myController.EmptyCarImage = frame.Clone();
            //}

            return ctnNo;            
        }

        //中缝到位判定函数
        //返回上面中缝的垂直位置，没有检测到，则返回-1
        private int IsInPlaceM(Mat frame, resultPP[] results)
        {
            arrivedStateM = false;

            if (results.Length == 0)
            {
                return -1;  //没有目标返回
            }

            //判定条件1，画面中存在三个以上的Box_Corner，水平文本框
            var bumps = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Bump" && x.Rect.Y < frame.Height * 0.22).OrderBy(x => x.Rect.Y).ToArray();
            var locks = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Lock" && x.Rect.Y < frame.Height * 0.22).OrderBy(x => x.Rect.Y).ToArray();
            var textResult = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Text_Block" && x.Rect.Width > x.Rect.Height * 1.7).OrderBy(x => x.Rect.Y).ToArray();
            var cornerResult = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Corner" && x.Rect.Y < frame.Height * 0.45).OrderBy(x => x.Rect.X).ToArray();
            var cornerResult1 = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Corner" && x.Rect.Y < frame.Height * 0.45).OrderBy(x => x.Rect.Y).ToArray();  //按Y排序
            var seamResult = results.Where(x => x.Confidence > 0.6 && x.LabelName == "Seam" && x.Rect.Y > frame.Height * 0.12 && x.Rect.Y + x.Rect.Height < frame.Height * 0.7).ToArray();
            var carheads = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Car_Head").ToArray();

            //见到车头，说明不是中缝
            if (carheads.Length > 0)
            {
                return -1;
            }
            
            //判定条件1：有Seam,且有两个及以上的textblock或3个及以上boxcorner
            if (seamResult.Length > 0)
            {
                if ( (textResult.Length > 1) || (cornerResult.Length > 2) )
                {
                    myController.state1 = "Seam";    //对应中缝
                    arrivedStateM = true;
                    return (seamResult[0].Rect.Top + seamResult[0].Rect.Bottom) / 2;
                }
            }           

            //条件1：3个Boxcorner,条件2：2个Boxcorner，并且两个是在同一边；看识别情况
            if ( (cornerResult.Length == 3) || ((cornerResult.Length == 2) && (cornerResult[1].Rect.X - cornerResult[0].Rect.X < frame.Width / 8) ) )
            {
                if (textResult.Length >= 2)
                {
                    //if ( (textResult[0].Rect.Y < cornerResult1[0].Rect.Y + cornerResult1[0].Rect.Height) && (cornerResult[cornerResult.Length - 1].Rect.X - (textResult[1].Rect.X + textResult[1].Rect.Width) > textResult[1].Rect.Width * 0.12) )
                    if ( (textResult[0].Rect.Y < cornerResult1[0].Rect.Y + cornerResult1[0].Rect.Height) && (cornerResult1[cornerResult.Length - 1].Rect.Y- cornerResult1[0].Rect.Y < frame.Height/3) ) //&& (cornerResult[cornerResult.Length - 1].Rect.X - (textResult[1].Rect.X + textResult[1].Rect.Width) > textResult[1].Rect.Width * 0.0) )
                    {//文本框与box左右、上下要有一定的间距
                        string CtnNo1 = "??????????";
                        string CtnNo2 = "??????????";
                        if (ContainerHelper.OcrDetection2(OcrRecognizer, frame, textResult, ref CtnNo1, ref CtnNo2) == 1)
                        {  //正反两个箱号
                            myController.state1 = "3B+2T+R";
                            arrivedStateM = true;
                            return (textResult[0].Rect.Top + textResult[1].Rect.Bottom)/2;
                        }
                    }
                }
            }

            //当有4个Box_Corner,并且有一定的间距，不是挨在一起，表明是中缝
            if (cornerResult.Length == 4)
            {
                int y01 = Math.Abs(cornerResult[0].Rect.Y - cornerResult[1].Rect.Y);
                int h01 = (cornerResult[0].Rect.Height + cornerResult[1].Rect.Height);
                int y23 = Math.Abs(cornerResult[2].Rect.Y - cornerResult[3].Rect.Y);
                int h23 = (cornerResult[2].Rect.Height + cornerResult[3].Rect.Height);
                if ((y01 > 0.425 * h01) && (y23 > 0.425 * h23) && (y01 < 1.2 * h01) && (y23 < 1.2 * h23))
                {
                    myController.state1 = "4B+";
                    arrivedStateM = true;
                    return (cornerResult[0].Rect.Top + cornerResult[3].Rect.Bottom) / 2;
                    //return 1;
                }
                else if ( (y01 < 2.5 * h01) && (y23 < 2.5 * h23) && ((bumps.Length > 1) || (locks.Length > 0)))
                {
                    myController.state1 = "4B++";
                    arrivedStateM = true;
                    return (cornerResult[0].Rect.Top + cornerResult[3].Rect.Bottom) / 2;
                    //return 1;
                }
            }

            return -1;
        }

        /// <summary>
        /// 处理识别任务
        /// </summary>
        /// <param name="imgType">图像类型，前相机，前相机辅助，后相机，后相机辅助，区别是后缀是辅助的图像不做保存</param>
        /// <param name="frame">待识别的帧</param>
        /// <param name="grab_pb">要传入识别结果的picturebox控件</param>
        private void BRecognizeImage(string imgType, Mat frame, PictureBox grab_pb)
        {
            //新建箱号信息结构体，来保存此次识别的信息
            ContainerData myData = new ContainerData();

            //此次抓拍时间
            myData.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string tmpPath = ContainerHelper.getFilePath(myData,myController);
            string ImgName = ContainerHelper.getFileName(myData, imgType, myController);

            myData.ImagePath = Path.Combine(tmpPath, ImgName); 
            
            //相机机位
            myData.CameraType = imgType;
            //创建tempFrame
            Mat tempFrame = new Mat();
            //Cv2.CopyMakeBorder(frame, tempFrame, 0, 20, 0, 0, borderType: BorderTypes.Constant, new Scalar(0, 0, 0));
            tempFrame = frame.Clone();

            //文字识别,最大放大到宽1280
            ContainerHelper.NoRecognize(OcrDetector, OcrRecognizer, frame, ref myData, 1280f / frame.Width, frame.Height);

            //识别结果校验
            if (ContainerHelper.DataCheckOK(myData.ContainerNO))
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myData.ContainerNO} {myData.ContainerType}";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(256, 256, 256), 2);
                myData.CheckResult = 1;
            }
            else
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myData.ContainerNO} {myData.ContainerType}   ?";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(256, 256, 256), 2);
                myData.CheckResult = 0;
            }

            //保存图像
            //ContainerHelper.ImgSave(tempFrame, myController.tempPath, ImgName);
            //ContainerHelper.ImgSave(tempFrame, tmpPath, ImgName);
            myData.grab_pb = grab_pb; //后面一次性从这个控件中读图像存盘

            //将图像发送到前台显示
            Rect rect = new Rect(0, 0, tempFrame.Width, tempFrame.Height);   //存储的区域
            if (imgType == "后相机")
            {
                rect.Left = InitialValue.Video[0].leftMargin1;   
                rect.Width = InitialValue.Video[0].widthMargin1;
            }
            else
            {
                rect.Left = InitialValue.Video[0].leftMargin2;
                rect.Width = InitialValue.Video[0].widthMargin2;
            }

            ImageShow2Pb(tempFrame.Clone(rect), grab_pb);

            //箱号识别结果放入List
            myController.dataList.Add(myData);

            tempFrame.Dispose();
        }

        //grab_pb1显示的是前箱，图象是正的，grab_pb2显示的是后箱，图像是反的, pictureIndex:图像序号
        private void MRecognizeImage(string imgType, Mat frame, PictureBox grab_pb1, PictureBox grab_pb2, int pictureIndex, int seamPosition)
        {
            //新建两个箱号信息结构体，来保存此次识别的信息，一正一反
            ContainerData myDataB = new ContainerData();
            ContainerData myDataF = new ContainerData();

            //此次抓拍时间
            myDataB.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            myDataF.GrabTime = myDataB.GrabTime;
            string tmpPath = ContainerHelper.getFilePath(myDataB,myController);
            //保存图像文件名，只保存一张
            string imgName = ContainerHelper.getFileName(myDataB, imgType,myController);

            if(imgType == "中缝")
            {
                myDataB.CameraType = "中缝后";
                myDataF.CameraType = "中缝前";
            }
            else //if (imgType == "中缝辅助")
            {
                myDataB.CameraType = "中缝辅助后";
                myDataF.CameraType = "中缝辅助前";
            }
            //图像路径
            myDataB.ImagePath = Path.Combine(tmpPath, imgName);
            myDataF.ImagePath = Path.Combine(tmpPath, imgName);

            //相机机位
            //创建tempFrame，底下加20行的边，以存放识别结果
            Mat tempFrame = new Mat();
            //Cv2.CopyMakeBorder(frame, tempFrame, 0, 20, 0, 0, borderType: BorderTypes.Constant, new Scalar(0, 0, 0));
            tempFrame = frame.Clone(); 

            //文字识别
            ContainerHelper.NoRecognize(OcrDetector, OcrRecognizer, tempFrame, ref myDataF, 1280f / frame.Width, seamPosition);

            //识别结果校验
            if (ContainerHelper.DataCheckOK(myDataF.ContainerNO))
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myDataF.ContainerNO} {myDataF.ContainerType}";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(256, 256, 256), 2);
                myDataF.CheckResult = 1;
            }
            else
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myDataF.ContainerNO} {myDataF.ContainerType}   ?";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(256, 256, 256), 2);
                myDataF.CheckResult = 0;
            }
            //保存图像
            if (pictureIndex == 1)
                //ContainerHelper.ImgSave(tempFrame, myController.tempPath, imgName);
                //ContainerHelper.ImgSave(tempFrame, tmpPath, imgName);
                myDataF.grab_pb = grab_pb1; //后面一次性从这个控件中读图像存盘
            else
                myDataF.grab_pb = null;

            //将图像发送到前台显示
            Rect rect = new Rect(0, 0, tempFrame.Width, tempFrame.Height);   //存储的区域
            if (imgType == "中缝")
            {
                rect.Left = InitialValue.Video[0].leftMargin1;
                rect.Width = InitialValue.Video[0].widthMargin1;
            }
            else
            {
                rect.Left = InitialValue.Video[0].leftMargin2;
                rect.Width = InitialValue.Video[0].widthMargin2;
            }

            ImageShow2Pb(tempFrame.Clone(rect), grab_pb1);

            //图像垂直翻转后作为前侧相机的中缝图像
            Mat dest = new Mat();
            Cv2.Flip(frame, dest, FlipMode.XY);  //图像旋转180度
            //Cv2.CopyMakeBorder(dest, tempFrame, 0, 20, 0, 0, borderType: BorderTypes.Constant, new Scalar(0, 0, 0));
            tempFrame = dest.Clone();

            //文字识别
            ContainerHelper.NoRecognize(OcrDetector, OcrRecognizer, dest, ref myDataB, 1280f / frame.Width, frame.Height-seamPosition);
            dest.Dispose();

            //识别结果校验
            if (ContainerHelper.DataCheckOK(myDataB.ContainerNO))
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myDataB.ContainerNO} {myDataB.ContainerType}";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20*temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(255, 255, 255), 2);
                myDataB.CheckResult = 1;
            }
            else
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myDataB.ContainerNO} {myDataB.ContainerType}   ?";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(255, 255, 255), 2);
                myDataB.CheckResult = 0;
            }

            if (pictureIndex == 2)   //中缝的第二个图像存储的是上下翻转的图像
                //ContainerHelper.ImgSave(tempFrame, myController.tempPath, imgName);
                //ContainerHelper.ImgSave(tempFrame, tmpPath, imgName);
                myDataB.grab_pb = grab_pb2; //后面一次性从这个控件中读图像存盘
            else
                myDataB.grab_pb = null;

            //将图像发送到前台显示
            ImageShow2Pb(tempFrame, grab_pb2);

            tempFrame.Dispose();

            //最后，将箱号识别结果放入List
            myController.dataList.Add(myDataB);
            myController.dataList.Add(myDataF);
        }

        //重置检测任务
        private void DetectRestart(int waitDuration)
        {
            //等待waitDuration秒再重启
            //等待waitDuration秒再重启
            Mat frame = new Mat();
            for (int i = 0; i < waitDuration; i++)
            {
                Thread.Sleep(1000);
                if (rawImgQueue.Count > 0)
                {
                    rawImgQueue.TryDequeue(out frame);
                    Rect subImageRect = new Rect(frame.Width / 4, 0, frame.Width / 2, frame.Height / 2);  //取中上部的一小部分图像
                    VideoForm.ImageStaticFlag1 = ContainerHelper.StaticImageDetection(frame, frame.Clone(subImageRect));
                    if (VideoForm.ImageStaticFlag1)
                    {
                        break;
                    }
                }
            }
            frame.Dispose();

            //生成新的状态码
            myController.verifyCode = ContainerHelper.codeGet(myController.verifyCode);
            myController.picClearFlag = false;
            myController.dataList = new List<ContainerData>();
            System.GC.Collect();
            
            //重置arrivedState
            arrivedState = false;
            arrivedStateM = false;
            myController.state = ArrivedState.Wait;
            myController.state1 = "";
            myController.stateF = false; 
            myController.stateM = false;
            myController.stateB = false;
            myController.stateCarHead = false;
            myController.stateEmptyCar = false;
            myController.stateVehicles = false;
            myController.timeCarHead = 0;
            myController.timeEmptyCar = 0;
            myController.dataList.Clear();
            if (myController.CarHeadImage != null)
            {
                myController.CarHeadImage.Dispose();
                myController.CarHeadImage = null;
            }
            if (myController.EmptyCarImage != null)
            {
                myController.EmptyCarImage.Dispose();
                myController.EmptyCarImage = null;
            }

            VideoForm.FrontTriggerFlag = false;
            VideoForm.BackTriggerFlag = false;

            lbStateShow(m_lbState, "等待车辆进入...", false);
        }

        //启动线程
        public void ThreadStart()
        {
            imgReadThread.Start();
            LoopRecognizeThread.Start();
        }

    }
}
