﻿using HKTest.Utils;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HKTest.ONNX;
//using PaddleOCRSharp;
using Sdcb.PaddleInference;
using Sdcb.PaddleOCR;
using Point = OpenCvSharp.Point;

namespace HKTest.Models
{
    class FrontDetectorModel
    {
        //用于到位判定的状态位
        private bool arrivedState;
        private bool running;
        //用于存储各种标志位
        public ProcessInfo myController;
        //读取到的原图放入rawImgQueue队列，供目标检测使用
        private ConcurrentQueue<Mat> rawImgQueue;
        //存放抓拍图像的pictureBox
        private PictureBox[] m_grab_pb;
        //存放视频图像的pictureBox
        private PictureBox m_video_pb;
        //显示当前状态的label控件
        private Label m_lbState;
        //相机状态Label
        private Label cameraState;
        //图像读取线程
        private Thread imgReadThread;
        //目标检测线程
        private Thread LoopRecognizeThread;
        //picImage清除图像
        private Action picImgeClear;
        //文字识别Action
        private Action m_myNoGather;
        //检测模型
        //private  PaddleDetector detector = null;
        //private  PaddleOcrDetector OcrDetector = null;
        private MyONNX detector = new MyONNX();
        private MyONNX OcrDetector = new MyONNX();
        private PaddleOcrRecognizer OcrRecognizer = null;

        public FrontDetectorModel(ref ProcessInfo controller, PictureBox video_pb, PictureBox[] grab_pb,Action myPicImageClear,Label lbState,Label FCameraState,Action myNoGather)
        {
            running = true;
            arrivedState = false;
            myController = controller;
            //原图队列
            rawImgQueue = new ConcurrentQueue<Mat>();
            //设置视频流pictureBox
            m_video_pb = video_pb;
            //设置抓图pictureBox，是一个数组
            m_grab_pb = grab_pb;
            //picImage清除函数
            picImgeClear = myPicImageClear;
            //综合函数
            m_myNoGather = myNoGather;
            m_lbState = lbState;
            cameraState = FCameraState;

            //视频流读取线程
            imgReadThread = new Thread(ImgRead);
            imgReadThread.IsBackground = true;
            imgReadThread.Start();

            //初始化识别模型
            string tmpString = ContainerHelper.InitialModelsONNX(ref detector, ref OcrDetector, ref OcrRecognizer);
            if (tmpString != "")
                lbState.Text = tmpString;

            //目标检测线程
            LoopRecognizeThread = new Thread(LoopRecognize);
            LoopRecognizeThread.IsBackground = true;
            LoopRecognizeThread.Start();
        }

        private void ImgRead()
        {
            while (running)
            {
                Mat frame = new Mat();
                bool ret;
                try
                {
                    using (VideoCapture myCapture = new VideoCapture(InitialValue.Video[1].VideoURL))
                    {
                        lbStateShow(cameraState, "前相机（已连接）", false);

                        while (true)
                        {
                            try
                            {
                                if (InitialValue.Settings.FrameRateFlag == 1)
                                    ret = myCapture.Grab();
                                    frame = myCapture.RetrieveMat();
                                //if (!ret)
                                if (frame.Empty())
                                {
                                    lbStateShow(cameraState, "前相机（正在建立连接...）", false);

                                    break;
                                }
                                else
                                {
                                    //frame = myCapture.RetrieveMat();
                                    //不使用Clone，队列中每一张图像都是相同的
                                    rawImgQueue.Enqueue(frame.Clone());

                                    if (rawImgQueue.Count > 3)
                                    {
                                        Mat tmp;
                                        if (rawImgQueue.TryDequeue(out tmp))
                                            tmp.Dispose();
                                    }
                                    //将图像发送到前台显示
                                    ImageShow2Pb(frame, m_video_pb);
                                }
                                //Thread.Sleep(1);
                            }
                            catch (Exception e)
                            {

                            }

                            Application.DoEvents();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception occurred: {ex.Message}, reconnecting...");

                    lbStateShow(cameraState, "前相机（正在建立连接...）", false);
                }

                if (running)
                {
                    Thread.Sleep(100); // Wait a bit before trying to reconnect
                }
            }
        }

        private void ImageShow2Pb(Mat frame, PictureBox pb)
        {
            if (pb.InvokeRequired)
            {
                pb.Invoke(new Action(() =>
                {
                    pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                    //System.GC.Collect();
                }));
            }
        }

        private void LoopRecognize()
        {
            Mat frame1 = new Mat();
            Mat frame2 = new Mat();
            string ctnNo = "??????????";
            Mat preFrame = new Mat();
            //DetectionResult[] results;
            resultPP[] results;
            ContainerData tmpData = new ContainerData();

            int yy1 = 0, yy2 = 0; //检测到的文本框位置


            while (rawImgQueue.Count == 0) {Application.DoEvents(); }
            //先取最新的一帧图像
            //rawImgQueue.TryDequeue(out preFrame);
            while (!rawImgQueue.TryDequeue(out preFrame)) { Application.DoEvents(); }

            Rect subImageRect = new Rect(preFrame.Width / 4, 0, preFrame.Width / 2, preFrame.Height / 2);  //取中上部的一小部分图像
            preFrame = preFrame.Clone(subImageRect);

            while (true)
            {
                //从原图队列中取出图像进行识别
                if (rawImgQueue.Count > 0)
                {
                    while(rawImgQueue.Count>0)  //取最新的一帧图像
                    //rawImgQueue.TryDequeue(out frame1);
                    while (!rawImgQueue.TryDequeue(out frame1)) { Application.DoEvents(); }

                    if (!frame1.Empty())
                    {
                        //车辆进入10+FrontTimeout秒以后自动复位，空车已走
                        long tmp = (DateTime.Now.Ticks - myController.timeCarHead) / 10000000;
                        if ((tmp > 10 + InitialValue.Settings.FrontTimeout) && (myController.stateCarHead == true))
                        {
                            myController.stateCarHead = false;
                            
                            lbStateShow(m_lbState, "等待车辆进入...", false);

                            DetectRestart(0);
                            Console.WriteLine("F:车辆进入10+FrontTimeout秒以后自动复位:" + DateTime.Now.ToString("HHmmss"));
                        }

                        //检测是否静止图像
                        if (ContainerHelper.StaticImageDetection(preFrame, frame1.Clone(subImageRect)))
                        {
                            preFrame = frame1.Clone(subImageRect);
                            VideoForm.ImageStaticFlag1 = true;
                            if (!VideoForm.FrontTriggerFlag)  //没有手动触发
                                continue;  //是静止图像，放弃后面的检测
                        }
                        else
                        {
                            preFrame = frame1.Clone(subImageRect);
                            VideoForm.ImageStaticFlag1 = false;
                        }


                        //如果后摄像机拍到的图像是静止的，说明，这次拍到的是箱尾，放弃触发
                        //if (VideoForm.ImageStaticFlag2)
                        //{
                        //    if (!VideoForm.FrontTriggerFlag)   //没有手动触发
                        //        continue;
                        //}

                        VideoForm.frameNumF += 1;

                        //前侧到位判定
                        //if (myController.state == ArrivedState.Wait)
                        if (myController.stateF == false) 
                        {
                            //图像校正
                            if (InitialValue.Video[1].CalibParas1 != "")
                                ContainerHelper.ImgCalib(ref frame1, InitialValue.Video[1].warpMatrix1);

                            //计算左右切边位置,OcrRecognizer初始识别一次，因为第一次通常都非常慢
                            if (InitialValue.Video[1].leftMargin1 == -1)
                            {
                                ContainerHelper.marginComputing(frame1, "F");
                                Thread.Sleep(1000);
                                Mat roi = frame1.Clone(new Rect(new OpenCvSharp.Point(100,100), new OpenCvSharp.Size(200,40)));
                                PaddleOcrRecognizerResult result = OcrRecognizer.Run(roi);
                            }
                            
                            try
                            {
                                //paddleDetection目标检测
                                //long t1 = DateTime.Now.Ticks;
                                results = detector.Run(frame1);
                                //long t2 = (DateTime.Now.Ticks - t1) / 10000;
                            }
                            catch
                            {
                                results = new resultPP[0];  //空数组
                                continue;
                            }

                            ctnNo = IsFInPlace(frame1, results);

                            if (!arrivedState) //&& myController.stateCarHead)
                            {
                                //前侧到位，抓拍两张图像识别
                                if ((myController.stateVehicles)) //社会车辆
                                {
                                    if ((DateTime.Now.Ticks - myController.timeCarHead) / 10000 > 3000)    //等待3秒
                                    {
                                        m_myNoGather();

                                        //重启检测流程
                                        DetectRestart(InitialValue.Settings.FrontTimeout);
                                    }
                                }
                                else if (myController.stateEmptyCar && myController.stateCarHead)  //空车集卡
                                {
                                    if ((DateTime.Now.Ticks - myController.timeEmptyCar) / 10000 > 7000)    //等待1.5秒
                                    {
                                        m_myNoGather();

                                        //重启检测流程
                                        DetectRestart(InitialValue.Settings.FrontTimeout);
                                    }
                                }
                            }
                            else
                            {
                                var textBoxsH = results.Where(x => (x.Confidence > 0.5) && (x.LabelName == "Text_Block") && (x.Rect.Width > x.Rect.Height * 1.8)).OrderBy(x => x.Rect.Y).ToArray();
                                if (textBoxsH.Length > 0)
                                    yy1 = textBoxsH[0].Rect.Y;
                                else
                                    yy1 = -1;

                                //保存触发时间
                                VideoForm.CaptureTime1 = DateTime.Now.Ticks;

                                //刷新状态提示label

                                if ((myController.stateM == false) && (myController.stateB == false))
                                    lbStateShow(m_lbState,"前侧到位！", false);
                                else
                                    lbStateShow(m_lbState, "前侧到位！", true);
                                Console.WriteLine("F:" + myController.state1 + "," + ctnNo);

                                //前侧到位
                                //获取第二张图像
                                //Thread.Sleep(100);
                                for (int i = 0; i < InitialValue.Settings.FrontFrameInterval; i++)
                                {
                                    while (rawImgQueue.Count == 0) { Application.DoEvents(); }
                                    //rawImgQueue.TryDequeue(out frame2);
                                    while (!rawImgQueue.TryDequeue(out frame2)) { Application.DoEvents(); }
                                }

                                //图像校正
                                if (InitialValue.Video[1].CalibParas2 != "")
                                {
                                    ContainerHelper.ImgCalib(ref frame2, InitialValue.Video[1].warpMatrix2);
                                }
                                //计算左右切边位置
                                if (InitialValue.Video[1].leftMargin2 == -1)
                                    ContainerHelper.marginComputing(frame2, "FF");

                                //保存车速
                                results = detector.Run(frame2);
                                textBoxsH = results.Where(x => (x.Confidence > 0.5) && (x.LabelName == "Text_Block") && (x.Rect.Width > x.Rect.Height * 1.8)).OrderBy(x => x.Rect.Y).ToArray();
                                if (textBoxsH.Length > 0)
                                    yy2 = textBoxsH[0].Rect.Y;
                                else
                                    yy2 = -1;

                                if ( (yy1>0) && (yy2>0) )
                                {
                                    if (yy2 - yy1 > 0)
                                    {
                                        VideoForm.carSpeedLine = yy2 - yy1;
                                    }
                                    else
                                    {
                                        VideoForm.carSpeedLine = -1;
                                        //Console.WriteLine(DateTime.Now.ToString() + ":倒车" + (yy2 - yy1).ToString());
                                        //continue;  //判断是倒车，放弃触发！
                                    }
                                }
                                else
                                {
                                    VideoForm.carSpeedLine = -1;
                                }

                                //先文字识别，如果识别出的箱号与前一次的箱号相同，则放弃，可能是倒车
                                if (InitialValue.Settings.RepeatRecogFlag == 0)
                                {
                                    ContainerHelper.NoRecognize(OcrDetector, OcrRecognizer, frame1, ref tmpData, 1280f / frame1.Width, frame1.Height);
                                    if (ContainerHelper.DataCheckOK(tmpData.ContainerNO) && ((tmpData.ContainerNO == VideoForm.CurrentNo1) || (tmpData.ContainerNO == VideoForm.CurrentNo2)))
                                    {
                                        Console.WriteLine("F:箱号相同放弃:" + DateTime.Now.ToString("HHmmss"));
                                        DetectRestart(InitialValue.Settings.FrontTimeout);
                                        continue;
                                    }
                                }

                                //清除当前picImage中的所有图像
                                if ((myController.stateM == false) && (myController.stateB == false))
                                    picImgeClear(); 
                                
                                System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
                                stopwatch.Reset(); stopwatch.Start();
                                RecognizeImage("前相机", frame1, m_grab_pb[0]);
                                RecognizeImage("前相机辅助", frame2, m_grab_pb[1]);
                                string str1 = stopwatch.ElapsedMilliseconds.ToString();

                                //置标志状态
                                if (myController.state < ArrivedState.FrontArrived)
                                    myController.state = ArrivedState.FrontArrived;
                                myController.stateF = true;
                                
                                //时延InitialValue.Settings.FrontTimeout秒,若后侧拍到，正在识别，则退出
                                long t1 = DateTime.Now.Ticks / 10000;
                                while (true) 
                                {
                                    long t2 = DateTime.Now.Ticks / 10000 - t1;
                                    if (myController.state > ArrivedState.FrontArrived)
                                    {
                                        //已经到了后续步骤，则退出时延
                                        break;
                                    }
                                    else if (t2 > 1000*InitialValue.Settings.FrontTimeout) //间隔超过 InitialValue.Settings.FrontTimeouts
                                    {  //超时重启
                                        if ( (myController.stateCarHead) && (ctnNo != "???????????") )
                                        {
                                            //检测状态置为Recognizing
                                            myController.state = ArrivedState.Recognizing;

                                            //综合识别结果
                                            m_myNoGather();

                                            //重启检测流程
                                            DetectRestart(0);
                                        }
                                        else
                                        {  //无车头检测，属误触发，放弃
                                           //重启检测流程
                                            DetectRestart(0);
                                        }

                                        break;
                                    }
                                    else
                                    {
                                        Application.DoEvents();
                                    }
                                }

                                //用任务的方式会产生重复触发的问题
                                //开启Task任务，若10秒后还无侧面或后侧到位，则做一次识别，进入下一个流程
                                //int oldCode = myController.verifyCode;
                                //Task t2 = new Task(() => { StateMonitor(myController, oldCode); });
                                //t2.Start();
                            }
                        }
                    }
                }

                Application.DoEvents();
            }
        }

        private void lbStateShow(Label lbState, string msg, bool concatFlag)
        {
            if (lbState.InvokeRequired)
            {
                lbState.Invoke(new Action(() =>
                {
                    if (concatFlag)
                        lbState.Text = lbState.Text + msg;
                    else
                        lbState.Text = msg;
                }));
            }
        }

        private string IsFInPlace(Mat frame, resultPP[] results)
        {
            string ctnNo = "??????????";
            int yH = 0, yV = 0, yy = 0; //水平、垂直文本框位置

            if (VideoForm.FrontTriggerFlag)    //界面手动触发
            {
                myController.stateCarHead = true;
                myController.timeCarHead = DateTime.Now.Ticks;
                arrivedState = true;
                return ctnNo;
            }

            arrivedState = false;

            if (results.Length==0)
            {
                return ctnNo;  //没有目标返回
            }

            var bumps = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Bump" && x.Rect.Y > frame.Height * 0.16).OrderBy(x => x.Rect.Y).ToArray();
            var corners = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Corner" && x.Rect.Y > frame.Height * 0.12).OrderBy(x => x.Rect.Y).ToArray();
            var locks = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box_Lock" && x.Rect.Y > frame.Height * 0.16).OrderBy(x => x.Rect.Y).ToArray();
            var boxs = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Box" && x.Rect.Width < x.Rect.Height * 2 && x.Rect.Y > frame.Height * 0.05).ToArray();
            var seams = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Seam").ToArray();
            var carheads = results.Where(x => (x.Confidence > 0.5) && ((x.LabelName == "Car_Head") || (x.LabelName == "Vehicle")) && (x.Rect.Bottom>frame.Height/3) && (x.Rect.X+x.Rect.Width>frame.Width/2) && (x.Rect.X < frame.Width / 2)).ToArray();
            var textBoxs = results.Where(x => x.Confidence > 0.5 && x.LabelName == "Text_Block").OrderBy(x => x.Rect.Y).ToArray();

            if (carheads.Length > 0)
            {
                if (!myController.stateCarHead)
                {
                    myController.stateCarHead = true;
                    myController.timeCarHead = DateTime.Now.Ticks;
                    lbStateShow(m_lbState, "检测到车辆进入！", false);
                    Console.WriteLine("F:检测到车辆进入," + DateTime.Now.ToString("HHmmss"));

                    if (InitialValue.Settings.ImagePath1 == "")
                        myController.CarHeadImage = null;
                    else if (myController.CarHeadImage == null)
                            myController.CarHeadImage = frame.Clone();
                }
                else if ((carheads[0].Rect.Height > frame.Height * 0.5) && (carheads[0].Rect.Bottom < frame.Height * 0.7))
                {
                    if (myController.CarHeadImage != null)
                        myController.CarHeadImage.Dispose();
                    myController.CarHeadImage = frame.Clone();
                }

                if (carheads[0].Rect.Y < frame.Height*0.4)
                    return ctnNo;   //车头在上面，说明前侧还没有到
            }


            //需没有Seam
            //if ( (seams.Length > 0) && (carheads.Length == 0) )
            //{
            //    return ctnNo;   //检测到中缝，并且没有检测到车头，说明不是前侧，返回
            //}

            //判定条件0：有Box,且垂直位置大于0.05H
            bool continueFlag = false;
            if ((boxs.Length > 0) && (bumps.Length + locks.Length + textBoxs.Length >= 1))
            {
                int YY = 0;
                if (boxs[0].Rect.Y > frame.Height / 2)  //box太低则放弃
                {
                    continueFlag = true;
                }
                else if (textBoxs.Length > 0)
                {
                    if ((textBoxs[0].Rect.Width < textBoxs[0].Rect.Height) && (boxs[0].Rect.Y > textBoxs[0].Rect.Y))  //垂直文本框在Box上面则放弃
                    {
                        continueFlag = true;
                    }
                }
                
                if (!continueFlag)
                {
                    if (textBoxs.Length > 0)
                        YY = textBoxs[0].Rect.Y;
                    else if (bumps.Length > 0)
                        YY = bumps[0].Rect.Y;
                    else
                        YY = locks[0].Rect.Y;


                    if ((boxs[0].Rect.Y < frame.Height / 5) && (boxs[0].Rect.Y < YY + frame.Height / 8))   //文本框（bump、lock）不能在box框的上面,同时，box不能低于高度的20%
                    {
                        myController.state1 = "Box";    //检测到箱子
                        arrivedState = true;
                        return ctnNo;
                    }
                }
            }
            //判定条件1：置信度大于0.5，Bump数量达到2个以上,且门角数量1-2个
            else if (bumps.Length > 2 && corners.Length > 0 && corners.Length < 3)
            {
                myController.state1 = "Bump+Corner";    //对应无门侧面
                arrivedState = true;
                return ctnNo;
            }
            //判定条件2：置信度大于0.5，门锁达到1个以上,且门角数量达到1-2个
            else if (locks.Length > 1 && corners.Length > 0 && corners.Length <3 )
            {
                myController.state1 = "Lock+Corner";     //对应有门侧面
                arrivedState = true;
                return ctnNo;
            }
            else  //看文本框的识别情况
            {
                //看有没有水平文本识别出来
                yH = ContainerHelper.OcrDetection(OcrRecognizer, frame, results, ref ctnNo);
                if (yH < 0)
                {//看有没有垂直文本识别出来
                    ctnNo = "??????????";
                    yV = ContainerHelper.OcrDetectionV(OcrRecognizer, frame, results, ref ctnNo);
                }
                yy = Math.Max(yH, yV);

                if (yy > 0)
                {
                    if ( (InitialValue.Settings.RepeatRecogFlag == 0) && ((ContainerHelper.DistanceCalculate(ctnNo, VideoForm.CurrentNo1) < 3) || (ContainerHelper.DistanceCalculate(ctnNo, VideoForm.CurrentNo2) < 3)) )
                    { //与上次识别的箱号相同，则属于重复触发，丢弃
                        arrivedState = false;
                        return ctnNo;
                    }
                    else if (((bumps.Length >= 2) || (locks.Length >= 1)) && (yy > frame.Height * 0.25) && (yy < frame.Height * 0.5))  //至少有其他目标，文本框位置必须在一定的范围内
                    {
                        myController.state1 = "Text+x";
                        arrivedState = true;
                        return ctnNo;
                    }
                    else if ( (myController.stateCarHead) && (yy > frame.Height * 0.33) && ((DateTime.Now.Ticks - myController.timeCarHead) / 10000 < 1000) )   //检测到车头的1秒内识别到箱号，避免车速太快漏检？？？？？
                     {
                        myController.state1 = "Text";
                        arrivedState = true;
                        return ctnNo;
                    }
                }
            }

            if ((arrivedState == false) && (textBoxs.Length >= 2)) //没有识别出文本框, 但有一个水平，一个垂直文本块，则认为检测到
            {
                var textBoxsH = results.Where(x => (x.Confidence > 0.5) && (x.LabelName == "Text_Block") && (x.Rect.Width > x.Rect.Height * 1.8) && (x.Rect.Y > frame.Height * 0.05) && (x.Rect.Y < frame.Height * 0.25)).OrderBy(x => x.Rect.Y).ToArray();
                var textBoxsV = results.Where(x => (x.Confidence > 0.5) && (x.LabelName == "Text_Block") && (x.Rect.Width < x.Rect.Height / 2) && (x.Rect.Y > frame.Height * 0.05)).OrderBy(x => x.Rect.Y).ToArray();
                //一个水平，一个垂直文本块
                if ((textBoxsH.Length > 0) && (textBoxsV.Length > 0))
                {
                    myController.state1 = "Text2";     //对应有两个文本框
                    arrivedState = true;
                    return ctnNo;
                }
            }

            var emptycars = results.Where(x => x.Confidence > 0.6 && x.LabelName == "Empty_Car").OrderBy(x => x.Rect.Y).ToArray();
            var vehicles = results.Where(x => x.Confidence > 0.6 && x.LabelName == "Vehicles").OrderBy(x => x.Rect.Y).ToArray();

            //if ((vehicles.Length > 0) && (textBoxs.Length == 0) && (InitialValue.Settings.ImagePath1 != "")) //检测到社会车辆
            //{
            //    myController.stateVehicles = true;
            //    myController.timeCarHead = DateTime.Now.Ticks;
            //    lbStateShow(m_lbState, "检测到社会车辆进入！", false);
            //    Console.WriteLine("F:检测到社会车辆进入," + DateTime.Now.ToString("HHmmss"));

            //    myController.CarHeadImage = frame.Clone();
            //}

            if ((emptycars.Length > 0) && (textBoxs.Length == 0) && (myController.stateCarHead==true) && (InitialValue.Settings.ImagePath1 != ""))  //检测到空车
            {
                if ((emptycars.Length == 1) && (emptycars[0].Rect.Height < frame.Height * 0.5))
                    return ctnNo;

                if (InitialValue.Settings.ImagePath1 == "")
                    myController.EmptyCarImage = null;
                else if (myController.EmptyCarImage == null)
                {
                    myController.stateEmptyCar = true;

                    if (!myController.stateCarHead)
                        myController.timeCarHead = DateTime.Now.Ticks;

                    myController.timeEmptyCar = DateTime.Now.Ticks;

                    lbStateShow(m_lbState, "检测到空车进入！", false);
                    Console.WriteLine("F:检测到空车进入," + DateTime.Now.ToString("HHmmss"));

                    myController.EmptyCarImage = frame.Clone();
                }
            }

            return ctnNo;
        }

        //启动线程
        public void ThreadStart()
        {
            imgReadThread.Start();
            LoopRecognizeThread.Start();
        }

        /// <summary>
        /// 经过一定时延后若后侧和侧面还未到位，则丢弃此次结果
        /// </summary>
        /// <param name="myController"></param>
        private void StateMonitor(ProcessInfo myController, int oldCode)
        {
            //时延10s,若后侧拍到，正在识别，则退出
            for (int i = 0; i < InitialValue.Settings.FrontTimeout; i++)
            {
                Thread.Sleep(1000);
                if (myController.state > ArrivedState.FrontArrived)
                { //已经到了后续步骤，则退出
                    return;
                }
            }

            //依然是前侧到位，检测到车头，且随机码不变，对结果做一次识别，重启检测流程   
            if (myController.verifyCode == oldCode)
            {
                if (myController.stateCarHead)
                {
                    //检测状态置为Recognizing
                    myController.state = ArrivedState.Recognizing;

                    //综合识别结果
                    m_myNoGather();

                    //重启检测流程
                    DetectRestart(0);
                }
                else
                {  //无车头检测，属误触发，放弃
                    //重启检测流程
                    DetectRestart(0);
                }
            }
        }

        /// <summary>
        /// 处理识别任务
        /// </summary>
        /// <param name="imgType">图像类型，前相机，前相机辅助，后相机，后相机辅助，区别是后缀是辅助的图像不做保存</param>
        /// <param name="frame">待识别的帧</param>
        /// <param name="grab_pb">要传入识别结果的picturebox控件</param>
        private void RecognizeImage(string imgType, Mat frame, PictureBox grab_pb)
        {
            //新建箱号信息结构体，来保存此次识别的信息
            ContainerData myData = new ContainerData();
            //此次抓拍时间
            myData.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string tmpPath = ContainerHelper.getFilePath(myData,myController);
            string ImgName = ContainerHelper.getFileName(myData, imgType,myController);

            myData.ImagePath = Path.Combine(tmpPath, ImgName); 

            //相机机位
            myData.CameraType = imgType;
            //创建tempFrame，底下加20行的边，以存放识别结果
            Mat tempFrame = new Mat();
            //Cv2.CopyMakeBorder(frame, tempFrame, 0, 20, 0, 0, borderType: BorderTypes.Constant, new Scalar(0, 0, 0));
            tempFrame = frame.Clone();

            //文字识别
            ContainerHelper.NoRecognize(OcrDetector, OcrRecognizer, frame, ref myData, 1280f / frame.Width, frame.Height);

            //识别结果校验
            if (ContainerHelper.DataCheckOK(myData.ContainerNO))
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myData.ContainerNO} {myData.ContainerType}";
                if (myData.ContainerType=="????")
                    temStr = $"{myData.ContainerNO}";

                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(255, 255, 255), 2);
                myData.CheckResult = 1;
            }
            else
            {
                //将识别结果打印在tempFrame上
                string temStr = $"{myData.ContainerNO} {myData.ContainerType}  ?";
                if (myData.ContainerType == "????")
                    temStr = $"{myData.ContainerNO}  ?";
                Cv2.Rectangle(tempFrame, new OpenCvSharp.Point(tempFrame.Width / 4, tempFrame.Height - 26), new OpenCvSharp.Point(tempFrame.Width / 4 + 20 * temStr.Length, tempFrame.Height - 1), new Scalar(0, 0, 0), -1);
                Cv2.PutText(tempFrame, temStr, new OpenCvSharp.Point(tempFrame.Width/4, tempFrame.Height - 2), HersheyFonts.HersheySimplex, 1, new Scalar(255, 255, 255), 2);
                myData.CheckResult = 0;
            }

            //保存图像
            //ContainerHelper.ImgSave(tempFrame, myController.tempPath, ImgName);
            //ContainerHelper.ImgSave(tempFrame, tmpPath, ImgName);
            myData.grab_pb = grab_pb; //后面一次性从这个控件中读图像存盘

            //将图像发送到前台显示
            Rect rect = new Rect(0, 0, tempFrame.Width, tempFrame.Height);   //存储的区域
            if (imgType == "前相机")
            {
                rect.Left = InitialValue.Video[1].leftMargin1;
                rect.Width = InitialValue.Video[1].widthMargin1;
            }
            else
            {
                rect.Left = InitialValue.Video[1].leftMargin2;
                rect.Width = InitialValue.Video[1].widthMargin2;
            }

            ImageShow2Pb(tempFrame.Clone(rect), grab_pb);

            //箱号识别结果放入List
            myController.dataList.Add(myData); 
        }

        //重置检测任务,SaveFlag:是否存储图片，如果是true，代表这次识别是有效的，否则是放弃这次识别
        private void DetectRestart(int waitDuration)
        {
            //等待waitDuration秒再重启
            Mat frame = new Mat();
             for (int i = 0; i < waitDuration; i++)
            {
                Thread.Sleep(1000);
                if (rawImgQueue.Count > 0)
                {
                    rawImgQueue.TryDequeue(out frame);
                    Rect subImageRect = new Rect(frame.Width / 4, 0, frame.Width / 2, frame.Height / 2);  //取中上部的一小部分图像
                    VideoForm.ImageStaticFlag1 = ContainerHelper.StaticImageDetection(frame, frame.Clone(subImageRect));
                    if (VideoForm.ImageStaticFlag1)
                    {
                        break;
                    }
                }
            }
            frame.Dispose();

            //生成新的状态码
            myController.verifyCode = ContainerHelper.codeGet(myController.verifyCode);
            myController.picClearFlag = false;
            myController.dataList = new List<ContainerData>();
            System.GC.Collect();

            //重置arrivedState
            myController.stateCarHead = false;
            myController.stateEmptyCar = false;
            myController.stateVehicles = false;
            arrivedState = false;
            myController.state = ArrivedState.Wait;
            myController.state1 = "";
            myController.stateF = false;
            myController.stateM = false; 
            myController.stateB = false;
            myController.timeCarHead = 0;
            myController.timeEmptyCar = 0;
            if (myController.CarHeadImage != null)
            {
                myController.CarHeadImage.Dispose();
                myController.CarHeadImage = null;
            }
            if (myController.EmptyCarImage != null)
            {
                myController.EmptyCarImage.Dispose();
                myController.EmptyCarImage = null;
            }

            VideoForm.FrontTriggerFlag = false;
            VideoForm.BackTriggerFlag = false;

            lbStateShow(m_lbState, "等待车辆进入...", false);
        }
    }

}
