﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramConsole
{
    class ContainerHelper
    {
        //集装箱信息结构体
        public struct ContainerData
        {
            public string ContainerNO;
            public string ContainerType;
            public string CameraType;
            public string ImagePath;
            public string GrabTime;
            public int CheckResult;
        }


    }
}
