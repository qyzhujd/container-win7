﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static ProgramConsole.ContainerHelper;

namespace ProgramConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ////用于建立数据库连接
            //SQLiteConnection mConn = new SQLiteConnection();
            //mConn.ConnectionString = @"data source = " + System.IO.Directory.GetCurrentDirectory() + "/ContainerDB.db";
            //mConn.Open();

            //List<ContainerData> dataList = new List<ContainerData>();

            ////放入六次数据
            //for(int i = 0; i < 6; i++)
            //{
            //    ContainerData myData = new ContainerData(); ; c
            //    myData.ContainerNO = "MBDI1234567";
            //    myData.ContainerType = "45G1";
            //    myData.CameraType = "前相机";
            //    myData.ImagePath = "...";
            //    myData.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //    myData.CheckResult = 1;
            //    dataList.Add(myData);
            //}

            ////将数据写入数据库
            //for(int i = 0; i < dataList.ToArray().Length; i++)
            //{
            //    SQLiteCommand cmd = new SQLiteCommand();
            //    cmd.Connection = mConn;
            //    cmd.CommandText = "INSERT INTO [TABLE] (ContainerNo,ContainerType,CameraType,ImagePath,GrabTime,CheckResult) VALUES (@ContainerNo,@ContainerType,@CameraType,@ImagePath,@GrabTime,@CheckResult)";
            //    cmd.Parameters.Add("ContainerNo", System.Data.DbType.String).Value = dataList[i].ContainerNO;
            //    cmd.Parameters.Add("ContainerType", System.Data.DbType.String).Value = dataList[i].ContainerType;
            //    cmd.Parameters.Add("CameraType", System.Data.DbType.String).Value = dataList[i].CameraType;
            //    cmd.Parameters.Add("ImagePath", System.Data.DbType.String).Value = dataList[i].ImagePath;
            //    cmd.Parameters.Add("GrabTime", System.Data.DbType.String).Value = dataList[i].GrabTime;
            //    cmd.Parameters.Add("CheckResult", System.Data.DbType.Int32).Value = dataList[i].CheckResult;
            //    cmd.ExecuteNonQuery();

            //}
            //Console.ReadKey();

            //string A = "ABCD";
            //string B = "EBDC";
            //int similarity = Calculate(A, B);
            //Console.WriteLine("相似度：" + similarity);
            //Console.ReadKey();

            //string input = "kkl12lkk";
            //string[] results = MatchLetters(input);

            //foreach (string result in results)
            //{
            //    Console.WriteLine(result);
            //}
            //Console.ReadKey();

            List<ContainerData> dataList = new List<ContainerData>();
            ContainerData myData1 = new ContainerData(); ;
            myData1.ContainerNO = "???????????";
            myData1.ContainerType = "22G1";
            myData1.CameraType = "前相机";
            myData1.ImagePath = "...";
            myData1.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            myData1.CheckResult = 0;
            dataList.Add(myData1);

            ContainerData myData2 = new ContainerData(); ;
            myData2.ContainerNO = "???????????";
            myData2.ContainerType = "22G1";
            myData2.CameraType = "前相机辅助";
            myData2.ImagePath = "...";
            myData2.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            myData2.CheckResult = 0;
            dataList.Add(myData2);

            //ContainerData myData3 = new ContainerData(); ;
            //myData3.ContainerNO = "TEMU253443";
            //myData3.ContainerType = "22G1";
            //myData3.CameraType = "后相机";
            //myData3.ImagePath = "...";
            //myData3.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //myData3.CheckResult = 0;
            //dataList.Add(myData3);

            //ContainerData myData4 = new ContainerData(); ;
            //myData4.ContainerNO = "TEMU253443";
            //myData4.ContainerType = "22G1";
            //myData4.CameraType = "后相机辅助";
            //myData4.ImagePath = "...";
            //myData4.GrabTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //myData4.CheckResult = 0;
            //dataList.Add(myData4);

            NoGather(dataList);
            Console.ReadKey();
        }

        public static string[] FindLongestStrings(string[] array)
        {
            // 计算最长字符串的长度
            int maxLength = 0;
            foreach (string str in array)
            {
                if (str.Length > maxLength)
                {
                    maxLength = str.Length;
                }
            }

            // 筛选出最长字符串
            List<string> longestStrings = new List<string>();
            foreach (string str in array)
            {
                if (str.Length == maxLength)
                {
                    longestStrings.Add(str);
                }
            }

            // 将最长字符串转换为数组并返回
            return longestStrings.ToArray();
        }

        //集装箱信息结构体
        public struct ContainerData
        {
            public string ContainerNO;
            public string ContainerType;
            public string CameraType;
            public string ImagePath;
            public string GrabTime;
            public int CheckResult;
        }

        public static void NoGather(List<ContainerData> dataList)
        {
            List<ContainerData> dataListF = new List<ContainerData>();
            List<ContainerData> dataListB = new List<ContainerData>();
            //新建综合箱号结构体
            ContainerData myDataF = new ContainerData();
            ContainerData myDataB = new ContainerData();
            ContainerData myData1 = new ContainerData();
            ContainerData myData2 = new ContainerData();

            myDataF.CameraType = "前相机";
            myDataB.CameraType = "后相机";
            myDataF.CheckResult = 0;
            myDataB.CheckResult = 0;

            //分出前后箱号数据
            foreach (ContainerData dt in dataList)
            {
                if (dt.CameraType.Contains("前"))
                {
                    dataListF.Add(dt);
                }
                else
                {
                    dataListB.Add(dt);
                }
            }

            //从前箱号数据中综合出myDataF
            NoExtract(dataListF, ref myDataF);
            //当有后箱号数据时，从后箱号数据中综合出myDataB，并进行双箱的判定
            if (dataListB.ToArray().Length != 0)
            {
                NoExtract(dataListB, ref myDataB);
                //判定箱型
                if (myDataF.ContainerType[0] == '4' || myDataF.ContainerType[0] == 'L' || myDataB.ContainerType[0] == '4' || myDataB.ContainerType[0] == 'L' || Calculate(RemoveQuestionMarks(myDataF.ContainerNO), RemoveQuestionMarks(myDataB.ContainerNO)) <= 3)
                {
                    //说明是单箱
                    //若前箱可以通过校验
                    if (myDataF.CheckResult == 1)
                    {
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }
                    //若后箱箱号可以通过校验
                    else if (myDataB.CheckResult == 1)
                    {
                        //更新箱号和CheckResult
                        myData1 = myDataB;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }
                    ////若前箱字母和后箱数字组合可以通过校验
                    else if (DataCheck(myDataF.ContainerNO.Substring(0, 4) + myDataB.ContainerNO.Substring(4, 6)).ToString() == myDataB.ContainerNO.Substring(10))
                    {
                        myData1 = myDataF;
                        myData1.ContainerNO = myDataF.ContainerNO.Substring(0, 4) + myDataB.ContainerNO.Substring(4);
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                        myData1.CheckResult = 1;
                    }
                    ////若前箱数字和后箱字母组合可以通过校验
                    else if (DataCheck(myDataB.ContainerNO.Substring(0, 4) + myDataF.ContainerNO.Substring(4, 6)).ToString() == myDataF.ContainerNO.Substring(10))
                    {
                        myData1 = myDataF;
                        myData1.ContainerNO = myDataB.ContainerNO.Substring(0, 4) + myDataF.ContainerNO.Substring(4);
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                        myData1.CheckResult = 1;
                    }
                    //都不行就发前箱的
                    else
                    {
                        myData1 = myDataF;
                        myData1.ImagePath = "...";
                        myData1.CameraType = "综合";
                    }
                    //最后赋箱型
                    if (myDataF.ContainerType != "????")
                    {
                        myData1.ContainerType = myDataF.ContainerType;
                    }
                    else if (myDataB.ContainerType != "????")
                    {
                        myData1.ContainerType = myDataB.ContainerType;
                    }
                    Console.WriteLine($"{myData1.ContainerNO} {myData1.ContainerType}");
                }
                else //说明是双箱
                {
                    myData1 = myDataF;
                    myData1.ImagePath = "...";
                    myData1.CameraType = "综合";
                    myData2 = myDataB;
                    myData2.ImagePath = "...";
                    myData2.CameraType = "综合";
                    Console.WriteLine($"{myData1.ContainerNO} {myData1.ContainerType}");
                    Console.WriteLine($"{myData2.ContainerNO} {myData2.ContainerType}");
                }
            }
            //只有前箱两张图
            else
            {
                myData1 = myDataF;
                myData1.ImagePath = "...";
                myData1.CameraType = "综合";
                Console.WriteLine($"{myData1.ContainerNO} {myData1.ContainerType}");
            }
        }

        public static void NoExtract(List<ContainerData> dataList, ref ContainerData myData)
        {
            //写入一下时间数据
            myData.GrabTime = dataList[0].GrabTime;
            //写入一下图像路径
            myData.ImagePath = dataList[0].ImagePath;
            myData.ContainerType = "????";
            //先遍历寻找箱型
            foreach (ContainerData dt in dataList)
            {
                if (dt.ContainerType != "????")
                {
                    myData.ContainerType = dt.ContainerType;
                    break;
                }
            }
            //再遍历匹配箱号,匹配到就直接return结束流程
            foreach (ContainerData dt in dataList)
            {
                if (dt.CheckResult == 1)
                {
                    myData.ContainerNO = dt.ContainerNO;
                    myData.CheckResult = 1;
                    return;
                }
            }

            //能走到这里说明没匹配到能通过校验箱号，所以通过循环来遍历寻找合适的
            //存放前四位箱号信息
            List<string> noListF = new List<string>();
            //存放后七位箱号信息
            List<string> noListS = new List<string>();
            //提取数据
            foreach (ContainerData dt in dataList)
            {
                noListF.Add(dt.ContainerNO.Substring(0, 4));
                noListS.Add(dt.ContainerNO.Substring(4));
            }
            //保个底
            myData.ContainerNO = PadString(SortStrings(noListF.ToArray())[0],4) + PadString(SortStrings(noListS.ToArray())[0],7);
            //遍历匹配
            foreach (string dtF in noListF)
            {
                foreach (string dtS in noListS)
                {
                    if (DataCheck(dtF + dtS.Substring(0, 6)).ToString() == dtS.Substring(6))
                    {
                        myData.ContainerNO = dtF + dtS;
                        myData.CheckResult = 1;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// 根据识别到箱号的前十位计算校验码
        /// 如果箱号中存在？的情况，返回11
        /// </summary>
        /// <param name="Result">识别到的箱号</param>
        /// <returns></returns>
        public static int DataCheck(String Result)
        {
            int AscValue, p = 0, pp = 1;
            char[] cc = Result.ToCharArray();

            for (int i = 0; i < 4; i++)
            {
                AscValue = cc[i];
                if (AscValue == 63)
                {
                    return 11;
                }
                if (AscValue == 65)
                {
                    p += 10 * pp;
                }
                else if (AscValue <= 75)
                {
                    p += (AscValue - 54) * pp;
                }
                else if (AscValue <= 85)
                {
                    p += (AscValue - 53) * pp;
                }
                else if (AscValue <= 90)
                {
                    p += (AscValue - 52) * pp;
                }
                pp *= 2;
            }

            for (int i = 0; i < 6; i++)
            {
                AscValue = cc[i + 4];
                if (AscValue == 63) return 11;
                p += (AscValue - 48) * pp;
                pp *= 2;
            }

            int p1 = ((p % 11)) % 10;

            return p1;
        }

        //编辑距离算法
        public static int Calculate(string source, string target)
        {
            int[,] dp = new int[source.Length + 1, target.Length + 1];

            for (int i = 0; i <= source.Length; i++)
                dp[i, 0] = i;

            for (int j = 0; j <= target.Length; j++)
                dp[0, j] = j;

            for (int i = 1; i <= source.Length; i++)
            {
                for (int j = 1; j <= target.Length; j++)
                {
                    int cost = (source[i - 1] == target[j - 1]) ? 0 : 1;
                    dp[i, j] = Math.Min(Math.Min(dp[i - 1, j] + 1, dp[i, j - 1] + 1), dp[i - 1, j - 1] + cost);
                }
            }
            return dp[source.Length, target.Length];
        }

        /// <summary>
        /// 返回字符串中的所有七位数字，若没有，则返回相对最长的结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] MatchNumbers(string input)
        {
            List<string> results = new List<string>();
            //匹配七位数字
            for (int i = 0; i <= input.Length - 7; i++)
            {
                string substring = input.Substring(i, 7);

                if (IsNumeric(substring))
                {
                    results.Add(substring);
                }
            }
            //若没有符合条件的，则返回相对最长的
            if (results.ToArray().Length == 0)
            {
                string longestNumber = string.Empty;
                string currentNumber = string.Empty;

                foreach (char c in input)
                {
                    if (char.IsDigit(c))
                    {
                        currentNumber += c;
                    }
                    else
                    {
                        if (currentNumber.Length > longestNumber.Length)
                        {
                            longestNumber = currentNumber;
                        }
                        currentNumber = string.Empty;
                    }
                }

                // 处理最后一个数字序列
                if (currentNumber.Length > longestNumber.Length)
                {
                    longestNumber = currentNumber;
                }

                results.Add(longestNumber);
            }
            return results.ToArray();
        }

        //判断字符串中是否都为数字
        public static bool IsNumeric(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 返回字符串中所有四位字母，若没有，则返回相对最长的结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] MatchLetters(string input)
        {
            List<string> results = new List<string>();
            //匹配四位字母
            for (int i=0; i<=input.Length-4;i++)
            {
                string substring = input.Substring(i, 4);
                if (IsAlphabetic(substring))
                {
                    results.Add(substring);
                }
            }
            //若没有符合条件的，则返回相对最长的
            if(results.ToArray().Length == 0)
            {
                string longestLetter = string.Empty;
                string currentLetter = string.Empty;

                foreach(char c in input)
                {
                    if (char.IsLetter(c))
                    {
                        currentLetter += c;
                    }
                    else
                    {
                        if (currentLetter.Length > longestLetter.Length)
                        {
                            longestLetter = currentLetter;
                        }
                        currentLetter = string.Empty;
                    }
                }
                //处理最后一个字符
                if(currentLetter.Length > longestLetter.Length)
                {
                    longestLetter = currentLetter;
                }
                results.Add(longestLetter);
            }
            return results.ToArray();
        }

        //判断字符串中是否都为字母
        public static bool IsAlphabetic(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsLetter(c))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 移除字符串中所有？
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveQuestionMarks(string str)
        {
            return str.Replace("?", "");
        }

        /// <summary>
        /// 对字符串数组中的字符串进行去除问号后从大到小的排序
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] SortStrings(string[] input)
        {
            // 去除字符串中的 "?"
            string[] processedStrings = input.Select(s => s.Replace("?", "")).ToArray();

            // 按照处理后的字符串长度进行排序
            Array.Sort(processedStrings, (a, b) => b.Length.CompareTo(a.Length));

            return processedStrings;
        }

        /// <summary>
        /// 输入字符串，返回使用？填充到指定位数的字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static string PadString(string str, int N)
        {
            if (str.Length < N)
            {
                int diff = N - str.Length;
                string padding = new string('?', diff);
                return str + padding;
            }

            return str;
        }


    }


}
