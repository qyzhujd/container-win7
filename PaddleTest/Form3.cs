﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaddleTest
{
    public partial class Form3 : Form
    {
        private Mat rawFrame;
        private Mat ripeFrame;
        Graphics g1;
        Graphics g2;
        bool[] importstates;
        OpenCvSharp.Point[] points;
        int count;


        public Form3()
        {
            InitializeComponent();
            count = 0;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

            g1 = pictureBox1.CreateGraphics();//创建一个画板
            g2 = pictureBox2.CreateGraphics();//创建一个画板

            importstates = new bool[] { false, false };
            points = new OpenCvSharp.Point[8];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "请选择文件";
            dialog.Filter = "图片文件(*.jpg,*.gif,*.bmp)|*.jpg;*.gif;*.bmp";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string s = dialog.FileName;
                rawFrame = new Mat(s);
                pictureBox1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(rawFrame);
            }
        }

        private void GetImagePixLocation(OpenCvSharp.Size pictureBoxSize, OpenCvSharp.Size imageSize, OpenCvSharp.Point pictureBoxPoint, out OpenCvSharp.Point imagePoint)

        {

            imagePoint = new OpenCvSharp.Point(0, 0);

            double scale_w;
            double scale_h;
            scale_w = 1.0 * imageSize.Width / pictureBoxSize.Width;
            scale_h = 1.0 * imageSize.Height / pictureBoxSize.Height;
            imagePoint.X = Convert.ToInt32(pictureBoxPoint.X * scale_w);
            imagePoint.Y = Convert.ToInt32(pictureBoxPoint.Y * scale_h);

        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if(importstates[0] && importstates[1] && count < 8)
            {
                OpenCvSharp.Point pictrueBoxPoint = new OpenCvSharp.Point(e.X, e.Y);
                OpenCvSharp.Point imagePoint = new OpenCvSharp.Point();
                GetImagePixLocation(new OpenCvSharp.Size(pictureBox1.Width, pictureBox1.Height), new OpenCvSharp.Size(rawFrame.Width, rawFrame.Height), pictrueBoxPoint, out imagePoint);
                points[count] = imagePoint;

                if (e.Button == MouseButtons.Left)
                {
                    count++;
                    g1.FillEllipse(Brushes.Red, e.X-5, e.Y-5, 10, 10);
                    //// Create string to draw.
                    ////String drawString = count.ToString();

                    //// Create font and brush.
                    //Font drawFont = new Font("Arial", 16, FontStyle.Bold);
                    //SolidBrush drawBrush = new SolidBrush(Color.Red);

                    //// Create point for upper-left corner of drawing.
                    //float x = e.X;
                    //float y = e.Y;

                    //// Set format of string.
                    //StringFormat drawFormat = new StringFormat();
                    //drawFormat.FormatFlags = StringFormatFlags.LineLimit;
                    //// Draw string to screen.
                    ////g1.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
                }

                if (count == 8)
                {
                    string s = "";
                    foreach (OpenCvSharp.Point point in points)
                    {
                        s = s + point.X + "," + point.Y + ",";
                    }
                    s = s.Substring(0, s.Length - 1);
                    textBox1.Text = s;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "请选择文件";
            dialog.Filter = "图片文件(*.jpg,*.gif,*.bmp)|*.jpg;*.gif;*.bmp";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string s = dialog.FileName;
                ripeFrame = new Mat(s);
                pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(ripeFrame);
            }
        }

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (importstates[0] && importstates[1] && count<8)
            {
                OpenCvSharp.Point pictrueBoxPoint = new OpenCvSharp.Point(e.X, e.Y);
                OpenCvSharp.Point imagePoint = new OpenCvSharp.Point();
                GetImagePixLocation(new OpenCvSharp.Size(pictureBox1.Width, pictureBox1.Height), new OpenCvSharp.Size(ripeFrame.Width,ripeFrame.Height), pictrueBoxPoint, out imagePoint);
                points[count] = imagePoint;

                if (e.Button == MouseButtons.Left)
                {
                    count++;
                    g2.FillEllipse(Brushes.Red, e.X-5, e.Y-5, 10, 10);
                    //// Create string to draw.
                    //String drawString = count.ToString();

                    //// Create font and brush.
                    //Font drawFont = new Font("Arial", 16, FontStyle.Bold);
                    //SolidBrush drawBrush = new SolidBrush(Color.Red);

                    //// Create point for upper-left corner of drawing.
                    //float x = e.X;
                    //float y = e.Y;

                    //// Set format of string.
                    //StringFormat drawFormat = new StringFormat();
                    //drawFormat.FormatFlags = StringFormatFlags.LineLimit;
                    //// Draw string to screen.
                    //g2.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
                }
                if (count == 8)
                {
                    string s = "";
                    foreach (OpenCvSharp.Point point in points)
                    {
                        s = s + point.X + "," + point.Y + ",";
                    }
                    s = s.Substring(0, s.Length - 1);
                    textBox1.Text = s;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(rawFrame != null && ripeFrame != null)
            {
                importstates[0] = true;
                importstates[1] = true;
                g1.Clear(this.BackColor);
                g2.Clear(this.BackColor);
                pictureBox1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(rawFrame);
                pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(ripeFrame);
                count = 0;
                points = new OpenCvSharp.Point[8];
                System.GC.Collect();

            }
            else
            {
                MessageBox.Show("请导入图像！");
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            System.GC.Collect();
            if(textBox1.Text != ""){
                string input = textBox1.Text;

                int[] numbers = input.Split(',')
                                    .Select(s => int.Parse(s))
                                    .ToArray();

                Point2f[] srcPt = new Point2f[4];
                Point2f[] dstPt = new Point2f[4];
                srcPt[0] = new Point2f(numbers[0], numbers[1]);
                srcPt[1] = new Point2f(numbers[2], numbers[3]);
                srcPt[2] = new Point2f(numbers[4], numbers[5]);
                srcPt[3] = new Point2f(numbers[6], numbers[7]);
                dstPt[0] = new Point2f(numbers[8], numbers[9]);
                dstPt[1] = new Point2f(numbers[10], numbers[11]);
                dstPt[2] = new Point2f(numbers[12], numbers[13]);
                dstPt[3] = new Point2f(numbers[14], numbers[15]);

                Mat final = new Mat();
                Mat warpmatrix = Cv2.GetPerspectiveTransform(srcPt, dstPt);
                Cv2.WarpPerspective(rawFrame, final, warpmatrix, rawFrame.Size());
                Cv2.NamedWindow("dst", WindowFlags.Normal);
                Cv2.ImShow("dst", final);
                Cv2.WaitKey();
            }
            else
            {
                MessageBox.Show("还未生成校正参数");
            }
        }
    }
}
