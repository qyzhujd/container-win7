﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using HKTest.ONNX;
using Point = OpenCvSharp.Point;
using Sdcb.PaddleInference;
using Sdcb.PaddleOCR;
using Sdcb.PaddleOCR.Models;

//test  
namespace PaddleTest
{
    public partial class Form2 : Form
    {
        private string[] fileList;
        private int count;
        private int correctCount;
        private int sum;
        private MyONNX OcrDetector = new MyONNX();
        private PaddleOcrRecognizer OcrRecognizer;

        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

        public Form2()
        {
            InitializeComponent();
            textBox1.Text = @".\Model";
            textBox2.Text = @".\Model\ppocr_v3_rec_en";
            
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //打开对话框，选择文件夹
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                var dirPath = dialog.SelectedPath.Trim();
                fileList = Directory.GetFileSystemEntries(dirPath);
                Mat frame = new Mat(fileList[0]);
                pictureBox1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
            }
            else
            {
                Console.WriteLine("ERROR!!!");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", Environment.CurrentDirectory + "\\ImgSave\\");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "请选择文件";
            dialog.Filter = "图片文件(*.jpg,*.gif,*.bmp)|*.jpg;*.gif;*.bmp";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileList = new string[1];
                fileList[0] = dialog.FileName;
                Mat frame = new Mat(fileList[0]);
                pictureBox1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
            }
            Form2.ActiveForm.Text = dialog.FileName;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            count = 0;
            correctCount = 0;
            sum = fileList.Length;

            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("模型加载路径不能为空！");
                return;
            }

            try
            {
                for (int k = 0; k < fileList.Length; k++)
                {
                    //读取图像
                    Mat rawFrame = new Mat(fileList[k]);
                    //ImgCalib(ref rawFrame);
                    //裁剪图像
                    //Mat ripeFrame = rawFrame;
                    //Mat ripeFrame = ContrastAjust(rawFrame[new Rect(0, 0, 640, 480)], 115);
                    Mat ripeFrame = rawFrame[new Rect(0, 20, rawFrame.Width, rawFrame.Height - 40)];
                    //Mat ripeFrame = rawFrame[new Rect(100, 20, rawFrame.Width-150, rawFrame.Height - 200)];
                    //Mat ripeFrame = BarrelDistortion(rawFrame[new Rect(0, 20, rawFrame.Width, rawFrame.Height - 40)],-0.0003);
                    //ripeFrame = ripeFrame[new Rect(100, 20, ripeFrame.Width - 150, ripeFrame.Height - 200)];
                    //放大图像至原来的三倍
                    float kFactor = 1920 / ripeFrame.Width;      //放大倍数
                    Cv2.Resize(ripeFrame, ripeFrame, new OpenCvSharp.Size(ripeFrame.Width * kFactor, ripeFrame.Height * kFactor), 0, 0, InterpolationFlags.Linear);
                    //返回检测结果,文本检测的返回结果没有置信度
                    
                    stopwatch.Reset(); stopwatch.Start();
                    resultPP[] rects = OcrDetector.Run(ripeFrame);
                    stopwatch.Stop();
                    txtTime1.Text = stopwatch.ElapsedMilliseconds.ToString();

                    pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(ripeFrame);

                    for (int i = 0; i < rects.Length; i++)
                    {
                        if (rects[i].Confidence > 0.5f)
                        {
                            //将检测框绘制到图像上
                            Cv2.Rectangle(ripeFrame, rects[i].Rect, new Scalar(0, 0, 255), 2, LineTypes.Link8);
                            Cv2.Rectangle(ripeFrame, new Point(rects[i].Rect.TopLeft.X, rects[i].Rect.TopLeft.Y - 20),
                                new Point(rects[i].Rect.BottomRight.X, rects[i].Rect.TopLeft.Y), new Scalar(0, 255, 255), -1);
                            Cv2.PutText(ripeFrame, rects[i].Confidence.ToString("0.00"),
                                new Point(rects[i].Rect.X, rects[i].Rect.Y - 5),
                                HersheyFonts.HersheySimplex, 0.6, new Scalar(0, 0, 0), 1);
                        }
                    }
                    //存放文本识别结果
                    List<PaddleOcrRecognizerResult> resultList = new List<PaddleOcrRecognizerResult>();
                    string tempStr = "";
                    
                    //将每一块可能的检测区域从图像上裁切下来，存放进mats数组
                    Mat[] mats =
                    rects.Select(rect =>
                    {
                        Mat roi = ripeFrame.Clone(GetCropedRect(rect.Rect, ripeFrame.Size())); //[GetCropedRect(rect.BoundingRect(), ripeFrame.Size())];
                        bool wider = roi.Width > roi.Height;
                        if (!wider)
                        {
                            Cv2.Transpose(roi, roi);
                            Cv2.Flip(roi, roi, FlipMode.X);
                        }
                        return roi;
                    })
                    .ToArray();
                    //遍历mats数组，对其中图像块进行识别，将其结果放入resultList和tempStr，因此tempStr中存放的是不经筛选的识别结果
                    try
                    {
                        stopwatch.Reset(); stopwatch.Start();
                        for (int kk = 0; kk < mats.Length; kk++)
                        {
                            Cv2.ImWrite($"./results/{kk}.jpg", mats[kk]);
                            PaddleOcrRecognizerResult tmpResult = OcrRecognizePN(mats[kk]);
                            resultList.Add(tmpResult);
                            if (tempStr == "")
                                tempStr = tmpResult.Text;
                            else
                                tempStr = tempStr + " " + tmpResult.Text;
                        }
                        txtTime2.Text = stopwatch.ElapsedMilliseconds.ToString();
                    }
                    finally
                    {
                        foreach (Mat mat in mats)
                        {
                            mat.Dispose();
                        }
                    }

                    textBox3.Text = tempStr;

                    //缩放图像至原尺寸
                    Cv2.Resize(ripeFrame, ripeFrame, new OpenCvSharp.Size(ripeFrame.Width / kFactor, ripeFrame.Height / kFactor), 0, 0, InterpolationFlags.Linear);
                    Cv2.CopyMakeBorder(ripeFrame, ripeFrame, 20, 20, 0, 0, borderType: BorderTypes.Constant, new Scalar(0, 0, 0));
                    Console.WriteLine(tempStr);

                    //对识别结果进行整合，获得箱号
                    string containerNo = NoContact(resultList.ToArray());
                    //将不经筛选的识别结果打印在ripeFrame上
                    if (tempStr != "")
                    {
                        Cv2.PutText(ripeFrame, tempStr, new OpenCvSharp.Point(0, 16), HersheyFonts.HersheySimplex, 0.6, new Scalar(255, 255, 255), 2);
                        //将经过整合的箱号结果打印在ripeFrame上
                        Cv2.PutText(ripeFrame, $"{containerNo.Substring(0, 11)} {containerNo.Substring(11)}", new OpenCvSharp.Point(0, ripeFrame.Height - 4), HersheyFonts.HersheySimplex, 0.6, new Scalar(255, 255, 255), 2);
                    }
                    //将第一张识别结果发送到前台
                    if (k == 0)
                    {
                        pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(ripeFrame);
                    }
                    //识别结果校验
                    if (DataCheck(containerNo.Substring(0, 10)).ToString() == containerNo.Substring(10, 1))
                    {
                        correctCount++;
                    }
                    else
                    {
                        //保存图像
                        Cv2.ImWrite($"./ImgSave/{Path.GetFileName(fileList[k])}", ripeFrame);
                    }
                    count++;
                    Double d = correctCount * 1.0 / sum * 100;
                    label2.Text = String.Format("{0:F}", d) + "%";
                    label4.Text = $"{count}/{sum}";
                    Application.DoEvents();
                    System.GC.Collect();
                }
            }
            finally
            {
//                OcrDetector.Dispose();
//                OcrRecognizer.Dispose();
            }
        }

        //自动正反识别
        public PaddleOcrRecognizerResult OcrRecognizePN(Mat mat)
        {
            PaddleOcrRecognizerResult result = OcrRecognizer.Run(mat); //OcrRecognizer.Run(mats[kk]);

            if (!DataCheckOK(result.Text))
            {//顶上箱号反掉的反过来识别一下看看
                Cv2.Flip(mat, mat, FlipMode.XY);
                PaddleOcrRecognizerResult result1 = OcrRecognizer.Run(mat); // OcrRecognizer.Run(mats[kk]); 
                if (result1.Score > result.Score)
                    return result1;
                else
                    return result;
            }
            else
            {
                return result;
            }
        }

        public static bool DataCheckOK(string ContainerNo)
        {
            if (ContainerNo == null)
            {
                return false;
            }
            if (ContainerNo.Length < 11)
                return false;

            ContainerNo = ContainerNo.Substring(0, 11);

            if (!System.Text.RegularExpressions.Regex.IsMatch(ContainerNo.Substring(0, 4), @"^[A-Z]*$"))   //大写字母
                return false;

            if (!System.Text.RegularExpressions.Regex.IsMatch(ContainerNo.Substring(4, 7), @"^[0-9]*$"))  //数字
                return false;

            if (ContainerNo.Contains("?"))
                return false;

            if (DataCheck(ContainerNo.Substring(0, 10)).ToString() == ContainerNo.Substring(10))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ImgCalib(ref Mat frame)
        {
            //string input = "52,28,609,24,499,443,106,441,162,18,489,17,466,423,147,415";//正
            string input = "4,45,614,31,486,422,127,420,121,28,501,18,468,383,146,381";
            //string input = "108,94,540,87,450,368,167,365,162,93,595,88,465,367,182,362";//右偏

            int[] numbers = input.Split(',')
                                .Select(s => int.Parse(s))
                                .ToArray();

            Point2f[] srcPt = new Point2f[4];
            Point2f[] dstPt = new Point2f[4];
            srcPt[0] = new Point2f(numbers[0], numbers[1]);
            srcPt[1] = new Point2f(numbers[2], numbers[3]);
            srcPt[2] = new Point2f(numbers[4], numbers[5]);
            srcPt[3] = new Point2f(numbers[6], numbers[7]);
            dstPt[0] = new Point2f(numbers[8], numbers[9]);
            dstPt[1] = new Point2f(numbers[10], numbers[11]);
            dstPt[2] = new Point2f(numbers[12], numbers[13]);
            dstPt[3] = new Point2f(numbers[14], numbers[15]);

            Mat warpmatrix = Cv2.GetPerspectiveTransform(srcPt, dstPt);
            Cv2.WarpPerspective(frame, frame, warpmatrix, frame.Size());
        }

        public Mat BarrelDistortion(Mat image, double k)
        {
            int height = image.Height;
            int width = image.Width;
            double cx = width / 2.0;
            double cy = height / 2.0;

            Mat distortedImage = new Mat(image.Size(), image.Type());

            double kNormalized = k / (width / 2.0);

            Mat mapX = new Mat(image.Size(), MatType.CV_32FC1);
            Mat mapY = new Mat(image.Size(), MatType.CV_32FC1);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double dx = x - cx;
                    double dy = y - cy;
                    double r = Math.Sqrt(dx * dx + dy * dy);

                    double distortedR = r * (1 + kNormalized * r * r);
                    double scale = r != 0 ? distortedR / r : 0;
                    double distortedX = dx * scale + cx;
                    double distortedY = dy * scale + cy;
                    if (distortedX >= 0 && distortedX < width && distortedY >= 0 && distortedY < height)
                    {
                        mapX.At<float>(y, x) = (float)distortedX;
                        mapY.At<float>(y, x) = (float)distortedY;
                    }
                }
            }
            Cv2.Remap(image, distortedImage, mapX, mapY, InterpolationFlags.Linear);
            return distortedImage;
        }

        /// <summary>
        /// 对paddleocr的单张箱号识别结果进行初处理
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private string NoContact(PaddleOcrRecognizerResult[] results)
        {
            //声明三个部分，字母部分，数字部分，箱号部分
            string lettersPart, numbersPart, typePart;
            //匹配箱号
            Regex typeReg = new Regex("[4,2,A-Z][0-9,A-Z][G,U,L,R][0,1,2,3]");
            lettersPart = "????";
            numbersPart = "???????";
            typePart = "????";

            //存放箱号字母部分的字符数组
            string[] letterArray = { };
            //存放箱号数字部分的字符数组
            string[] numberArray = { };

            //寻找箱号的七位数字，存入数组
            foreach (PaddleOcrRecognizerResult result in results)
            {
                if (result.Score > 0.7)
                {
                    numberArray = MergeArrays(numberArray, MatchNumbers(result.Text.Replace("O", "0")));
                }
            }
            numberArray = FindLongestStrings(numberArray);

            //寻找箱号的四位字母，存入数组
            foreach (PaddleOcrRecognizerResult result in results)
            {
                if (result.Score > 0.7)
                {
                    letterArray = MergeArrays(letterArray, MatchLetters(result.Text.Replace("0", "O")));
                }
            }
            letterArray = FindLongestStrings(letterArray);
            
            //经过校验组合出箱号，如果没有，就把字母部分和数字部分各自赋值为数组的第一个元素
            if (letterArray.Length != 0)
            {
                lettersPart = letterArray[0];
            }
            if (numberArray.Length != 0)
            {
                numbersPart = numberArray[0];
            }
            //要判定一下返回的结果是否位数足够
            if (letterArray.Length != 0 && numberArray.Length != 0 && letterArray[0].Length == 4 && numberArray[0].Length == 7)
            {
                foreach (string letters in letterArray)
                {
                    foreach (string numbers in numberArray)
                    {
                        if (DataCheck(letters + numbers.Substring(0, 6)).ToString() == numbers.Substring(6))
                        {
                            lettersPart = letters;
                            numbersPart = numbers;
                            goto endLoop;
                        }
                    }
                }
            endLoop:;
            }

            //再寻找箱型数据
            foreach (PaddleOcrRecognizerResult result in results)
            {
                if (result.Score > 0.7 && result.Text.Length < 5)
                {
                    if (typeReg.IsMatch(result.Text))
                    {
                        //返回匹配的第一个结果
                        typePart = typeReg.Match(result.Text).ToString();
                        break;
                    }
                }
            }
            return $"{PadString(lettersPart, 4)}{PadString(numbersPart, 7)}{typePart}";
        }

        /// <summary>
        /// 根据识别到箱号的前十位计算校验码
        /// 如果箱号中存在？的情况，返回11
        /// </summary>
        /// <param name="Result">识别到的箱号</param>
        /// <returns></returns>
        public static int DataCheck(String Result)
        {
            int AscValue, p = 0, pp = 1;
            char[] cc = Result.ToCharArray();

            for (int i = 0; i < 4; i++)
            {
                AscValue = cc[i];
                if (AscValue == 63)
                {
                    return 11;
                }
                if (AscValue == 65)
                {
                    p += 10 * pp;
                }
                else if (AscValue <= 75)
                {
                    p += (AscValue - 54) * pp;
                }
                else if (AscValue <= 85)
                {
                    p += (AscValue - 53) * pp;
                }
                else if (AscValue <= 90)
                {
                    p += (AscValue - 52) * pp;
                }
                pp *= 2;
            }

            for (int i = 0; i < 6; i++)
            {
                AscValue = cc[i + 4];
                if (AscValue == 63) return 11;
                p += (AscValue - 48) * pp;
                pp *= 2;
            }

            int p1 = ((p % 11)) % 10;

            return p1;
        }

        private static Rect GetCropedRect(Rect rect, OpenCvSharp.Size size)
        {
            return Rect.FromLTRB(
                ClampValue(rect.Left, 0, size.Width),
                ClampValue(rect.Top, 0, size.Height),
                ClampValue(rect.Right, 0, size.Width),
                ClampValue(rect.Bottom, 0, size.Height));
        }
        private static int ClampValue(int x, int min, int max)
        {
            if (x < min) return min;
            else
            {
                if (x >= max) return max-1;
                else
                    return x;
            }
        }

        private static Mat ContrastAjust(Mat src, int contrastValue)
        {
            Mat dst = new Mat(src.Size(), src.Type());
            for (int y = 0; y < src.Rows; y++)
            {
                for (int x = 0; x < src.Cols; x++)
                {
                    Vec3b color = new Vec3b
                    {
                        Item0 = Saturate_cast(src.Get<Vec3b>(y, x).Item0 * (contrastValue * 0.01)), // B
                        Item1 = Saturate_cast(src.Get<Vec3b>(y, x).Item1 * (contrastValue * 0.01)), // G
                        Item2 = Saturate_cast(src.Get<Vec3b>(y, x).Item2 * (contrastValue * 0.01))  // R
                    };
                    dst.Set(y, x, color);
                }
            }
            return dst;
        }

        //要确保运算后的像素值在正确的范围内
        private static byte Saturate_cast(double n)
        {
            if (n <= 0)
            {
                return 0;
            }
            else if (n > 255)
            {
                return 255;
            }
            else

            {
                return (byte)n;
            }
        }

        /// <summary>
        /// 返回字符串中的所有七位数字，若没有，则返回相对最长的结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] MatchNumbers(string input)
        {
            List<string> results = new List<string>();
            //匹配七位数字
            for (int i = 0; i <= input.Length - 7; i++)
            {
                string substring = input.Substring(i, 7);

                if (IsNumeric(substring))
                {
                    results.Add(substring);
                }
            }
            //若没有符合条件的，则返回相对最长的
            if (results.ToArray().Length == 0)
            {
                string longestNumber = string.Empty;
                string currentNumber = string.Empty;

                foreach (char c in input)
                {
                    if (char.IsDigit(c))
                    {
                        currentNumber += c;
                    }
                    else
                    {
                        if (currentNumber.Length > longestNumber.Length)
                        {
                            longestNumber = currentNumber;
                        }
                        currentNumber = string.Empty;
                    }
                }

                // 处理最后一个数字序列
                if (currentNumber.Length > longestNumber.Length)
                {
                    longestNumber = currentNumber;
                }

                results.Add(longestNumber);
            }
            return results.ToArray();
        }

        //判断字符串中是否都为数字
        public static bool IsNumeric(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 返回字符串中所有四位字母，若没有，则返回相对最长的结果
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string[] MatchLetters(string input)
        {
            Regex lettersReg = new Regex("[A-Z]{0,3}U");
            List<string> results = new List<string>();
            //匹配四位字母且最后一位为U的
            for (int i = 0; i <= input.Length - 4; i++)
            {
                string substring = input.Substring(i, 4);
                if (IsAlphabetic(substring) && substring[substring.Length-1] == 'U')
                {
                    results.Add(substring);
                }
            }
            //若没有符合条件的，则返回相对最长的
            if (results.ToArray().Length == 0)
            {
                if (lettersReg.IsMatch(input))
                {
                    //返回匹配的第一个结果
                    results.Add(lettersReg.Match(input).ToString());
                }
            }
            return results.ToArray();
        }

        //判断字符串中是否都为字母
        public static bool IsAlphabetic(string value)
        {
            foreach (char c in value)
            {
                if (!char.IsLetter(c))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 将字符串数组 B 的内容放入字符串数组 A 中
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        public static string[] MergeArrays(string[] A, string[] B)
        {
            // 创建一个新的数组，大小为 A.Length + B.Length
            string[] result = new string[A.Length + B.Length];

            // 复制 A 数组的内容到结果数组
            Array.Copy(A, result, A.Length);

            // 复制 B 数组的内容到结果数组的末尾
            Array.Copy(B, 0, result, A.Length, B.Length);

            return result;
        }

        /// <summary>
        /// 输入字符串，返回使用？填充到指定位数的字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="N"></param>
        /// <returns></returns>
        public static string PadString(string str, int N)
        {
            if (str.Length < N)
            {
                int diff = N - str.Length;
                string padding = new string('?', diff);
                return str + padding;
            }

            return str;
        }

        /// <summary>
        /// 移除字符串中所有？
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveQuestionMarks(string str)
        {
            return str.Replace("?", "");
        }

        /// <summary>
        /// 筛选出字符串数组中字符长度最长的那些元素
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string[] FindLongestStrings(string[] array)
        {
            // 计算最长字符串的长度
            int maxLength = 0;
            foreach (string str in array)
            {
                if (str.Length > maxLength)
                {
                    maxLength = str.Length;
                }
            }

            // 筛选出最长字符串
            List<string> longestStrings = new List<string>();
            foreach (string str in array)
            {
                if (str.Length == maxLength)
                {
                    longestStrings.Add(str);
                }
            }

            // 将最长字符串转换为数组并返回
            return longestStrings.ToArray();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //打开对话框，选择文件夹
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                var dirPath = dialog.SelectedPath.Trim();
                textBox2.Text = dirPath;
            }
            else
            {
                Console.WriteLine("ERROR!!!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //打开对话框，选择文件夹
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                var dirPath = dialog.SelectedPath.Trim();
                textBox1.Text = dirPath;
            }
            else
            {
                Console.WriteLine("ERROR!!!");
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            OcrDetector.ONNX_dispose();
            OcrRecognizer.Dispose();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //初始化paddleocr检测器
            //PaddleOcrDetector detector = new PaddleOcrDetector("./Model/det_inference/");
            lblGPU.Text = "正在初始化识别模型...";
            try
            {
                OcrDetector.ONNX_Load(textBox1.Text + @"\DetOCR.onnx", textBox1.Text + @"\labelOCR.txt");
                lblGPU.Text = "初始化完成，系统配置了GPU！";
            }
            catch
            {
                OcrDetector.ONNX_Load_CPU(textBox1.Text + @"\DetOCR.onnx", textBox1.Text + @"\labelOCR.txt");
                lblGPU.Text = "初始化完成，系统没有配置GPU！";
            }


            //初始化paddleocr识别器
            //if (lblGPU.Text == "初始化完成，系统配置了GPU！")
            string labelfile = @"\en_dict.txt";
            try
            {
                //OcrRecognizer = new PaddleOcrRecognizer(OcrRecogModel, PaddleDevice.Gpu());
                OcrRecognizer = new PaddleOcrRecognizer(RecognizationModel.FromDirectory(textBox2.Text, textBox2.Text + labelfile, ModelVersion.V3), PaddleDevice.Gpu());
            }
            //else
            catch
            {
                //OcrRecognizer = new PaddleOcrRecognizer(OcrRecogModel, PaddleDevice.Openblas());
                OcrRecognizer = new PaddleOcrRecognizer(RecognizationModel.FromDirectory(textBox2.Text, textBox2.Text + labelfile, ModelVersion.V3), PaddleDevice.Mkldnn());
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            button7_Click(null, null);
        }
    }
}
