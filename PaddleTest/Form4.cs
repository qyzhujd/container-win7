﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using HKTest.ONNX;
using Point = OpenCvSharp.Point;
using Sdcb.PaddleInference;
using Sdcb.PaddleOCR;
using Sdcb.PaddleOCR.Models;


namespace PaddleTest
{
    public partial class Form4 : Form
    {
        private Mat rawFrame;
        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

        private MyONNX detector = new MyONNX();
        private PaddleOcrRecognizer OcrRecognizer;

        public Form4()
        {
            InitializeComponent();
            
            //初始化textBox路径为目标检测模型路径
            textBox1.Text = @".\Model";
            textBox2.Text = @".\Model\ppocr_v3_rec_en";

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "请选择文件";
            dialog.Filter = "图片文件(*.jpg,*.gif,*.bmp)|*.jpg;*.gif;*.bmp";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                rawFrame = new Mat(dialog.FileName);
                pictureBox1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(rawFrame);
            }
            Form4.ActiveForm.Text = dialog.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //打开对话框，选择文件夹
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                var dirPath = dialog.SelectedPath.Trim();
                textBox1.Text = dirPath;
            }
            else
            {
                Console.WriteLine("ERROR!!!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("模型加载路径不能为空！");
                return;
            }
            {
//                Cv2.Resize(rawFrame, rawFrame, new OpenCvSharp.Size(640, 480), 0, 0, InterpolationFlags.Linear);
                stopwatch.Reset(); stopwatch.Start();
                resultPP[] results = detector.Run(rawFrame);
                stopwatch.Stop();
                txtTime.Text = stopwatch.ElapsedMilliseconds.ToString();

                Mat showimage = rawFrame.Clone();
                
                pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(showimage);

                //显示检测框
                for (int i = 0; i < results.Length; i++)
                {
                    if (results[i].Confidence > 0.5f)
                    {
                        Cv2.Rectangle(showimage, results[i].Rect, new Scalar(0, 0, 255), 2, LineTypes.Link8);
                        Cv2.Rectangle(showimage, new Point(results[i].Rect.TopLeft.X, results[i].Rect.TopLeft.Y - 20),
                            new Point(results[i].Rect.BottomRight.X, results[i].Rect.TopLeft.Y), new Scalar(0, 255, 255), -1);
                        //Cv2.PutText(showimage, results[i].LabelName + "-" + results[i].Confidence.ToString("0.00"),
                        //    new Point(results[i].Rect.X, results[i].Rect.Y - 5),
                        //    HersheyFonts.HersheySimplex, 0.6, new Scalar(0, 0, 0), 1);
                        Cv2.PutText(showimage, results[i].Confidence.ToString("0.00")+","+ results[i].LabelName.ToString(),
                            new Point(results[i].Rect.X, results[i].Rect.Y - 5),
                            HersheyFonts.HersheySimplex, 0.6, new Scalar(0, 0, 0), 1);
                        pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(showimage);

                        string ctnNo1 = "";
                        string ctnNo2 = "";
                        OcrDetection(rawFrame, results, ref ctnNo1);
                        OcrDetectionV(rawFrame, results, ref ctnNo2);

                        txtNO.Text = ctnNo1 + "；" + ctnNo2;
                    }
                }
            }           
        }


        private static Rect GetCropedRect(Rect rect, OpenCvSharp.Size size)
        {
            return Rect.FromLTRB(
                ClampValue(rect.Left, 0, size.Width),
                ClampValue(rect.Top, 0, size.Height),
                ClampValue(rect.Right, 0, size.Width),
                ClampValue(rect.Bottom, 0, size.Height));
        }
        private static int ClampValue(int x, int min, int max)
        {
            if (x < min) return min;
            else
            {
                if (x >= max) return max - 1;
                else
                    return x;
            }
        }

        //水平方向文本检测识别
        //从检测结果中提取文本框，并识别为文本返回
        //Frame是图像，results是检测结果
        //返回文本框的垂直位置，如果是-1，则没有识别到
        public int OcrDetection(Mat frame, resultPP[] results, ref string CtnNo)
        {
            //对最顶上的一个水平文本块进行识别
            CtnNo = "";
            var text1 = results.Where(x => x.Confidence > 0.45 && x.LabelName == "Text_Block" && x.Rect.Width > x.Rect.Height * 1.8).OrderBy(x => x.Rect.Y).ToArray();
            if (text1 != null)
            {
                for (int i=0; i<text1.Length; i++)
                {
                    Mat roi = frame.Clone(GetCropedRect(text1[i].Rect,frame.Size()));

                    string NoString1 = OcrImage(roi);

                    Cv2.Flip(roi, roi, FlipMode.XY);  //图像水平垂直翻转
                    string NoString2 = OcrImage(roi);

                    if (i == 0)
                        CtnNo = NoString1 + "；" + NoString2;
                    else
                        CtnNo += "；"+NoString1 + "；" + NoString2;
                }
            }

            return 0;
        }

        //垂直方向文本检测识别
        public int OcrDetectionV(Mat frame, resultPP[] results, ref string CtnNo)
        {
            //对最顶上的一个垂直文本块进行识别
            var text1 = results.Where(x => x.Confidence > 0.45 && x.LabelName == "Text_Block" && x.Rect.Width < x.Rect.Height * 1.8 && x.Rect.Y > frame.Height * 0.02 && x.Rect.Y < frame.Height * 0.23).OrderBy(x => x.Rect.Y).ToArray();
            if (text1.Length>0)
            {
                Mat roi = frame[text1[0].Rect];
                //旋转图像为水平
                Cv2.Transpose(roi, roi);
                Cv2.Flip(roi, roi, FlipMode.X);

                string NoString = OcrImage(roi);

                CtnNo = NoString;
            }
            return 0;
        }

        /// <summary>
        ///对图像块进行文字识别,不检测，返回识别字符串
        /// </summary>
        /// <param name="frame">图像块</param>
        public string OcrImage(Mat frame)
        {
            PaddleOcrRecognizerResult result = OcrRecognizer.Run(frame);

            return result.Score.ToString("0.00")+":"+ result.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lblGPU.Text = "正在初始化识别模型...";
            try
            {
                detector.ONNX_Load(textBox1.Text + @"\DetContainer.onnx", textBox1.Text + @"\labelContainer.txt");
                lblGPU.Text = "初始化完成，系统配置了GPU！";
            }
            catch (Exception ex)
            {
                detector.ONNX_Load_CPU(textBox1.Text + @"\DetContainer.onnx", textBox1.Text + @"\labelContainer.txt");
                lblGPU.Text = "初始化完成，系统没有配置GPU！";
            }

            //初始化paddleocr识别器
            //if (lblGPU.Text == "初始化完成，系统配置了GPU！")
            string labelfile = @"\en_dict.txt";
            try
            {
                //OcrRecognizer = new PaddleOcrRecognizer(OcrRecogModel, PaddleDevice.Gpu());
                OcrRecognizer = new PaddleOcrRecognizer(RecognizationModel.FromDirectory(textBox2.Text, textBox2.Text + labelfile, ModelVersion.V3), PaddleDevice.Gpu());
            }
            //else
            catch
            {
                //OcrRecognizer = new PaddleOcrRecognizer(OcrRecogModel, PaddleDevice.Openblas());
                OcrRecognizer = new PaddleOcrRecognizer(RecognizationModel.FromDirectory(textBox2.Text, textBox2.Text + labelfile, ModelVersion.V3), PaddleDevice.Mkldnn());
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //打开对话框，选择文件夹
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                var dirPath = dialog.SelectedPath.Trim();
                textBox2.Text = dirPath;
            }
            else
            {
                Console.WriteLine("ERROR!!!");
            }
        }

        private void Form4_FormClosing(object sender, FormClosingEventArgs e)
        {
            detector.ONNX_dispose();
            OcrRecognizer.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            //load model
            button4_Click(null, null);
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}