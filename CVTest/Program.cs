﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CVTest
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());


            //Mat src = new Mat("C:/Users/Administrator/Desktop/092323R.jpg");
            //Point2f[] srcPt = new Point2f[4];
            //Point2f[] dstPt = new Point2f[4];
            //srcPt[0] = new Point2f(50, 64);
            //srcPt[1] = new Point2f(580, 44);
            //srcPt[2] = new Point2f(461, 378);
            //srcPt[3] = new Point2f(53, 400);
            //dstPt[0] = new Point2f(43, 57);
            //dstPt[1] = new Point2f(511, 46);
            //dstPt[2] = new Point2f(444, 364);
            //dstPt[3] = new Point2f(29, 388);

            //Mat final = new Mat();
            //Mat warpmatrix = Cv2.GetPerspectiveTransform(srcPt, dstPt);
            //Cv2.WarpPerspective(src, final, warpmatrix, src.Size());

            //Cv2.ImShow("dst", final);
            //Cv2.WaitKey();

            //读取图像
            //Mat src = new Mat(@"C:\Users\Administrator\Desktop\直方图均衡测试图像\20230515121830.png");
            //Mat matArray = new Mat();
            //Cv2.CvtColor(src, matArray, ColorConversionCodes.BGR2YCrCb);
            //Mat[] imgYcbcr = new Mat[3];
            //Cv2.Split(matArray, out imgYcbcr);
            //Cv2.EqualizeHist(imgYcbcr[0], imgYcbcr[0]);
            //Cv2.Merge(imgYcbcr, matArray);
            //Cv2.CvtColor(matArray, src, ColorConversionCodes.YCrCb2BGR);
            //Cv2.ImShow("window", src);
            //Cv2.WaitKey();

            //对比度增强
            //Mat src = new Mat(@"C:\Users\Administrator\Desktop\直方图均衡测试图像\20230515121830.png");
            //Mat ripeFrame = ContrastAjust(src, 150);
            //Cv2.ImShow("window", ripeFrame);
            //Cv2.WaitKey();

            //图像锐化
            //Mat src = new Mat(@"C:\Users\Administrator\Desktop\4.16\050134R.jpg");
            ////Cv2.Resize(src, src, new OpenCvSharp.Size(src.Width * 3, src.Height * 3), 0, 0, InterpolationFlags.Linear);
            //Mat dst = ContrastAjust(src, 110);
            //dst = SharpenImage(dst);
            //Cv2.ImShow("src", src);
            //Cv2.ImShow("dst",dst);
            //Cv2.WaitKey();

            //// 加载图像
            //Mat image = Cv2.ImRead("051036B.jpg", ImreadModes.Color);
            //float k = -3;
            //// 添加枕形畸变特效
            //Mat distortedImage = BarrelDistortion(image,k*0.0001);

            //// 显示结果
            //Cv2.ImShow("Distorted Image", distortedImage);
            //Cv2.ImWrite("kkk.jpg", distortedImage);
            //Cv2.WaitKey(0);
            //Cv2.DestroyAllWindows();

        }

        private static Mat ContrastAjust(Mat src, int contrastValue)
        {
            Mat dst = new Mat(src.Size(), src.Type());
            for (int y = 0; y < src.Rows; y++)
            {
                for (int x = 0; x < src.Cols; x++)
                {
                    Vec3b color = new Vec3b
                    {
                        Item0 = Saturate_cast(src.Get<Vec3b>(y, x).Item0 * (contrastValue * 0.01)), // B
                        Item1 = Saturate_cast(src.Get<Vec3b>(y, x).Item1 * (contrastValue * 0.01)), // G
                        Item2 = Saturate_cast(src.Get<Vec3b>(y, x).Item2 * (contrastValue * 0.01))  // R
                    };
                    dst.Set(y, x, color);
                }
            }
            return dst;
        }

        //要确保运算后的像素值在正确的范围内
        private static byte Saturate_cast(double n)
        {
            if (n <= 0)
            {
                return 0;
            }
            else if (n > 255)
            {
                return 255;
            }
            else
            {
                return (byte)n;
            }
        }

        public static Mat SharpenImage(Mat sourceImage)
        {
            // 创建拉普拉斯算子模板
            Mat kernel = new Mat(3, 3, MatType.CV_32FC1, new float[] { 0, -1, 0, -1,5, -1, 0, -1, 0 });

            // 应用拉普拉斯算子模板进行图像锐化
            Mat sharpened = new Mat();
            Cv2.Filter2D(sourceImage, sharpened, -1, kernel);

            return sharpened;
        }

        /// <summary>
        /// 对图像添加径向畸变
        /// </summary>
        /// <param name="image"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static Mat BarrelDistortion(Mat image, double k)
        {
            int height = image.Height;
            int width = image.Width;
            double cx = width / 2.0;
            double cy = height / 2.0;

            Mat distortedImage = new Mat(image.Size(), image.Type());

            double kNormalized = k / (width / 2.0);

            Mat mapX = new Mat(image.Size(), MatType.CV_32FC1);
            Mat mapY = new Mat(image.Size(), MatType.CV_32FC1);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double dx = x - cx;
                    double dy = y - cy;
                    double r = Math.Sqrt(dx * dx + dy * dy);

                    double distortedR = r * (1 + kNormalized * r * r);
                    double scale = r != 0 ? distortedR / r : 0;
                    double distortedX = dx * scale + cx;
                    double distortedY = dy * scale + cy;
                    if (distortedX >= 0 && distortedX < width && distortedY >= 0 && distortedY < height)
                    {
                        mapX.At<float>(y, x) = (float)distortedX;
                        mapY.At<float>(y, x) = (float)distortedY;
                    }
                }
            }
            Cv2.Remap(image, distortedImage, mapX, mapY, InterpolationFlags.Linear);
            return distortedImage;
        }


    }
}
