﻿
namespace CVTest
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.PnlVideo = new System.Windows.Forms.Panel();
            this.BCameraState = new System.Windows.Forms.Label();
            this.FCameraState = new System.Windows.Forms.Label();
            this.BTab = new System.Windows.Forms.TabControl();
            this.BTImg1 = new System.Windows.Forms.TabPage();
            this.BTImgPB1 = new System.Windows.Forms.PictureBox();
            this.BTImg2 = new System.Windows.Forms.TabPage();
            this.BTImgPB2 = new System.Windows.Forms.PictureBox();
            this.BImg1 = new System.Windows.Forms.TabPage();
            this.BImgPB1 = new System.Windows.Forms.PictureBox();
            this.BImg2 = new System.Windows.Forms.TabPage();
            this.BImgPB2 = new System.Windows.Forms.PictureBox();
            this.FTab = new System.Windows.Forms.TabControl();
            this.FImg1 = new System.Windows.Forms.TabPage();
            this.FImgPB1 = new System.Windows.Forms.PictureBox();
            this.FImg2 = new System.Windows.Forms.TabPage();
            this.FImgPB2 = new System.Windows.Forms.PictureBox();
            this.FTImg1 = new System.Windows.Forms.TabPage();
            this.FTImgPB1 = new System.Windows.Forms.PictureBox();
            this.FTImg2 = new System.Windows.Forms.TabPage();
            this.FTImgPB2 = new System.Windows.Forms.PictureBox();
            this.FrontVideoPB = new System.Windows.Forms.PictureBox();
            this.BackVideoPB = new System.Windows.Forms.PictureBox();
            this.lbDateTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbContainerType = new System.Windows.Forms.ComboBox();
            this.textType2 = new System.Windows.Forms.TextBox();
            this.textType1 = new System.Windows.Forms.TextBox();
            this.textNo2 = new System.Windows.Forms.TextBox();
            this.textNo1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.PnlVideo.SuspendLayout();
            this.BTab.SuspendLayout();
            this.BTImg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB1)).BeginInit();
            this.BTImg2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB2)).BeginInit();
            this.BImg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB1)).BeginInit();
            this.BImg2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB2)).BeginInit();
            this.FTab.SuspendLayout();
            this.FImg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB1)).BeginInit();
            this.FImg2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB2)).BeginInit();
            this.FTImg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB1)).BeginInit();
            this.FTImg2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrontVideoPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackVideoPB)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlVideo
            // 
            this.PnlVideo.Controls.Add(this.BCameraState);
            this.PnlVideo.Controls.Add(this.FCameraState);
            this.PnlVideo.Controls.Add(this.BTab);
            this.PnlVideo.Controls.Add(this.FTab);
            this.PnlVideo.Controls.Add(this.FrontVideoPB);
            this.PnlVideo.Controls.Add(this.BackVideoPB);
            this.PnlVideo.Location = new System.Drawing.Point(0, 0);
            this.PnlVideo.Name = "PnlVideo";
            this.PnlVideo.Size = new System.Drawing.Size(962, 563);
            this.PnlVideo.TabIndex = 1;
            // 
            // BCameraState
            // 
            this.BCameraState.BackColor = System.Drawing.SystemColors.Control;
            this.BCameraState.Font = new System.Drawing.Font("宋体", 9F);
            this.BCameraState.ForeColor = System.Drawing.Color.Black;
            this.BCameraState.Location = new System.Drawing.Point(0, 0);
            this.BCameraState.Name = "BCameraState";
            this.BCameraState.Size = new System.Drawing.Size(480, 20);
            this.BCameraState.TabIndex = 20;
            this.BCameraState.Text = "后相机";
            this.BCameraState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FCameraState
            // 
            this.FCameraState.BackColor = System.Drawing.SystemColors.Control;
            this.FCameraState.Font = new System.Drawing.Font("宋体", 9F);
            this.FCameraState.ForeColor = System.Drawing.Color.Black;
            this.FCameraState.Location = new System.Drawing.Point(481, 0);
            this.FCameraState.Name = "FCameraState";
            this.FCameraState.Size = new System.Drawing.Size(480, 20);
            this.FCameraState.TabIndex = 21;
            this.FCameraState.Text = "前相机";
            this.FCameraState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BTab
            // 
            this.BTab.Controls.Add(this.BTImg1);
            this.BTab.Controls.Add(this.BTImg2);
            this.BTab.Controls.Add(this.BImg1);
            this.BTab.Controls.Add(this.BImg2);
            this.BTab.Location = new System.Drawing.Point(0, 292);
            this.BTab.Name = "BTab";
            this.BTab.SelectedIndex = 0;
            this.BTab.Size = new System.Drawing.Size(480, 270);
            this.BTab.TabIndex = 3;
            this.BTab.Tag = "0";
            this.BTab.Resize += new System.EventHandler(this.tabControl_Resize);
            // 
            // BTImg1
            // 
            this.BTImg1.Controls.Add(this.BTImgPB1);
            this.BTImg1.Location = new System.Drawing.Point(4, 22);
            this.BTImg1.Name = "BTImg1";
            this.BTImg1.Padding = new System.Windows.Forms.Padding(3);
            this.BTImg1.Size = new System.Drawing.Size(472, 244);
            this.BTImg1.TabIndex = 0;
            this.BTImg1.Text = "后顶图01";
            this.BTImg1.UseVisualStyleBackColor = true;
            // 
            // BTImgPB1
            // 
            this.BTImgPB1.Location = new System.Drawing.Point(0, 0);
            this.BTImgPB1.Name = "BTImgPB1";
            this.BTImgPB1.Size = new System.Drawing.Size(472, 244);
            this.BTImgPB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTImgPB1.TabIndex = 0;
            this.BTImgPB1.TabStop = false;
            // 
            // BTImg2
            // 
            this.BTImg2.Controls.Add(this.BTImgPB2);
            this.BTImg2.Location = new System.Drawing.Point(4, 22);
            this.BTImg2.Name = "BTImg2";
            this.BTImg2.Size = new System.Drawing.Size(472, 244);
            this.BTImg2.TabIndex = 3;
            this.BTImg2.Text = "后顶图02";
            this.BTImg2.UseVisualStyleBackColor = true;
            // 
            // BTImgPB2
            // 
            this.BTImgPB2.Location = new System.Drawing.Point(0, 0);
            this.BTImgPB2.Name = "BTImgPB2";
            this.BTImgPB2.Size = new System.Drawing.Size(472, 244);
            this.BTImgPB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTImgPB2.TabIndex = 1;
            this.BTImgPB2.TabStop = false;
            // 
            // BImg1
            // 
            this.BImg1.Controls.Add(this.BImgPB1);
            this.BImg1.Location = new System.Drawing.Point(4, 22);
            this.BImg1.Name = "BImg1";
            this.BImg1.Padding = new System.Windows.Forms.Padding(3);
            this.BImg1.Size = new System.Drawing.Size(472, 244);
            this.BImg1.TabIndex = 1;
            this.BImg1.Text = "后图01";
            this.BImg1.UseVisualStyleBackColor = true;
            // 
            // BImgPB1
            // 
            this.BImgPB1.Location = new System.Drawing.Point(0, 0);
            this.BImgPB1.Name = "BImgPB1";
            this.BImgPB1.Size = new System.Drawing.Size(472, 244);
            this.BImgPB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BImgPB1.TabIndex = 0;
            this.BImgPB1.TabStop = false;
            // 
            // BImg2
            // 
            this.BImg2.Controls.Add(this.BImgPB2);
            this.BImg2.Location = new System.Drawing.Point(4, 22);
            this.BImg2.Name = "BImg2";
            this.BImg2.Padding = new System.Windows.Forms.Padding(3);
            this.BImg2.Size = new System.Drawing.Size(472, 244);
            this.BImg2.TabIndex = 2;
            this.BImg2.Text = "后图02";
            this.BImg2.UseVisualStyleBackColor = true;
            // 
            // BImgPB2
            // 
            this.BImgPB2.Location = new System.Drawing.Point(0, 0);
            this.BImgPB2.Name = "BImgPB2";
            this.BImgPB2.Size = new System.Drawing.Size(472, 244);
            this.BImgPB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BImgPB2.TabIndex = 0;
            this.BImgPB2.TabStop = false;
            // 
            // FTab
            // 
            this.FTab.Controls.Add(this.FImg1);
            this.FTab.Controls.Add(this.FImg2);
            this.FTab.Controls.Add(this.FTImg1);
            this.FTab.Controls.Add(this.FTImg2);
            this.FTab.Location = new System.Drawing.Point(481, 292);
            this.FTab.Name = "FTab";
            this.FTab.SelectedIndex = 0;
            this.FTab.Size = new System.Drawing.Size(480, 270);
            this.FTab.TabIndex = 2;
            this.FTab.Tag = "1";
            this.FTab.Resize += new System.EventHandler(this.tabControl_Resize);
            // 
            // FImg1
            // 
            this.FImg1.Controls.Add(this.FImgPB1);
            this.FImg1.Location = new System.Drawing.Point(4, 22);
            this.FImg1.Name = "FImg1";
            this.FImg1.Padding = new System.Windows.Forms.Padding(3);
            this.FImg1.Size = new System.Drawing.Size(472, 244);
            this.FImg1.TabIndex = 2;
            this.FImg1.Text = "前图01";
            this.FImg1.UseVisualStyleBackColor = true;
            // 
            // FImgPB1
            // 
            this.FImgPB1.Location = new System.Drawing.Point(0, 0);
            this.FImgPB1.Name = "FImgPB1";
            this.FImgPB1.Size = new System.Drawing.Size(472, 244);
            this.FImgPB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FImgPB1.TabIndex = 0;
            this.FImgPB1.TabStop = false;
            // 
            // FImg2
            // 
            this.FImg2.Controls.Add(this.FImgPB2);
            this.FImg2.Location = new System.Drawing.Point(4, 22);
            this.FImg2.Name = "FImg2";
            this.FImg2.Size = new System.Drawing.Size(472, 244);
            this.FImg2.TabIndex = 3;
            this.FImg2.Text = "前图02";
            this.FImg2.UseVisualStyleBackColor = true;
            // 
            // FImgPB2
            // 
            this.FImgPB2.Location = new System.Drawing.Point(0, 0);
            this.FImgPB2.Name = "FImgPB2";
            this.FImgPB2.Size = new System.Drawing.Size(472, 244);
            this.FImgPB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FImgPB2.TabIndex = 1;
            this.FImgPB2.TabStop = false;
            // 
            // FTImg1
            // 
            this.FTImg1.Controls.Add(this.FTImgPB1);
            this.FTImg1.Location = new System.Drawing.Point(4, 22);
            this.FTImg1.Name = "FTImg1";
            this.FTImg1.Padding = new System.Windows.Forms.Padding(3);
            this.FTImg1.Size = new System.Drawing.Size(472, 244);
            this.FTImg1.TabIndex = 1;
            this.FTImg1.Text = "前顶图01";
            this.FTImg1.UseVisualStyleBackColor = true;
            // 
            // FTImgPB1
            // 
            this.FTImgPB1.Location = new System.Drawing.Point(0, 0);
            this.FTImgPB1.Name = "FTImgPB1";
            this.FTImgPB1.Size = new System.Drawing.Size(472, 244);
            this.FTImgPB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FTImgPB1.TabIndex = 0;
            this.FTImgPB1.TabStop = false;
            // 
            // FTImg2
            // 
            this.FTImg2.Controls.Add(this.FTImgPB2);
            this.FTImg2.Location = new System.Drawing.Point(4, 22);
            this.FTImg2.Name = "FTImg2";
            this.FTImg2.Padding = new System.Windows.Forms.Padding(3);
            this.FTImg2.Size = new System.Drawing.Size(472, 244);
            this.FTImg2.TabIndex = 0;
            this.FTImg2.Text = "前顶图02";
            this.FTImg2.UseVisualStyleBackColor = true;
            // 
            // FTImgPB2
            // 
            this.FTImgPB2.Location = new System.Drawing.Point(0, 0);
            this.FTImgPB2.Name = "FTImgPB2";
            this.FTImgPB2.Size = new System.Drawing.Size(472, 244);
            this.FTImgPB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FTImgPB2.TabIndex = 0;
            this.FTImgPB2.TabStop = false;
            // 
            // FrontVideoPB
            // 
            this.FrontVideoPB.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FrontVideoPB.Location = new System.Drawing.Point(481, 21);
            this.FrontVideoPB.Name = "FrontVideoPB";
            this.FrontVideoPB.Size = new System.Drawing.Size(480, 270);
            this.FrontVideoPB.TabIndex = 1;
            this.FrontVideoPB.TabStop = false;
            this.FrontVideoPB.Tag = "1";
            // 
            // BackVideoPB
            // 
            this.BackVideoPB.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackVideoPB.Location = new System.Drawing.Point(0, 21);
            this.BackVideoPB.Name = "BackVideoPB";
            this.BackVideoPB.Size = new System.Drawing.Size(480, 270);
            this.BackVideoPB.TabIndex = 0;
            this.BackVideoPB.TabStop = false;
            this.BackVideoPB.Tag = "0";
            // 
            // lbDateTime
            // 
            this.lbDateTime.BackColor = System.Drawing.Color.White;
            this.lbDateTime.Font = new System.Drawing.Font("黑体", 14.25F);
            this.lbDateTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbDateTime.Location = new System.Drawing.Point(12, 695);
            this.lbDateTime.Name = "lbDateTime";
            this.lbDateTime.Size = new System.Drawing.Size(463, 23);
            this.lbDateTime.TabIndex = 4;
            this.lbDateTime.Text = "2023/01/01 00:00:00";
            this.lbDateTime.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("黑体", 14.25F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(481, 695);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(481, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "上海芳铭科技有限公司研制 v3.0";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbContainerType
            // 
            this.cmbContainerType.Font = new System.Drawing.Font("宋体", 15.75F);
            this.cmbContainerType.FormattingEnabled = true;
            this.cmbContainerType.Location = new System.Drawing.Point(367, 601);
            this.cmbContainerType.Name = "cmbContainerType";
            this.cmbContainerType.Size = new System.Drawing.Size(90, 29);
            this.cmbContainerType.TabIndex = 13;
            // 
            // textType2
            // 
            this.textType2.Font = new System.Drawing.Font("宋体", 15.75F);
            this.textType2.Location = new System.Drawing.Point(217, 607);
            this.textType2.Name = "textType2";
            this.textType2.Size = new System.Drawing.Size(143, 31);
            this.textType2.TabIndex = 12;
            // 
            // textType1
            // 
            this.textType1.Font = new System.Drawing.Font("宋体", 15.75F);
            this.textType1.Location = new System.Drawing.Point(217, 569);
            this.textType1.Name = "textType1";
            this.textType1.Size = new System.Drawing.Size(143, 31);
            this.textType1.TabIndex = 11;
            // 
            // textNo2
            // 
            this.textNo2.Font = new System.Drawing.Font("宋体", 14.75F);
            this.textNo2.Location = new System.Drawing.Point(68, 607);
            this.textNo2.Name = "textNo2";
            this.textNo2.Size = new System.Drawing.Size(143, 30);
            this.textNo2.TabIndex = 10;
            // 
            // textNo1
            // 
            this.textNo1.Font = new System.Drawing.Font("宋体", 15.75F);
            this.textNo1.Location = new System.Drawing.Point(68, 569);
            this.textNo1.Name = "textNo1";
            this.textNo1.Size = new System.Drawing.Size(143, 31);
            this.textNo1.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(485, 566);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(230, 31);
            this.button1.TabIndex = 14;
            this.button1.Text = "发送信息";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(732, 603);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(230, 31);
            this.button2.TabIndex = 15;
            this.button2.Text = "退出系统";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(731, 566);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(230, 31);
            this.button3.TabIndex = 16;
            this.button3.Text = "查询";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(485, 603);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(230, 31);
            this.button4.TabIndex = 17;
            this.button4.Text = "系统设置";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("宋体", 15.75F);
            this.textBox1.Location = new System.Drawing.Point(603, 650);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(113, 31);
            this.textBox1.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("黑体", 14.25F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(0, 574);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 23);
            this.label4.TabIndex = 22;
            this.label4.Text = "箱号1";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("黑体", 14.25F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(0, 614);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 23);
            this.label5.TabIndex = 23;
            this.label5.Text = "箱号2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Font = new System.Drawing.Font("黑体", 14.25F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(374, 575);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 23);
            this.label6.TabIndex = 24;
            this.label6.Text = "类别";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Font = new System.Drawing.Font("黑体", 14.25F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(505, 655);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 23);
            this.label7.TabIndex = 25;
            this.label7.Text = "接口连接";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("宋体", 15.75F);
            this.textBox2.Location = new System.Drawing.Point(722, 650);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(240, 31);
            this.textBox2.TabIndex = 26;
            this.textBox2.Text = "道口名称";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Font = new System.Drawing.Font("黑体", 14.25F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 658);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 23);
            this.label8.TabIndex = 27;
            this.label8.Text = "当前状态";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("宋体", 14.75F);
            this.textBox3.Location = new System.Drawing.Point(99, 654);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(358, 30);
            this.textBox3.TabIndex = 28;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 727);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textNo2);
            this.Controls.Add(this.textNo1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmbContainerType);
            this.Controls.Add(this.textType2);
            this.Controls.Add(this.textType1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbDateTime);
            this.Controls.Add(this.PnlVideo);
            this.Name = "Form1";
            this.Text = "集装箱编码识别系统";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.PnlVideo.ResumeLayout(false);
            this.BTab.ResumeLayout(false);
            this.BTImg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB1)).EndInit();
            this.BTImg2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BTImgPB2)).EndInit();
            this.BImg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB1)).EndInit();
            this.BImg2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BImgPB2)).EndInit();
            this.FTab.ResumeLayout(false);
            this.FImg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB1)).EndInit();
            this.FImg2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FImgPB2)).EndInit();
            this.FTImg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB1)).EndInit();
            this.FTImg2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FTImgPB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrontVideoPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackVideoPB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PnlVideo;
        private System.Windows.Forms.PictureBox FrontVideoPB;
        private System.Windows.Forms.PictureBox BackVideoPB;
        private System.Windows.Forms.TabControl FTab;
        private System.Windows.Forms.TabPage FImg1;
        private System.Windows.Forms.PictureBox FImgPB1;
        private System.Windows.Forms.TabPage FImg2;
        private System.Windows.Forms.PictureBox FImgPB2;
        private System.Windows.Forms.TabPage FTImg1;
        private System.Windows.Forms.PictureBox FTImgPB1;
        private System.Windows.Forms.TabPage FTImg2;
        private System.Windows.Forms.PictureBox FTImgPB2;
        private System.Windows.Forms.TabControl BTab;
        private System.Windows.Forms.TabPage BTImg1;
        private System.Windows.Forms.PictureBox BTImgPB1;
        private System.Windows.Forms.TabPage BTImg2;
        private System.Windows.Forms.PictureBox BTImgPB2;
        private System.Windows.Forms.TabPage BImg1;
        private System.Windows.Forms.PictureBox BImgPB1;
        private System.Windows.Forms.TabPage BImg2;
        private System.Windows.Forms.PictureBox BImgPB2;
        private System.Windows.Forms.Label lbDateTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbContainerType;
        private System.Windows.Forms.TextBox textType2;
        private System.Windows.Forms.TextBox textType1;
        private System.Windows.Forms.TextBox textNo2;
        private System.Windows.Forms.TextBox textNo1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label BCameraState;
        private System.Windows.Forms.Label FCameraState;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox3;
    }
}

