﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using System.Threading;


namespace CVTest
{
    class CodeRead
    {
    }
}


public class CameraService
{
    private VideoCapture capture;
    private Thread cameraThread;
    private bool running;
    private string cameraUrl;

    public CameraService(string cameraUrl)
    {
        this.cameraUrl = cameraUrl;
    }

    public void Start()
    {
        if (running) return;
        running = true;

        cameraThread = new Thread(() =>
        {
            while (running)
            {
                try
                {
                    using (capture = new VideoCapture(cameraUrl))
                    {
                        Mat frame = new Mat();
                        while (running)
                        {
                            if (capture.Read(frame))
                            {
                                // Process frame...
                            }
                            else
                            {
                                Console.WriteLine("Connection lost, reconnecting...");
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception occurred: {ex.Message}, reconnecting...");
                }

                if (running)
                {
                    Thread.Sleep(1000); // Wait a bit before trying to reconnect
                }
            }
        });
        cameraThread.IsBackground = true;
        cameraThread.Start();
    }

    public void Stop()
    {
        if (!running) return;
        running = false;

        if (cameraThread != null)
        {
            cameraThread.Join();
            cameraThread = null;
        }

        if (capture != null)
        {
            capture.Release();
            capture.Dispose();
            capture = null;
        }
    }
}