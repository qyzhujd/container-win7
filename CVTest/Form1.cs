﻿using CVTest.Models;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CVTest
{
    public partial class Form1 : Form
    {
        //定义读取视频流模块
        private CameraModel myModel;
        //用于OCR识别的八张图对应的PB数组
        private PictureBox[] picImage = new PictureBox[8];
        //用于显示视频流对应的PB数组
        private PictureBox[] picBox = new PictureBox[2];
        public Form1()
        {
            InitializeComponent();
            myModel = new CameraModel(BackVideoPB, BCameraState);
            myModel.Start();
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    //读取视频流模块初始化
        //}

        private void Form1_Load(object sender, EventArgs e)
        {
            ////设置视频流拉伸显示
            //pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            //Mat frame = Cv2.ImRead("./test.jpg");
            //pictureBox1.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);

            //初始化视频显示控件
            picBox = new PictureBox[2] { BackVideoPB, FrontVideoPB };
            for (int i = 0; i < 2; i++)
            {
                //设置图像显示方式为拉伸适应
                picBox[i].SizeMode = PictureBoxSizeMode.StretchImage;
                //为图像控件绑定双击事件
                picBox[i].DoubleClick += new System.EventHandler(this.picBox_DoubleClick);
            }
            //初始化图片显示控件，并为其绑定双击事件
            picImage = new PictureBox[8] {BTImgPB1, BTImgPB2, BImgPB1, BImgPB2, FImgPB1, FImgPB2, FTImgPB1, FTImgPB2 };
            for (int i = 0; i < 8; i++)
            {
                picImage[i].DoubleClick += new System.EventHandler(this.picImage_DoubleClick);
                picImage[i].SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        /// <summary>
        /// 该函数用于配合视频流预览图像的放大缩小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_DoubleClick(object sender, EventArgs e)  //预览窗口双击放大恢复
        {
            //获取事件的发送者，将其转变为PictureBox类型
            PictureBox pic = (PictureBox)sender;
            //pictureBox的宽度，这是人为设定好的
            int w2 = 480;
            //若当前被双击的PictureBox的宽度大于w2，说明处于窗口放大状态，因此双击后应当恢复四窗口
            //恢复的方法是遍历所有窗口，将其位置、大小重设，并设置可见
            if (pic.Width > w2 + 2)  //恢复四窗口
            {
                if(pic.Tag.ToString() == "0")
                {
                    pic.Location = new System.Drawing.Point(0,21);
                    pic.Size = new System.Drawing.Size(480, 270);
                }
                else
                {
                    pic.Location = new System.Drawing.Point(481, 21);
                    pic.Size = new System.Drawing.Size(480, 270);
                }
            }
            else  //放大窗口
            //放大的方法是将待放大的窗口的位置、大小重设，其他的设为不可见
            {
                pic.Location = new System.Drawing.Point(0, 0);
                pic.Size = new System.Drawing.Size(PnlVideo.Width, PnlVideo.Height);
                pic.BringToFront();
            }
        }

        /// <summary>
        /// 该函数用于配合tab页图像的放大缩小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picImage_DoubleClick(object sender, EventArgs e)
        {
            //将事件的发送者转换为pictureBox
            PictureBox pic = (PictureBox)sender;
            //pictureBox的父控件是TabPage，TabPage的父控件是TabControl
            TabControl tab = (TabControl)pic.Parent.Parent;
            //tabControl的宽度，这是人为设定好的
            int w2 = 480;

            //获取该TabControl控件的Tag值，若为0，代表双击的是左半区Tab，否则代表双击的是右半区Tab
            if (tab.Tag.ToString() == "0")
            {
                if (tab.Width > w2 +2 )
                {
                    tab.Location = new System.Drawing.Point(0,292);
                    tab.Size = new System.Drawing.Size(480,270);
                }
                else
                {
                    tab.Location = new System.Drawing.Point(0,0);
                    tab.Size = new System.Drawing.Size(PnlVideo.Width,PnlVideo.Height);
                }
            }
            else
            {
                if (tab.Width > w2 + 2)
                {
                    tab.Location = new System.Drawing.Point(481, 292);
                    tab.Size = new System.Drawing.Size(480, 270);
                }
                else
                {
                    tab.Location = new System.Drawing.Point(0, 0);
                    tab.Size = new System.Drawing.Size(PnlVideo.Width, PnlVideo.Height);
                }
            }
            //使用此方法使其被置于顶层
            tab.BringToFront();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 该函数用于配合tab页图像的放大缩小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl_Resize(object sender, EventArgs e)
        {
            TabControl tab = (TabControl)sender;

            int ii = Convert.ToInt32(tab.Tag) * 4;
            for (int i = 0 + ii; i < 4 + ii; i++)
            {
                picImage[i].Size = new System.Drawing.Size(tab.Size.Width - 8, tab.Size.Height - 26);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("功能待完成...");
        }
    }
}
