﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CVTest.Models
{
    class CameraModel
    {
        //存放视频图像的pictureBox
        private PictureBox m_video_pb;
        //显示当前状态的label控件
        private Label m_lbState;
        //图像读取线程
        private Thread imgReadThread;
        //运行状态标志位
        private bool running;
        //相机类
        private VideoCapture capture;

        public CameraModel(PictureBox video_pb, Label lbState)
        {
            //视频流读取线程
            m_video_pb = video_pb;
            m_lbState = lbState;
        }

        public void Start()
        {
            if (running) return;
            running = true;

            imgReadThread = new Thread(() =>
            {
                while (running)
                {
                    try
                    {
                        using (capture = new VideoCapture("rtsp://admin:ttt704666@58.199.163.161:554//Streaming/Channels/101"))
                        {
                            Mat frame = new Mat();
                            while (running)
                            {
                                if (capture.Read(frame))
                                {
                                    //将图像发送到前台显示
                                    if (m_video_pb.InvokeRequired)
                                    {
                                        m_video_pb.Invoke(new Action(() =>
                                        {
                                            m_video_pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                                            m_lbState.Text = "后相机（已连接）";
                                            System.GC.Collect();
                                        }));
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Connection lost, reconnecting...");
                                    if (m_lbState.InvokeRequired)
                                    {
                                        m_lbState.Invoke(new Action(() =>
                                        {
                                            m_lbState.Text = "后相机（正在建立连接...）";
                                        }));
                                    }
                                    break;
                                }
                                //ret = capture.Grab();
                                //if (!ret)
                                //{
                                //    Console.WriteLine("流读取失败");
                                //    if (m_lbState.InvokeRequired)
                                //    {
                                //        m_lbState.Invoke(new Action(() =>
                                //        {
                                //            m_lbState.Text = "连接丢失，正在重新建立连接...";
                                //        }));
                                //    }
                                //    running = false;
                                //    break;
                                //}
                                //else
                                //{
                                //    Mat frame = new Mat();
                                //    frame = capture.RetrieveMat();
                                //    //将图像发送到前台显示
                                //    if (m_video_pb.InvokeRequired)
                                //    {
                                //        m_video_pb.Invoke(new Action(() =>
                                //        {
                                //            m_video_pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(frame);
                                //            m_lbState.Text = "已连接";
                                //            System.GC.Collect();
                                //        }));
                                //    }
                                //}
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Exception occurred: {ex.Message}, reconnecting...");
                        if (m_lbState.InvokeRequired)
                        {
                            m_lbState.Invoke(new Action(() =>
                            {
                                m_lbState.Text = "后相机（正在建立连接...）";
                            }));
                        }
                    }

                    if (running)
                    {
                        Thread.Sleep(100); // Wait a bit before trying to reconnect
                    }
                }
            });
            imgReadThread.IsBackground = true;
            imgReadThread.Start();
        }

        public void Stop()
        {
            if (!running) return;
            running = false;

            if (imgReadThread != null)
            {
                imgReadThread.Join();
                imgReadThread = null;
            }

            if (capture != null)
            {
                capture.Release();
                capture.Dispose();
                capture = null;
            }
        }

    }
}
